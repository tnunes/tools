#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 18:13:04 2018
Last modified on 29 June 2020

@author: Sébastien Durand, Rashika Jain
"""

from astropy import units as u
from astropy.coordinates import ICRS
from astropy.coordinates import CartesianRepresentation, CartesianDifferential
import numpy as np
import os
import spiceypy as sp


def load_spice_kernel(eph_path):
    spice_file = []
    for dirpath, _, filenames in os.walk(eph_path):
        for f in filenames:
            if not f.endswith('.txt'):
                spice_file.append(os.path.abspath(os.path.join(dirpath, f)))

    sp.spiceypy.furnsh(spice_file)
    # print("Spice kernels loaded")


def get_state_SSB(naif_id, mjd):
    et = mjd - (51544.5*86400) # 51544.5 is 2000-01-01T12:00:00 in mjd
    p_v = sp.spiceypy.spkgeo(naif_id, et, 'J2000', 0)[0]
    p_v_next = sp.spiceypy.spkgeo(naif_id, et + 10, 'J2000', 0)[0]
    a = (p_v_next-p_v)[3:]/10
    value = np.concatenate([p_v,a])  #km, km/s, km/s2

    return value


def get_rot_mat(spice_frame_name, mjd):
    et = mjd - (51544.5*86400)
    rm = sp.spiceypy.pxform('J2000', spice_frame_name, et)
    
    return rm


def calceph2astropy_icrf(x, y, z, vx=[], vy=[], vz=[], position_unit="km", velocity_unit="km/s"):
    # convert ephemeride to ICRS astropy frame
    if vx:
        eph_astropy = ICRS(x=x * u.Unit(position_unit), y=y * u.Unit(position_unit), z=z * u.Unit(position_unit),
                           v_x=vx * u.Unit(velocity_unit), v_y=vy * u.Unit(velocity_unit),
                           v_z=vz * u.Unit(velocity_unit),
                           representation_type=CartesianRepresentation,
                           differential_type=CartesianDifferential)
    else:

        eph_astropy = ICRS(x=x * u.Unit(position_unit), y=y * u.Unit(position_unit), z=z * u.Unit(position_unit),
                           representation_type=CartesianRepresentation)

    return eph_astropy
