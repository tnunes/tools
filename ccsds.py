#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 11:49:46 2018

@author: Sébastien Durand
"""
import numpy as np
from datetime import datetime
from tqdm import tqdm
from astropy import units as u
from astropy.time import Time


# ////////////////////////////////////////// Output files creation /////////////////////////////////////////////

def write_ccsds(sat_quat_path_out, cic, origin, comment, header, sequence, data_time, data_science, separator='\t'):
    """
    write cic file

    :param sat_quat_path_out:(string) : Output path for new CIC file
    :param cic:(string) : CIC string Line
    :param origin:(string) : String origin Name Line
    :param comment:(string vector) : String List of Comment, line by line
    :param header:(string vector) : String List of other line
    :param sequence:(string vector) : String List for defined writing formats
                    (ex: for time + 3 data: ["%d","%.6f","%.8f","%.8f","%.8f"])
                    (ex: for time + 1 string data: ["%s","%s","%s"])
    :param data_time:(Nx2, float) : All Time, julian day and second
    :param data_science:(NxM, float) : All Data, M x data

    :return: No have return
    """

    if len(data_time) == len(data_science):
        data = []
        for i in range(len(data_time)):
            data.append(np.concatenate((data_time[i], data_science[i]),dtype=object))

        if len(data[0]) == len(sequence):

            f0 = open(sat_quat_path_out, 'w')

            # create first header :
            f0.write(cic + "\n")
            f0.write(datetime.now().strftime("CREATION_DATE = %Y-%m-%dT%H:%M:%S.%f")[:-3] + "\n")
            f0.write(origin + "\n")

            # create principal header
            f0.write("\n" + "META_START" + "\n" + "\n")
            for i in range(len(comment)):
                f0.write(comment[i] + "\n")
            f0.write("\n")
            for i in range(len(header)):
                f0.write(header[i] + "\n")
            f0.write("\n" + "META_STOP" + "\n" + "\n")

            # write data
            for i in range(len(data_science)):
                np.savetxt(f0, [data[i]], delimiter=separator, fmt=sequence)

            f0.close()
        else:
            print("Data_time + data_science size :", (len(data_time[0]) + len(data_science[0])))
            print("sequence size :", len(sequence))
            raise IndexError("Data_time + data_science size is not the same size as sequence")
    else:
        print("Data_time size :", len(data_time))
        print("Data Science size :", len(data_science))
        raise IndexError("Data_time and data_science have not the same size")


def read_ccsds(sat_quat_path_in, time_format='mjd2col', separator='\t'):
    """
    Read ccsds and cic file

    :param sat_quat_path_in:(string) Path to Input CIC File
    :param separator:(string) data separator ( space (" ") or tab ("\t") in generally), default tab ("\t")

    :return:cic:(string) CIC string Line
            origin:(string) String origin Name Line
            comment:(string array) String List of Comment, line by line
            header:(string array) String List of other line
            data_time:(Nx2, float) All Time, julian day and second
            data_science:(NxM, float) All Data, M x data
    """

    f_read = open(sat_quat_path_in, 'r')

    meta_stop = 'META_STOP'
    text = f_read.readlines()
    text = np.array(text)
    for i in range(text.size):
        text[i] = text[i].strip()

    text_filter = []
    for i in range(text.size):
        if text[i] != '':
            text_filter.append(text[i])
    text_filter = np.array(text_filter)

    # start = np.where(text_filter == meta_stop)[0]
    for i in range(text_filter.size):
        if text_filter[i] == meta_stop:
            stop = i
            break

    if not ('stop' in locals()):
        raise AttributeError("META_STOP is not detect")

    data = text_filter[stop + 1:]
    header = text_filter[0:stop]
    origin = ""
    cic = ""
    comment = []
    for i in tqdm(range(header.size), ascii=True, desc="Read Header", disable=True):
        if header[i].startswith('COMMENT'):
            comment.append(header[i])
        if header[i].startswith("CIC_AEM_VERS"):
            cic = header[i]
            cic_type = "attitude"
        elif header[i].startswith("CIC_OEM_VERS"):
            cic = header[i]
            cic_type = "orbit"
        elif header[i].startswith("CCSDS_OEM_VERS"):
            cic = header[i]
            cic_type = "orbit_ccsds"
        elif header[i].startswith("CCSDS_AEM_VERS"):
            cic = header[i]
            cic_type = "attitude_ccsds"
        elif header[i].startswith("CIC_MEM_VERS"):
            cic = header[i]
            cic_type = "event_cic"
        if header[i].startswith("ORIGINATOR"):
            origin = header[i]
        if header[i].startswith("META_START"):
            start = i
    if not ('start' in locals()):
        raise AttributeError("META_START is not detect")
    elif not ('cic' in locals()):
        raise AttributeError(
            "CIC_OEM_VERS or CIC_AEM_VERS or CCSDS_AEM_VERS or CCSDS_OEM_VERS or CIC_MEM_VERS is not detect")
    elif not ('origin' in locals()):
        raise AttributeError("ORIGINATOR is not detect")

    header = header[start + 1 + len(comment):]

    if time_format == 'mjd2col':
        data_sp = []
        if cic_type == "attitude_ccsds" or cic_type == "orbit_ccsds" or cic_type == "orbit" or cic_type == "attitude":
            for i in tqdm(data, ascii=True, desc="Read DATA File", disable=True):
                dd = np.float64(i.split(separator))
                data_sp.append(dd.astype(float))
        elif cic_type == "event_cic":
            for i in tqdm(data, ascii=True, desc="Read DATA File", disable=True):
                data_sp.append(i.split(separator))
        data_sp = np.array(data_sp)
        f_read.close()

        data_science = data_sp[:, 2:]
        data_time = data_sp[:, :2]
    
    else:
        data_time = []
        data_sp = []
        if cic_type == "attitude_ccsds" or cic_type == "orbit_ccsds" or cic_type == "orbit" or cic_type == "attitude":
            for i in tqdm(data, ascii=True, desc="Read DATA File", disable=True):
                if i.split()[0] != 'COMMENT':
                    data_time.append(i.split()[0])
                    dd = np.float64(i.split()[1:])
                    data_sp.append(dd.astype(float))
        data_sp = np.array(data_sp)
        f_read.close()

        data_science = data_sp
        data_time = np.array(data_time)

    return cic, origin, comment, header, data_time, data_science


def read_miriade_eph_file(file_path, coord_type="Rectangular", ref_plane="Equator", time_type="utc"):
    """
    Read miriade ephemeris file

    :param file_path: Path to Input miriade File
    :param coord_type: coordinate type (Rectangular or Spherical)
    :param ref_plane: reference plane (!not use!)
    :param time_type: Time type (utc or tt)

    :return:data_time:(Astropy Time) Data time vector
            data_science:(Array, witch astropy unit) Position in frame or angle in sky
            header:(string) text of header
    """
    # need heliocentric coord

    f_read = open(file_path, 'r')
    number_line_header = 5

    text = f_read.readlines()
    text = np.array(text)
    header = text[0:number_line_header - 1]
    data_text = text[number_line_header:]

    for i in range(data_text.size):
        data_text[i] = data_text[i].strip()

    f_read.close()

    data_text_filter = []
    for i in range(data_text.size):
        if data_text[i] != '':
            data_text_filter.append(data_text[i])
    data_text_filter = np.array(data_text_filter)

    data_split = []
    for i in tqdm(data_text_filter, ascii=True, desc="Read DATA File"):
        data_split.append(i.split(",   "))
    data_split = np.array(data_split)
    data_split = data_split.T

    data_time = data_split[1]
    data_science = data_split[2:]

    col = [["Target", "name"], ["Date", "ISOT"]]

    if coord_type == "Spherical":
        col.append(["RA", ["hour", "minute", "second"]])
        col.append(["DEC", ["day", "minute", "second"]])
        col.append(["Distance", "AU"])

    elif coord_type == "Rectangular":
        col.append(["X", "AU"])
        col.append(["Y", "AU"])
        col.append(["Z", "AU"])
        col.append(["Xp", "AU/day"])
        col.append(["Yp", "AU/day"])
        col.append(["Zp", "AU/day"])
        col.append(["Distance", "AU"])

    else:
        raise ValueError("This Coord_type is not support, just Spherical and Rectangular")

    print(col)
    new_data = []
    k = 0
    for i in range(2, len(col)):
        if type(col[i][1]) == list:
            for j in range(len(col[i][1])):
                new_data.append(np.float32(data_science[k]) * u.Unit(col[i][1][j]))
                k = k + 1
        else:
            new_data.append(np.float32(data_science[k]) * u.Unit(col[i][1]))
            k = k + 1

    data_science = new_data
    data_time = Time(data_time, format="isot", scale=time_type)

    return data_time, data_science, header
