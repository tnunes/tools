import numpy as np


### ELEMENT CONVERSION ###

# CARTESIAN TO KEPLERIAN ELEMENTS
def cart2kep(state, mu):
    # output angle between 0 and 360 (I think?)
    state = state.reshape(-1,6)
    r_arr = state[:,:3]
    v_arr = state[:, 3:]
    r = np.linalg.norm(r_arr, axis=1).reshape(-1,1)
    v = np.linalg.norm(v_arr, axis=1).reshape(-1,1)
    h_arr = np.cross(r_arr,v_arr)
    h = np.linalg.norm(h_arr, axis=1)
    N_arr = np.cross(np.array([0,0,1]),h_arr)
    N = np.linalg.norm(N_arr, axis=1).reshape(-1,1)
    a = 1 / (2/r - v**2/mu)
    e_arr = np.cross(v_arr,h_arr)/mu - r_arr/r
    e = np.linalg.norm(e_arr, axis=1).reshape(-1,1)
    i_kep = np.arccos(h_arr[:,2]/h)
    rhat_arr = r_arr/r
    ehat_arr = e_arr/e
    raan = np.zeros(len(state))
    omega = np.zeros(len(state))
    theta = np.zeros(len(state))
    for i in range(0,len(state)):
        if i_kep[i] == 0. or i_kep[i] == np.pi:
            raan[i] = 0.
            omega[i] = 0.
        else:
           Nxy = np.sqrt(N_arr[i,0]**2 + N_arr[i,1]**2)
           raan[i] = np.arctan2(N_arr[i,1]/Nxy, N_arr[i,0]/Nxy)
           Nhat_arr = N_arr/N[i]
           omega[i] = np.arccos(np.dot(ehat_arr[i],Nhat_arr[i]))
           if np.dot( np.cross(Nhat_arr[i],e_arr[i]), h_arr[i] ) < 0:
                omega[i] = omega[i] * -1
        theta[i] = np.arccos(np.dot(rhat_arr[i],ehat_arr[i]))
        if np.dot( np.cross(e_arr[i],r_arr[i]), h_arr[i] ) < 0:
            theta[i] = theta[i] * -1
    
    omega = omega%(2*np.pi) # 0-2pi
    raan = raan%(2*np.pi) # 0-2pi
    theta = theta%(2*np.pi) # 0-2pi

    orb_elems = np.hstack((a.reshape(-1,1), e.reshape(-1,1), i_kep.reshape(-1,1), omega.reshape(-1,1), raan.reshape(-1,1), theta.reshape(-1,1)))
    if orb_elems.shape[0]==1:
        orb_elems = orb_elems.reshape(6)

    return orb_elems

# KEPLERIAN ELEMENTS TO CARTESIAN
def kep2cart(orbital_ele, mu):
    orbital_ele = orbital_ele.reshape(-1,6)
    a = orbital_ele[:,0]
    e = orbital_ele[:,1]
    i_kep = orbital_ele[:,2]
    omega = orbital_ele[:,3]
    raan = orbital_ele[:,4]
    theta = orbital_ele[:,5]
    r = a * (1 - e**2) / (1 + e*np.cos(theta))
    x_kep = r * np.cos(theta)
    y_kep = r * np.sin(theta)
    l1 = np.cos(raan) * np.cos(omega) - np.sin(raan) * np.sin(omega) * np.cos(i_kep)
    l2 = -np.cos(raan) * np.sin(omega) - np.sin(raan) * np.cos(omega) * np.cos(i_kep)
    m1 = np.sin(raan) * np.cos(omega) + np.cos(raan) * np.sin(omega) * np.cos(i_kep)
    m2 = -np.sin(raan) * np.sin(omega) + np.cos(raan) * np.cos(omega) * np.cos(i_kep)
    n1 = np.sin(omega) * np.sin(i_kep)
    n2 = np.cos(omega) * np.sin(i_kep)
    h = np.sqrt(mu*a*(1-e**2))
    pos_cart = np.zeros((3,len(a)))
    vel_cart = np.zeros((3,len(a)))
    for i in range(0,len(orbital_ele)):
        Rk2c_pos = np.array([[l1[i], l2[i]], [m1[i], m2[i]], [n1[i], n2[i]]])
        pos_cart[:,i] = np.transpose(Rk2c_pos @ np.array([[x_kep[i]],[y_kep[i]]]))
        Rk2c_vel = np.array([[-l1[i], l2[i]], [-m1[i], m2[i]], [-n1[i], n2[i]]])
        vel_cart[:,i] = mu/h[i] * np.transpose( Rk2c_vel @ np.array([ [np.sin(theta[i])], [e[i] + np.cos(theta[i])] ]) )
    
    states = np.hstack((np.transpose(pos_cart), np.transpose(vel_cart)))
    if states.shape[0]==1:
        states = states.reshape(6)

    return states

# TRUE ANOMALY TO MEAN ANOMALY
def true2mean_anomaly(e, th):
    E = 2 * np.arctan2(np.sqrt(1 - e) * np.sin(th / 2), np.sqrt(1 + e) * np.cos(th / 2)) # -pi-pi
    M = E - e * np.sin(E)
    M = M%(2*np.pi) # 0-2pi

    return M

# TIME TO TRUE ANOMALY
def time2true_anomaly(dt, th_init, a, e, mu, tol=2e-15):
    # dt is times relative to initial time (of initial true anomaly (th_init))
    # dt can be an array
    n = np.sqrt(mu / a**3)
    T = 2*np.pi/n
    dt = dt%(T)
    M_init = true2mean_anomaly(e, th_init)
    M = M_init + n*dt
    E = M
    conv = np.zeros(len(E)) + 1.
    it = 0
    while np.any(conv > tol) and it < 1000:
        E_prev = E
        E = M + e * np.sin(E)
        conv = E - E_prev
        it += 1
    if it == 1000:
        print(f'time2true_anomaly: Tolerance required:{tol}  ; Tolerance Obtained:{max(abs(conv))}')
    theta = 2 * np.arctan2(np.sqrt(1 + e) * np.sin(E / 2), np.sqrt(1 - e) * np.cos(E / 2)) # -pi-pi
    theta = theta%(2*np.pi) # 0-2pi

    return theta, conv


### TIME CONVERSION ###

# MJD 2 COLUMNS to MJD IN SECONDS & TIME FLIGHT
def mjd_2col2seconds(data_time):
    time = data_time[:,0]*86400+data_time[:,1] # s
    time_flight = time - time[0] # s

    return time, time_flight


### PARAMETER COMPUTATION ###

# ANGULAR MOMENTUM FROM CARTESIAN COORDINATES
def cart2angular_momentum(states):
    states = states.reshape(-1,6)
    r = states[:,:3]
    v = states[:,3:]
    h_arr = np.cross(r, v)
    h = np.linalg.norm(h_arr, axis=1)

    if h_arr.shape[0] == 1:
        h_arr = h_arr.reshape(3)
        h = h[0]

    return h, h_arr

# ANGULAR MOMENTUM FROM KEPLERIAN ELEMENTS
def kep2angular_momentum_norm(a, e, mu):
    h = np.sqrt(mu * a * ( 1 - e**2))

    return h


# ENERGY FROM CARTESIAN COORDINATES
def cart2energy(states, mu):
    states = states.reshape(-1,6)
    r_arr = states[:,:3]
    v_arr = states[:, 3:]
    r = np.linalg.norm(r_arr, axis=1)
    v = np.linalg.norm(v_arr, axis=1)
    energy = v**2 / 2 - mu / r
    
    if energy.shape[0]==1:
        energy = energy[0]

    return energy


# ENERGY FROM KEPLERIAN ELEMENTS
def kep2energy(a, mu):
    energy = -mu / (2*a)

    return energy


### UTILS ###

# PRINT WITH NO DECIMAL CASES IN CASE OF INT
def print_int_float(value):
    if value % 1 == 0:
        return int(value)
    else:
        return value