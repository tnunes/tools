#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 22 11:01:09 2018

@author: Sébastien Durand
"""
from astropy.time import Time
import numpy as np

second_day = 24 * 60 * 60


def time_step_gen(time_interval, start_mjd, end_mjd, time_type="mjd", input_type="vector", output_type="vector"):
    """
    Generate list of Julian date with time step

    For start_mjd and end_mjd and mjd_date
    3 type of input (vector, float, astropy):
    vector --> float vector, size:2, 1:(int) day, 2:(float) second
    float --> float
    astropy --> astropy Time

    :param time_interval:(float) setting the interval of iteration (unit: sec)
    :param start_mjd:float 1x2) Vector for starting time in time_type format
    :param end_mjd:(list float 1x2) Vector for ending time in time_type format
    :param time_type:(string) mjd or jd for input and output
    :param input_type:(string): vector, float, astropy
    :param output_type:(string): vector, float, astropy

    :return:MJD_date:(float array N*2, float vector N*1 or astropy) vector for MJD date [day,second], [day], astropy
    """
    # Load parameters from input and normalize the initial quaternion

    if input_type == "vector":
        start_mjd = start_mjd
        end_mjd = end_mjd
    elif input_type == "float":
        start_mjd_new = np.zeros(2)
        start_mjd_new[0] = np.int(start_mjd)
        start_mjd_new[1] = (start_mjd % 1) * second_day
        start_mjd = start_mjd_new
        end_mjd_new = np.zeros(2)
        end_mjd_new[0] = np.int(end_mjd)
        end_mjd_new[1] = (end_mjd % 1) * second_day
        end_mjd = end_mjd_new
    elif input_type == "astropy":
        start_mjd_new = np.zeros(2)
        end_mjd_new = np.zeros(2)
        if time_type == "mjd":
            start_mjd_new[0] = np.int(start_mjd.mjd)
            start_mjd_new[1] = (start_mjd.mjd % 1) * second_day
            end_mjd_new[0] = np.int(end_mjd.mjd)
            end_mjd_new[1] = (end_mjd.mjd % 1) * second_day
        elif time_type == "jd":
            start_mjd_new[0] = np.int(start_mjd.jd)
            start_mjd_new[1] = (start_mjd.jd % 1) * second_day
            end_mjd_new[0] = np.int(end_mjd.jd)
            end_mjd_new[1] = (end_mjd.jd % 1) * second_day

        start_mjd = start_mjd_new
        end_mjd = end_mjd_new

    else:
        raise AttributeError("undefined output format")

    # Calculate the period of time
    if end_mjd[0] >= start_mjd[0]:
        if (end_mjd[0] == start_mjd[0]) and (end_mjd[1] < start_mjd[1]):
            raise ValueError("the MJD beginning second is not before the MJD end second")
        else:
            time_duration = (end_mjd[0] - start_mjd[0]) * second_day + (end_mjd[1] - start_mjd[1])
    else:
        raise ValueError("the MJD beginning date is not before the MJD end date")

    nb = np.int32(time_duration / time_interval)

    mjd_all_sec = (np.arange(nb) * time_interval) + start_mjd[1]
    mjd_sec = mjd_all_sec % second_day
    mjd_day = np.floor_divide(mjd_all_sec, second_day) + start_mjd[0]
    mjd_date_temp = np.zeros((nb, 2), dtype=float)
    mjd_date_temp[:, 0] = mjd_day
    mjd_date_temp[:, 1] = mjd_sec
    if output_type == "vector":
        mjd_date = mjd_date_temp

    elif output_type == "float" or output_type == "astropy":
        mjd_date = mjd_date_temp[:, 0] + mjd_date_temp[:, 1] / second_day

        if output_type == "astropy":
            mjd_date = Time(mjd_date, format=time_type, scale='utc')

    else:
        raise AttributeError("undefined output format")

    return mjd_date


def add_zero_to_str(num):
    """
    Convert List of float/int to string and add 0 for number < 10 and >= 0

    :param num:(list of int/float or Int/float number): int/float for converting

    :return:s_num: (string vector or string): converted number
    """

    if type(num) == list:
        correct_str = np.where((np.array(num) < 10) & (np.array(num) >= 0))[0]
        s_num = list(map(str, num))

        for i in correct_str:
            s_num[i] = "0" + s_num[i]
    else:
        if 10 > num >= 0:
            s_num = "0" + str(num)

        else:
            s_num = str(num)

    return s_num


def julian_to_vector_julian(julian_time):
    """
    Separate mjd/jd float vector in 2 float vector (day and second)

    :param julian_time:(float vector, size:N) time vector in mjd/jd

    :return: (float array, size:N*2) time array with 1:(int) mjd/jd day 2:(float) second
    """

    day, hour = np.divmod(julian_time, 1, dtype=float)
    vector_julian = [np.int32(day), second_day * hour]

    return np.array(vector_julian).T


def vector_julian_to_julian(julian_time_vector):
    """
    Collects 2 float vector (day and second) in one mjd/jd float vector

    :param julian_time_vector:(float array, size:N*2) time array with 1:(int) mjd/jd day 2:(float) second

    :return:julian:(float vector, size:N) time vector in mjd/jd
    """

    julian_time_vector.T[1] = julian_time_vector.T[1] / (60 * 60 * 24)
    julian = np.sum(julian_time_vector, axis=1)

    return julian


def utc_to_mjd(utc_time_years, utc_time_month, utc_time_day, utc_time_hour,
               utc_time_minute, utc_time_second, julian_type='mjd', output_type="vector"):
    """
    Convert List or one UTC date to Julian/MJD date

    For output, 3 type of input (vector, float, astropy):
    vector --> float vector, size:2, 1:(int) day, 2:(float) second
    float --> float
    astropy --> astropy Time

    :param utc_time_years:(int vector or int) Number of years
    :param utc_time_month:(int vector or int) Number of month
    :param utc_time_day:(int vector or int) Number of day
    :param utc_time_hour:(int vector or int) Number of hour
    :param utc_time_minute:(int vector or int) Number of minute
    :param utc_time_second:(float vector or float) Number of seconds
    :param julian_type:(string) Output type of Julian Date ("mjd", "jd")
    :param output_type:(string) Output format of Julian Date ( "vector", "float" or astropy)

    :return:(float array N*2, float vector N*1 or astropy) time_vector in output format [day,second], [day], astropy

    """

    if ((((type(utc_time_years) == list) and (type(utc_time_month) == list)) and
         ((type(utc_time_day) == list) and (type(utc_time_hour) == list))) and
            ((type(utc_time_minute) == list) and (type(utc_time_second) == list))):

        if (((len(utc_time_years) == len(utc_time_month)) and
             (len(utc_time_day) == len(utc_time_hour))) and
                (len(utc_time_minute) == len(utc_time_second))):

            if len(utc_time_years) == 1:
                s_utc_time_years = str(utc_time_years[0])
                s_utc_time_month = add_zero_to_str(utc_time_month[0])
                s_utc_time_day = add_zero_to_str(utc_time_day[0])
                s_utc_time_hour = add_zero_to_str(utc_time_hour[0])
                s_utc_time_minute = add_zero_to_str(utc_time_minute[0])
                s_utc_time_second = add_zero_to_str(utc_time_second[0])
                utc_time = "{0}-{1}-{2}T{3}:{4}:{5}".format(s_utc_time_years, s_utc_time_month, s_utc_time_day,
                                                            s_utc_time_hour, s_utc_time_minute, s_utc_time_second)

            else:
                utc_time = []

                s_utc_time_years = list(map(str, utc_time_years))
                s_utc_time_month = add_zero_to_str(utc_time_month)
                s_utc_time_day = add_zero_to_str(utc_time_day)
                s_utc_time_hour = add_zero_to_str(utc_time_hour)
                s_utc_time_minute = add_zero_to_str(utc_time_minute)
                s_utc_time_second = add_zero_to_str(utc_time_second)

                for i in range(len(utc_time_years)):
                    utc_time.append(
                        "{0}-{1}-{2}T{3}:{4}:{5}".format(s_utc_time_years[i], s_utc_time_month[i], s_utc_time_day[i],
                                                         s_utc_time_hour[i], s_utc_time_minute[i],
                                                         s_utc_time_second[i]))

        else:
            raise IndexError("Len Error of input arguments")

    else:
        s_utc_time_years = str(utc_time_years)
        s_utc_time_month = add_zero_to_str(utc_time_month)
        s_utc_time_day = add_zero_to_str(utc_time_day)
        s_utc_time_hour = add_zero_to_str(utc_time_hour)
        s_utc_time_minute = add_zero_to_str(utc_time_minute)
        s_utc_time_second = add_zero_to_str(utc_time_second)
        utc_time = "{0}-{1}-{2}T{3}:{4}:{5}".format(s_utc_time_years, s_utc_time_month, s_utc_time_day, s_utc_time_hour,
                                                    s_utc_time_minute, s_utc_time_second)

    time_astro_utc = Time(utc_time, format='isot', scale='utc')

    if julian_type == 'mjd':
        julian_time = time_astro_utc.mjd

    elif julian_type == 'jd':
        julian_time = time_astro_utc.jd

    else:
        raise AttributeError("undefined julian type")

    if output_type == "vector":
        vector_julian = [np.int32(julian_time),
                         np.array(utc_time_second) + 60 * np.array(utc_time_minute) + 60 * np.array(utc_time_hour)]
        return vector_julian
    elif output_type == "float":
        return julian_time
    elif output_type == "astropy":
        return time_astro_utc
    else:
        raise AttributeError("undefined output format")
