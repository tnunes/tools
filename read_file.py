import numpy as np

### READ DATA FROM FILE ###

# READ TUDAT DATA FILE
def get_tudat_file(filepath):

    with open(filepath) as f:
        lines = f.readlines()
    # convert data in list for each line
    data = [line.split() for line in lines]
    # convert data in np.array
    data = np.array(data,dtype=float)
    data[:,0] += (51544.5*86400) # convert to mjd seconds from et seconds [et is seconds past J200 in TDB]
    return data


# READ GMAT REPORT DATA FILE
def read_gmat_rep(filepath, first_col='No string'):
    if first_col == 'No string':
        with open(filepath) as f:
            lines = f.readlines()
        # convert data in list for each line
        data = [line.split() for line in lines[1:]]
        # convert data in np.array
        data = np.array(data,dtype=float)
        return data
    
    elif first_col == 'string time':
        with open(filepath) as f:
            lines = f.readlines()
        # convert data in list for each line
        data = [line.split()[4:] for line in lines[1:]]
        # convert data in np.array
        data = np.array(data,dtype=float)
        return data


# READ FREEFLYER REPORT DATA FILE
def read_ff_rep(filepath):
    with open(filepath) as f:
        lines = f.readlines()
    # convert data in list for each line
    data = [line.split() for line in lines[3:]] # km and s
    # convert data in np.array
    data = np.array(data,dtype=object)
    data_time = list(data[:,0])
    data_science = np.array(data[:,1:],dtype=float)
    # Convert MJD TDB to seconds
    data[:,0] *= 86400
    
    return data_time, data_science


# READ SH FROM DOCKS FILE AND RETURN COEFFICENTS IN TUDAT FORMAT
def read_docks_sh2tudat(filepath):
    # Load Spherical Harmonics
    with open(filepath) as f:
        lines = f.readlines()
    # convert data in list for each line
    data = [line.split(',') for line in lines]
    # get parameters
    R = float(data[0][0].replace(" ", "").replace("\t", "")) * 1e3 # m
    mu = float(data[0][1].replace(" ", "").replace("\t", "")) * 1e9 # kg and m and s
    degree = data[0][3].replace(" ", "").replace("\t", "")
    order = data[0][4].replace(" ", "").replace("\t", "")
    normalized = data[0][5].replace(" ", "").replace("\t", "")
    # get coefficients
    coeffs = data[3:]
    if ['\n'] in coeffs:
        coeffs.remove(['\n'])
    coeffs_C = np.zeros((int(degree)+1, int(degree)+1))
    coeffs_C[0,0] = 1.
    coeffs_S = np.zeros((int(degree)+1, int(degree)+1))
    for jjj in range(len(coeffs)):
        coeffs_C[int(coeffs[jjj][0]), int(coeffs[jjj][1])] = float(coeffs[jjj][2])
        coeffs_S[int(coeffs[jjj][0]), int(coeffs[jjj][1])] = float(coeffs[jjj][3])
    return coeffs_C, coeffs_S, mu, R
