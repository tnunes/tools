import os
import numpy as np
import matplotlib
matplotlib.rcParams.update({'font.size': 12})
matplotlib.rcParams.update({'figure.max_open_warning': 0})
from matplotlib import pyplot as plt, ticker as mticker
from .ccsds import read_ccsds
from .conversion import mjd_2col2seconds, print_int_float, cart2kep, kep2cart, time2true_anomaly,\
      cart2angular_momentum, cart2energy, kep2angular_momentum_norm, kep2energy, true2mean_anomaly
from .read_file import read_gmat_rep, read_ff_rep, get_tudat_file
from scipy.interpolate import interp1d
from astropy.time import Time
import copy
from matplotlib.animation import FuncAnimation
import pickle


class  Analysis:

    def __init__(self, Simulations_DOCKS, Simulations_TUDAT, Simulations_GMAT, Simulations_FF, 
                 output_DOCKS='./Outputs/DOCKS/', output_TUDAT='./Outputs/TUDAT/', 
                 output_GMAT='./Outputs/GMAT/', output_FF='./Outputs/FREEFLYER/',
                 ):
        self.Simulations_DOCKS = Simulations_DOCKS
        self.Simulations_TUDAT = Simulations_TUDAT
        self.Simulations_GMAT = Simulations_GMAT
        self.Simulations_FF = Simulations_FF
        self.output_DOCKS = output_DOCKS
        self.output_TUDAT = output_TUDAT
        self.output_GMAT = output_GMAT
        self.output_FF = output_FF

        self.GMAT_O = 'eph'
        self.probe_mass = 0 #kg
        self.G = 6.67430 * 1e-11 # m^3/kg/s^2


    ### Redefine / Info ###
        
    def show_plots(self):
        plt.show()
    
    def close_plots(self):
        plt.close()
        
    def print_outputs_path(self):
        print(f'DOCKS: {self.output_DOCKS}\nTUDAT: {self.output_TUDAT}\nGMAT: {self.output_GMAT}\nFREEFLYER: {self.output_FF}\n')

    def update_output_path(self, type, new_output):
        if type.lower() == 'docks':
            self.output_DOCKS = new_output
        elif type.lower() == 'tudat':
            self.output_TUDAT = new_output
        elif type.lower() == 'gmat':
            self.output_GMAT = new_output
        elif type.lower() == 'freeflyer':
            self.output_FF = new_output
    
    def load_analytical_from_file(self, filepath, mu, time_type='isot', units='km'):
        with open(filepath) as f:
            lines = f.readlines()
        lines = [line.split() for line in lines]
        if ['\n'] in lines:
            lines.remove(['\n'])
        # get last line
        data = lines[-1]
        self.analytical_mu = mu
        if time_type == 'isot':
            self.analytical_initial_date = data[0].replace('T', 'TDB')
            self.analytical_initial_state_cart = np.array(data[1:7], dtype=float)

        if units == 'km':
            self.analytical_initial_state_cart *= 1000
        self.analytical_initial_state_kep = cart2kep(self.analytical_initial_state_cart, mu)
        self.analytical_h_norm, self.analytical_h = cart2angular_momentum(self.analytical_initial_state_cart)
        self.analytical_e = cart2energy(self.analytical_initial_state_cart, mu)
        self.analytical_M_init = true2mean_anomaly(self.analytical_initial_state_kep[1], self.analytical_initial_state_kep[5])

    def load_analytical(self, initial_state, initial_time, mu, type='cartesian'):
        self.analytical_mu = mu
        self.analytical_initial_date = initial_time
        if type == 'cartesian':
            self.analytical_initial_state_cart = initial_state
            self.analytical_initial_state_kep = cart2kep(self.analytical_initial_state_cart, mu)
            self.analytical_h_norm, self.analytical_h = cart2angular_momentum(self.analytical_initial_state_cart)
            self.analytical_e = cart2energy(self.analytical_initial_state_cart, mu)
        elif type == 'keplerian':
            self.analytical_initial_state_kep = initial_state
            self.analytical_initial_state_cart = kep2cart(self.analytical_initial_state_kep, mu)
            self.analytical_h_norm, self.analytical_h = cart2angular_momentum(self.analytical_initial_state_cart) # I have to use cartesian in order to have h in array type
            self.analytical_e = kep2energy(initial_state[0], mu)
        else:
            print('WRONG TYPE IN LOAD ANALYTICAL')
        self.analytical_M_init = true2mean_anomaly(self.analytical_initial_state_kep[1], self.analytical_initial_state_kep[5])


    ### Utility ###
        
    def get_simulation(self, type):
        if type.lower() == 'docks':
            SIM = self.Simulations_DOCKS
        elif type.lower() == 'tudat':
            SIM = self.Simulations_TUDAT
        elif type.lower() == 'gmat':
            SIM = self.Simulations_GMAT
        elif type.lower() == 'freeflyer':
            SIM = self.Simulations_FF
        return SIM
    
    def create_folder(self, name):
        # Create Folder if it doesnt exists
        if not os.path.isdir(name):
            os.makedirs(name)
    
    def read_states_file(self, type, sim):
        # DOCKS READ
        if type.lower() == 'docks':
            # Simulation output file path
            output_file = self.output_DOCKS + sim  + '.txt'

            # Read output File Docks
            _, _, _, _, data_time, data_science = read_ccsds(output_file,separator='\t')
            states_sim = data_science[:,:6] # m and s
            time, time_flight = mjd_2col2seconds(data_time)
            time_flight_day = time_flight/3600/24
            acc_rep = data_science[1:,6:9] # m and s
        
        # TUDAT READ
        elif type.lower() == 'tudat':
            # Simulation output file path
            output_file = self.output_TUDAT + sim + '.txt'

            # Read output File TUDAT
            data_states = get_tudat_file(output_file)
            time = data_states[:,0]
            time_flight = time - time[0]
            time_flight_day = time_flight/3600/24
            states_sim = data_states[:,1:7] # m and s
            acc_rep = data_states[1:,7:10] # m and s
        
        # GMAT READ
        elif type.lower() == 'gmat':
            # Simulation output file path
            output_file_eph = self.output_GMAT + sim + '-eph' + '.txt'
            output_file_rep = self.output_GMAT + sim + '-rep' + '.txt'

            # Read output File GMAT
            _, _, _, _, data_time, data_science = read_ccsds(output_file_eph, time_format='ISOT-UTC', separator='\t')
            astro_time_utc = Time(data_time, format="isot", scale='utc')
            astro_time_tdb = astro_time_utc.tdb.copy().value
            time = Time(astro_time_tdb, format="isot", scale='tdb').mjd * 3600*24 # s
            time_flight = time - time[0]
            time_flight_day = time_flight/3600/24
            data_rep = read_gmat_rep(output_file_rep, first_col='string time')
            acc_rep = data_rep[1:,6:9] * 1000 # m and s
            if self.GMAT_O == 'eph':
                states_sim = data_science * 1000 # m and s
            elif self.GMAT_O == 'rep':
                states_sim = data_rep[:,:6] * 1000 # m and s

        # FREEFLYER READ
        elif type.lower() == 'freeflyer':
            # Simulation output file path
            output_file = self.output_FF + sim + '.txt'

            # Read output File FREEFLYER
            data_time, data_states = read_ff_rep(output_file)
            astro_time_tai = Time(data_time, format="isot", scale='tai')
            astro_time_tdb = astro_time_tai.tdb.copy().value
            time = Time(astro_time_tdb, format="isot", scale='tdb').mjd * 3600*24 # s
            time_flight = time - time[0]
            time_flight_day = time_flight/3600/24
            states_sim = data_states[:,:6] * 1000 # m and s
            acc_rep = data_states[1:,6:9] * 1000 # m and s
        
        return time, time_flight, time_flight_day, states_sim, acc_rep

    def interpolate_states_cubic(self, time, states, new_time, acc=[], with_acc=1):
        fr = interp1d(time, states[:,:3], kind='cubic', fill_value="extrapolate", axis=0)  # m
        fv = interp1d(time, states[:,3:6], kind='cubic', fill_value="extrapolate", axis=0)  # m/s
        new_states = np.hstack(( fr(new_time) , fv(new_time) ))
        if with_acc:
            fa = interp1d(time[1:], acc, kind='cubic', fill_value="extrapolate", axis=0)  # m/s/s
            new_acc = fa(new_time[1:])
            return 0, len(new_states), new_states, new_acc
        else:
            return 0, len(new_states), new_states

    def interpolate_states_lagrange(self, time, states, new_time, acc=[], with_acc=1):
        # Lagrange 8th order interpolation from tudat
        import tudatpy
        from tudatpy.kernel.math import interpolators

        state_dict = {t:s for t,s in zip(time, states)}
        interp = interpolators.create_one_dimensional_vector_interpolator(state_dict,
                                                                         interpolators.lagrange_interpolation(8))

        # start and end time due to interpolation
        time_interp_1 = time[4]
        time_interp_2 = time[-4]
        start_time_interp = min(time_interp_1, time_interp_2)
        end_time_interp = max(time_interp_1, time_interp_2)
        if new_time[1] - new_time[0] < 0:
            indE = np.argmax(new_time<start_time_interp)
            ind0 = np.argmax(new_time<=end_time_interp)
        else:
            ind0 = np.argmax(new_time>start_time_interp)
            indE = np.argmax(new_time>=end_time_interp)

        new_states = []
        for t in new_time[ind0:indE]:
            new_states.append(interp.interpolate(t))
        new_states = np.array(new_states)

        if with_acc:
            acc_dict = {t:a for t,a in zip(time[1:], acc)}
            interp = interpolators.create_one_dimensional_vector_interpolator(acc_dict,
                                                                         interpolators.lagrange_interpolation(8))
            new_acc = []
            for t in new_time[ind0+1:indE]:
                new_acc.append(interp.interpolate(t))
            new_acc = np.array(new_acc)
            return ind0, indE, new_states, new_acc
        else:
            return ind0, indE, new_states

    def compute_analytical(self, time_flight, type='time', states=[]):
        if type == 'time':
            th_ana, _ = time2true_anomaly(time_flight, 
                                   self.analytical_initial_state_kep[5],
                                   self.analytical_initial_state_kep[0], 
                                   self.analytical_initial_state_kep[1], 
                                   self.analytical_mu)
        elif type == 'true_anomaly':
            kep_states = cart2kep(states, self.analytical_mu)
            th_ana = kep_states[:,-1]
        # Get Analytical Keplerian
        kep_ana = np.zeros((len(th_ana),6))
        kep_ana[:,0] += self.analytical_initial_state_kep[0]
        kep_ana[:,1] += self.analytical_initial_state_kep[1]
        kep_ana[:,2] += self.analytical_initial_state_kep[2]
        kep_ana[:,3] += self.analytical_initial_state_kep[3]
        kep_ana[:,4] += self.analytical_initial_state_kep[4]
        kep_ana[:,5] = th_ana
        # Get Semi-Analytical Cartesian
        states_ana = kep2cart(kep_ana, self.analytical_mu)
        states_ana[0] = self.analytical_initial_state_cart
        return states_ana, kep_ana

    def compute_analytical_acceleration(self, position_vector):
        sat_mu = self.G * self.probe_mass
        acceleration = (- self.analytical_mu * position_vector / np.linalg.norm(position_vector, axis=1).reshape((-1,1))**3) * (1 + sat_mu / self.analytical_mu)
        return acceleration

    def compute_analytical_mean_anomaly(self, time_flight):
        M_ana = self.analytical_M_init + np.sqrt(self.analytical_mu / self.analytical_initial_state_kep[0]**3) * time_flight
        M_ana = M_ana%(2*np.pi) # 0-2pi
        return M_ana

    def compute_forw_back_residual(self, states_forw, states_back):
        return states_forw[0] - states_back[-1]

    def compute_RTC(self, states_forw, states_back):
        residual = self.compute_forw_back_residual(states_forw, states_back)
        residual_r = residual[:3]
        residual_v = residual[3:]
        r_0 = states_forw[0][:3]
        v_0 = states_forw[0][3:]
        RTC_r = np.linalg.norm(residual_r) / np.linalg.norm(r_0)
        RTC_v = np.linalg.norm(residual_v) / np.linalg.norm(v_0)
        RTC = 0.5 * (RTC_r + RTC_v)
        return RTC

    def outer_ellipsoid_fit(self, points, tol=0.001):
        """
        Taken from https://github.com/rmsandu/Ellipsoid-Fit/blob/main/outer_ellipsoid.py
        Find the minimum volume ellipsoid enclosing (outside) a set of points.
        Return A, c where the equation for the ellipse given in "center form" is
        (x-c).T * A * (x-c) = 1
        D -> 1 / [a,b,c]**2
        V -> Rotation Matrix from normal axes to Ellipsoid Axis
        """
        points = np.asmatrix(points)
        N, d = points.shape
        Q = np.column_stack((points, np.ones(N))).T
        u = np.ones(N) / N
        err = 1 + tol
        while err > tol:
            X = Q * np.diag(u) * Q.T
            M = np.diag(Q.T * np.linalg.inv(X) * Q)
            jdx = np.argmax(M)
            step_size = (M[jdx] - d - 1.0) / ((d + 1) * (M[jdx] - 1.0))
            new_u = (1 - step_size) * u
            new_u[jdx] += step_size
            err = np.linalg.norm(new_u - u)
            u = new_u

        c = u * points  # center of ellipsoid
        A = np.linalg.inv(points.T * np.diag(u) * points - c.T * c) / d

        _, D, V = np.linalg.svd(np.asarray(A))
        axes = 1. / np.sqrt(D)

        return np.squeeze(np.asarray(c)), axes, V

    def get_xyz_ellipsoid(self, center, axes, R):
        # Get Ellipsoid in Origin with no Rotation
        colat = np.linspace(0, np.pi, 15)
        long = np.linspace(0, 2*np.pi, 30)
        COLAT, LONG = np.meshgrid(colat, long)
        x = np.cos(LONG) * np.sin(COLAT) * axes[0]
        y = np.sin(LONG) * np.sin(COLAT) * axes[1]
        z = np.cos(COLAT) * axes[2]
        # Rotate and Translate Ellipsoid
        x, y, z = np.dot(np.array([x.flatten(), y.flatten(), z.flatten()]).T, R).T + center.reshape(3,1)

        return [x.reshape((15,30)), y.reshape((15,30)), z.reshape((15,30))]

    def read_residuals_ellipsoid(self, sim, reference_ellipsoid_path):
        # Files Path
        residuals_path = reference_ellipsoid_path + f'/{sim}_coordinates.txt'
        ellipsoid_path = reference_ellipsoid_path + f'/{sim}_ellipsoid.txt'

        # Get Residuals Coordinates
        with open(residuals_path) as f:
            lines = f.readlines()
        data = [line.split()[1:] for line in lines[2:]]
        residuals_coordinates = np.array(data,dtype=float)

        # Get Ellipsoid Parameters
        with open(ellipsoid_path) as f:
            lines = f.readlines()
        ellipsoid_parameters = np.array(lines[2].split(),dtype=float)

        return residuals_coordinates, ellipsoid_parameters

    def get_number_rate_function_calls(self, sim, encoding=True):
        filepath = self.output_DOCKS + f'LOGS/{sim}.txt'
        if encoding:
            with open(filepath, encoding='utf-16') as f:
                lines = f.readlines()
        else:
            with open(filepath) as f:
                lines = f.readlines()
        # Search line with rate_function
        for line in lines:
            if '(rate_function)' in line:
                ncalls = int(line.split()[0])
                return ncalls
        return None

    def read_performance_front_back_info(self, file):
        # Read Performance File
        with open(file) as f:
            lines = f.readlines()
        data = [line.split()[1:] for line in lines[1:]]
        performance_info = np.array(data,dtype=float)
        # Get Names with Tolerances
        names = [line.split()[0] for line in lines[1:]]
        # Get Tolerances
        tolerances = [n.split('(')[1].split(')')[0] for n in names]
        tolerances = np.array(tolerances,dtype=int)

        return names, performance_info, tolerances
    
    def read_ncalls_reference(self, file):
        with open(file+'_ncalls.txt') as f:
            lines = f.readlines()
        return int(lines[0].split()[2])

    def read_rtc_reference(self, file):
        with open(file+'_rtc.txt') as f:
            lines = f.readlines()
        return float(lines[0].split()[2])

    def check_inside_ellipsoid(self, center, axes, R, current_residual):
        # Rotate and Translate to Ellipsoid Axes
        translated_residual = current_residual - center
        rotated_residual = np.dot(translated_residual, np.linalg.inv(R))

        # Check If Inside Ellipsoid
        inside_check = (rotated_residual[0] / axes[0])**2 + (rotated_residual[1] / axes[1])**2 + (rotated_residual[2] / axes[2])**2 <= 1

        return inside_check

    # ToCalculate=[Cartesian, AngularMomentum&Enerygy, KeplerianElements]
    def compute_residuals_softwares(self, data_d, data_t, 
                          interpolation='no interpolation', ToCalculate=[1,1,1], interp_type='lagrange'):
        if interp_type.lower() == 'lagrange':
            interpp = self.interpolate_states_lagrange
        elif interp_type.lower() == 'cubic':
            interpp = self.interpolate_states_cubic
        
        # Output
        CART_OUT = {key: None for key in ['states_residuals','r_residuals_norm','v_residuals_norm','acceleration_residuals','a_residuals_norm']}
        HE_OUT = {key: None for key in ['h_residuals','h_residuals_norm','e_residuals','e_residuals_norm']}
        KEP_OUT = {key: None for key in ['kep_residuals', 'M_residuals']}
        OUTPUT = {key: None for key in ['time_plot','cartesian','h_e','keplerian']}
        # SIM D
        time_d, time_flight_d, time_flight_day_d, states_sim_d, acc_rep_d = data_d
        # SIM T
        time_t, time_flight_t, time_flight_day_t, states_sim_t, acc_rep_t = data_t

        # Interpolation
        if interpolation != 'no interpolation':
            if interpolation == 0:
                ind0, indE, states_sim_d_new, acc_rep_d_new = interpp(time_d, states_sim_d, time_t, acc=acc_rep_d, with_acc=1)
                states_sim_t_new = copy.deepcopy(states_sim_t[ind0:indE])
                acc_rep_t_new = copy.deepcopy(acc_rep_t[ind0:indE-1])
                OUTPUT['time_plot'] = time_flight_day_t[ind0:indE]
            elif interpolation == 1:
                ind0, indE, states_sim_t_new, acc_rep_t_new = interpp(time_t, states_sim_t, time_d, acc=acc_rep_t, with_acc=1)
                states_sim_d_new = copy.deepcopy(states_sim_d[ind0:indE])
                acc_rep_d_new = copy.deepcopy(acc_rep_d[ind0:indE-1])
                OUTPUT['time_plot'] = time_flight_day_d[ind0:indE]
        else:
            OUTPUT['time_plot'] = time_flight_day_d
            states_sim_d_new = copy.deepcopy(states_sim_d)
            states_sim_t_new = copy.deepcopy(states_sim_t)
            acc_rep_d_new = copy.deepcopy(acc_rep_d)
            acc_rep_t_new = copy.deepcopy(acc_rep_t)

        # Compute Cartesian residual
        if ToCalculate[0]:
            CART_OUT['states_residuals'] = states_sim_d_new - states_sim_t_new
            CART_OUT['r_residuals_norm'] = np.linalg.norm(CART_OUT['states_residuals'][:,:3], axis=1)
            CART_OUT['v_residuals_norm'] = np.linalg.norm(CART_OUT['states_residuals'][:,3:6], axis=1)
            CART_OUT['acceleration_residuals'] = acc_rep_d_new - acc_rep_t_new
            CART_OUT['a_residuals_norm'] = np.linalg.norm(CART_OUT['acceleration_residuals'], axis=1)
            OUTPUT['cartesian'] = CART_OUT
        
        # Compute Energy and Angular Momentum Residuals
        if ToCalculate[1]:
            _, h_sim_d = cart2angular_momentum(states_sim_d_new)
            _, h_sim_t = cart2angular_momentum(states_sim_t_new)
            HE_OUT['h_residuals'] = h_sim_d - h_sim_t
            HE_OUT['h_residuals_norm'] = np.linalg.norm(HE_OUT['h_residuals'], axis=1)
            e_sim_d = cart2energy(states_sim_d_new, self.analytical_mu)
            e_sim_t = cart2energy(states_sim_t_new, self.analytical_mu)
            HE_OUT['e_residuals'] = e_sim_d - e_sim_t
            HE_OUT['e_residuals_norm'] = abs(HE_OUT['e_residuals'])
            OUTPUT['h_e'] = HE_OUT

        # Compute Kepler Elements Residuals
        if ToCalculate[2]:
            kep_sim_d = cart2kep(states_sim_d_new, self.analytical_mu)
            kep_sim_t = cart2kep(states_sim_t_new, self.analytical_mu)
            M_d = true2mean_anomaly(kep_sim_d[:,1], kep_sim_d[:,5])
            M_t = true2mean_anomaly(kep_sim_t[:,1], kep_sim_t[:,5])
            KEP_OUT['kep_residuals'] = kep_sim_d - kep_sim_t
            KEP_OUT['M_residuals'] = M_d - M_t
            OUTPUT['keplerian'] = KEP_OUT
        
        return OUTPUT

    ### Analysis ###

    # BENCHMARK
    # FIGS: [BENCHMARK_R_RESIDUALS, R_RESIDUALS, BENCHMARK_V_RESIDUALS, V_RESIDUALS, BENCHMARK_H_RESIDUALS, H_RESIDUALS, 
    #        BENCHMARK_E_RESIDUALS, E_RESIDUALS, TS_TO_COMPARE]
    def benchmark(self, type, time_steps, fig_folder_name, method, 
                  name_sample='[-]', time_steps_compare=[], FIGS=[1,0,0,0,0,0,0,0,0]):

        if FIGS[0] or FIGS[2] or FIGS[4] or FIGS[6] or FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}'
            self.create_folder(fig_folder)
            # Create Figure: BENCHMARK_R_RESIDUALS
            if FIGS[0]:
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
            # Create Figure: BENCHMARK_V_RESIDUALS
            if FIGS[2]:
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
            # Create Figure: BENCHMARK_H_RESIDUALS
            if FIGS[4]:
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
            # Create Figure: BENCHMARK_E_RESIDUALS
            if FIGS[6]:
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()
            # Create Figure: BENCHMARK_TS_TO_COMPARE
            if FIGS[8]:
                # TS_TO_COMPARE: R
                fig9 = plt.figure(figsize=(10,5))
                ax9 = plt.axes()
                # TS_TO_COMPARE: V
                fig10 = plt.figure(figsize=(10,5))
                ax10 = plt.axes()
                # TS_TO_COMPARE: H
                fig11 = plt.figure(figsize=(10,5))
                ax11 = plt.axes()
                # TS_TO_COMPARE: E
                fig12 = plt.figure(figsize=(10,5))
                ax12 = plt.axes()
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/COMPARE_TS/NORMAL_SCALE'
            self.create_folder(fig_folder)
            fig_folder = f'./Plots/{fig_folder_name}/COMPARE_TS/LOG_SCALE'
            self.create_folder(fig_folder)

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Check if all elements in time_steps_compare are in time_steps
        if not all(element in time_steps for element in time_steps_compare):
            print('INVALID TERMS IN "time_steps_compare"!')
        
        # Parameters to compute
        compute_cart = 1
        compute_h_e = FIGS[4] or FIGS[5] or FIGS[6] or FIGS[7] or FIGS[8]
        compute_kep = 0
        to_compute = [compute_cart, compute_h_e, compute_kep]

        # List of half time_steps
        time_steps_2 = [tss/2 for tss in time_steps]

        max_error_r_list = []
        max_error_v_list = []
        max_error_h_list = []
        max_error_e_list = []
        ts_compare_list = [[], [], [], [], []] # t,r,v,h,e
        # Loop over time steps
        for ts, ts2 in zip(time_steps, time_steps_2):
            ts_sim = name_sample.replace('[-]',f'Benchmark-{print_int_float(ts)}')
            ts2_sim = name_sample.replace('[-]',f'Benchmark-{print_int_float(ts2)}')

            # SIM ts
            data_ts = self.read_states_file(type, ts_sim)
            time_ts, _, time_flight_day_ts = data_ts[:3]
            # SIM ts2
            data_ts2 = self.read_states_file(type, ts2_sim)
            time_ts2, _, time_flight_day_ts2 = data_ts2[:3]

            # Compute Residuals
            RESIDUALS = self.compute_residuals_softwares(data_ts, data_ts2, 
                                                            interpolation=1, ToCalculate=to_compute, interp_type='cubic')

            # Organize Residual Data
            time_plot = RESIDUALS['time_plot']
            r_diff_norm = RESIDUALS['cartesian']['r_residuals_norm']
            max_error_r = max(r_diff_norm)
            max_error_r_list.append(max_error_r)
            v_diff_norm = RESIDUALS['cartesian']['v_residuals_norm']
            max_error_v = max(v_diff_norm)
            max_error_v_list.append(max_error_v)
            if compute_h_e:
                h_diff_norm = RESIDUALS['h_e']['h_residuals_norm']
                max_error_h = max(h_diff_norm)
                max_error_h_list.append(max_error_h)
                e_diff_norm = RESIDUALS['h_e']['e_residuals_norm']
                max_error_e = max(e_diff_norm)
                max_error_e_list.append(max_error_e)

            # Save TS Compare data
            if ts in time_steps_compare:
                ts_compare_list[0].append(time_plot)
                ts_compare_list[1].append(r_diff_norm)
                ts_compare_list[2].append(v_diff_norm)
                ts_compare_list[3].append(h_diff_norm)
                ts_compare_list[4].append(e_diff_norm)

            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, r_diff_norm)
                ax2.set_xlabel('Time [days]')
                ax2.set_ylabel('Position Residual [m]')
                fig2.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'R_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: V_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = plt.axes()
                ax4.plot(time_plot, v_diff_norm)
                ax4.set_xlabel('Time [days]')
                ax4.set_ylabel('Velocity Residual [m/s]')
                fig4.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig4.tight_layout(pad=0.4)
                fig4.savefig(fig_folder+f'V_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, h_diff_norm)
                ax6.set_xlabel('Time [days]')
                ax6.set_ylabel('Angular Momentum Residual [m^2/s]')
                fig6.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'H_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: E_RESIDUALS
                fig8 = plt.figure(figsize=(10,5))
                ax8 = plt.axes()
                ax8.plot(time_plot, e_diff_norm)
                ax8.set_xlabel('Time [days]')
                ax8.set_ylabel('Energy Residual [m^2/s^2]')
                fig8.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig8.tight_layout(pad=0.4)
                fig8.savefig(fig_folder+f'E_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
        
        if FIGS[0]:
            ax1.scatter(time_steps, max_error_r_list, marker='*')
            ax1.plot(time_steps, max_error_r_list)
            ax1.grid()
            ax1.set_xlabel('Time Step [s]')
            ax1.set_ylabel('Max Position Residual [m]')
            ax1.set_xscale('log')
            ax1.set_yscale('log')
            fig1.suptitle(f'[{type}][{method}]')
            fig1.tight_layout(pad=0.4)
            fig1.savefig(fig_folder+f'BENCHMARK_R[{type}][{method}].png', dpi = 300)
        
        if FIGS[2]:
            ax3.scatter(time_steps, max_error_v_list, marker='*')
            ax3.plot(time_steps, max_error_v_list)
            ax3.grid()
            ax3.set_xlabel('Time Step [s]')
            ax3.set_ylabel('Max Velocity Residual [m/s]')
            ax3.set_xscale('log')
            ax3.set_yscale('log')
            fig3.suptitle(f'[{type}][{method}]')
            fig3.tight_layout(pad=0.4)
            fig3.savefig(fig_folder+f'BENCHMARK_V[{type}][{method}].png', dpi = 300)
        
        if FIGS[4]:
            ax5.scatter(time_steps, max_error_h_list, marker='*')
            ax5.plot(time_steps, max_error_h_list)
            ax5.grid()
            ax5.set_xlabel('Time Step [s]')
            ax5.set_ylabel('Max Angular Momentum Residual [m^2/s]')
            ax5.set_xscale('log')
            ax5.set_yscale('log')
            fig5.suptitle(f'[{type}][{method}]')
            fig5.tight_layout(pad=0.4)
            fig5.savefig(fig_folder+f'BENCHMARK_H[{type}][{method}].png', dpi = 300)
        
        if FIGS[6]:
            ax7.scatter(time_steps, max_error_e_list, marker='*')
            ax7.plot(time_steps, max_error_e_list)
            ax7.grid()
            ax7.set_xlabel('Time Step [s]')
            ax7.set_ylabel('Max Energy Residual [m^2/s^2]')
            ax7.set_xscale('log')
            ax7.set_yscale('log')
            fig7.suptitle(f'[{type}][{method}]')
            fig7.tight_layout(pad=0.4)
            fig7.savefig(fig_folder+f'BENCHMARK_E[{type}][{method}].png', dpi = 300)
        
        if FIGS[8]:
            figs_ts_compare = [fig9, fig10, fig11, fig12]
            ax_ts_compare = [ax9, ax10, ax11, ax12]
            y_axis = ['Position Residual [m]', 'Velocity Residual [m/s]', 'Angular Momentum Residual [m^2/s]', 'Energy Residual [m^2/s^2]']
            save = ['R', 'V', 'H', 'E']
            for jj, (fg, ax, y_ax, s) in enumerate(zip(figs_ts_compare, ax_ts_compare, y_axis, save)):
                for tsc, tim, param in zip(time_steps_compare, ts_compare_list[0], ts_compare_list[jj+1]):
                    ax.plot(tim, param, label=f'ts={print_int_float(tsc)}s')
                ax.set_xlabel('Time [days]')
                ax.set_ylabel(y_ax)
                ax.legend(loc=0)
                fg.suptitle(f'[{type}][{method}]')
                fg.tight_layout(pad=0.4)
                fg.savefig(fig_folder+f'COMPARE_TS/NORMAL_SCALE/BENCHMARK_{s}[{type}][{method}].png', dpi = 300)
                ax.set_yscale('log')
                fg.suptitle(f'[{type}][{method}]')
                fg.tight_layout(pad=0.4)
                fg.savefig(fig_folder+f'COMPARE_TS/LOG_SCALE/BENCHMARK_{s}[{type}][{method}].png', dpi = 300)


    # BENCHMARK WITH RESPECT TO SEMI-ANALYTICAL SOLUTION
    # FIGS: [BENCHMARK_R_RESIDUALS, R_RESIDUALS, BENCHMARK_V_RESIDUALS, V_RESIDUALS, BENCHMARK_H_RESIDUALS, H_RESIDUALS, 
    #        BENCHMARK_E_RESIDUALS, E_RESIDUALS, TS_TO_COMPARE]
    def benchmark_wrt_anlytical(self, type, time_steps, fig_folder_name, method, 
                                name_sample='[-]', time_steps_compare=[], FIGS=[1,0,0,0,0,0,0,0,0]):

        if FIGS[0] or FIGS[2] or FIGS[4] or FIGS[6] or FIGS[8] :
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}'
            self.create_folder(fig_folder)
            # Create Figure: BENCHMARK_R_RESIDUALS
            if FIGS[0]:
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
            # Create Figure: BENCHMARK_V_RESIDUALS
            if FIGS[2]:
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
            # Create Figure: BENCHMARK_H_RESIDUALS
            if FIGS[4]:
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
            # Create Figure: BENCHMARK_E_RESIDUALS
            if FIGS[6]:
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()
            # Create Figure: BENCHMARK_TS_TO_COMPARE
            if FIGS[8]:
                # TS_TO_COMPARE: R
                fig9 = plt.figure(figsize=(10,5))
                ax9 = plt.axes()
                # TS_TO_COMPARE: V
                fig10 = plt.figure(figsize=(10,5))
                ax10 = plt.axes()
                # TS_TO_COMPARE: H
                fig11 = plt.figure(figsize=(10,5))
                ax11 = plt.axes()
                # TS_TO_COMPARE: E
                fig12 = plt.figure(figsize=(10,5))
                ax12 = plt.axes()
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/COMPARE_TS/NORMAL_SCALE'
            self.create_folder(fig_folder)
            fig_folder = f'./Plots/{fig_folder_name}/COMPARE_TS/LOG_SCALE'
            self.create_folder(fig_folder)

        # Check if all elements in time_steps_compare are in time_steps
        if not all(element in time_steps for element in time_steps_compare):
            print('INVALID TERMS IN "time_steps_compare"!')
        
        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        max_error_r_list = []
        max_error_v_list = []
        max_error_h_list = []
        max_error_e_list = []
        ts_compare_list = [[], [], [], [], []] # t,r,v,h,e
        # Loop over time steps
        for ts in time_steps:
            ts_sim = name_sample.replace('[-]',f'Benchmark-{print_int_float(ts)}')
            # SIM ts
            time_ts, time_flight_ts, time_flight_day_ts, states_sim_ts, acc_rep_ts = self.read_states_file(type, ts_sim)
            time_plot = time_flight_day_ts
            # Get Semi-Analytical Solution
            state_analytical,_ = self.compute_analytical(time_flight_ts, type='time')
            if FIGS[0] or FIGS[1] or FIGS[8]:
                # Compute postion norm
                r_ts = states_sim_ts[:,:3]
                r_analytical = state_analytical[:,:3]
                r_diff = r_ts - r_analytical
                r_diff_norm = np.linalg.norm(r_diff, axis=1)
                max_error_r = max(r_diff_norm)
                max_error_r_list.append(max_error_r)
            if FIGS[2] or FIGS[3] or FIGS[8]:
                # Compute velocity residuals norm
                v_ts = states_sim_ts[:,3:6]
                v_analytical = state_analytical[:,3:6]
                v_diff = v_ts - v_analytical
                v_diff_norm = np.linalg.norm(v_diff, axis=1)
                max_error_v = max(v_diff_norm)
                max_error_v_list.append(max_error_v)
            if FIGS[4] or FIGS[5] or FIGS[8]:
                # Compute angular momentum residuals norm
                _, h_ts = cart2angular_momentum(states_sim_ts)
                h_diff = h_ts - self.analytical_h
                h_diff_norm = np.linalg.norm(h_diff, axis=1)
                max_error_h = max(h_diff_norm)
                max_error_h_list.append(max_error_h)
            if FIGS[6] or FIGS[7] or FIGS[8]:
                # Compute energy residuals norm
                e_ts = cart2energy(states_sim_ts, self.analytical_mu)
                e_diff = e_ts - self.analytical_e
                e_diff_norm = abs(e_diff)
                max_error_e = max(e_diff_norm)
                max_error_e_list.append(max_error_e)
            
            # Save TS COMPARE data
            if ts in time_steps_compare:
                ts_compare_list[0].append(time_plot)
                ts_compare_list[1].append(r_diff_norm)
                ts_compare_list[2].append(v_diff_norm)
                ts_compare_list[3].append(h_diff_norm)
                ts_compare_list[4].append(e_diff_norm)

            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, r_diff_norm)
                ax2.set_xlabel('Time [days]')
                ax2.set_ylabel('Position Residual [m]')
                fig2.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'R_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: V_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = plt.axes()
                ax4.plot(time_plot, v_diff_norm)
                ax4.set_xlabel('Time [days]')
                ax4.set_ylabel('Velocity Residual [m/s]')
                fig4.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig4.tight_layout(pad=0.4)
                fig4.savefig(fig_folder+f'V_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, h_diff_norm)
                ax6.set_xlabel('Time [days]')
                ax6.set_ylabel('Angular Momentum Residual [m^2/s]')
                fig6.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'H_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: E_RESIDUALS
                fig8 = plt.figure(figsize=(10,5))
                ax8 = plt.axes()
                ax8.plot(time_plot, e_diff_norm)
                ax8.set_xlabel('Time [days]')
                ax8.set_ylabel('Energy Residual [m^2/s^2]')
                fig8.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig8.tight_layout(pad=0.4)
                fig8.savefig(fig_folder+f'E_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
        
        if FIGS[0]:
            ax1.scatter(time_steps, max_error_r_list, marker='*')
            ax1.plot(time_steps, max_error_r_list)
            ax1.grid()
            ax1.set_xlabel('Time Step [s]')
            ax1.set_ylabel('Max Position Residual [m]')
            ax1.set_xscale('log')
            ax1.set_yscale('log')
            fig1.suptitle(f'[{type}][{method}]')
            fig1.tight_layout(pad=0.4)
            fig1.savefig(fig_folder+f'BENCHMARK_R[{type}][{method}].png', dpi = 300)
        
        if FIGS[2]:
            ax3.scatter(time_steps, max_error_v_list, marker='*')
            ax3.plot(time_steps, max_error_v_list)
            ax3.grid()
            ax3.set_xlabel('Time Step [s]')
            ax3.set_ylabel('Max Velocity Residual [m/s]')
            ax3.set_xscale('log')
            ax3.set_yscale('log')
            fig3.suptitle(f'[{type}][{method}]')
            fig3.tight_layout(pad=0.4)
            fig3.savefig(fig_folder+f'BENCHMARK_V[{type}][{method}].png', dpi = 300)
        
        if FIGS[4]:
            ax5.scatter(time_steps, max_error_h_list, marker='*')
            ax5.plot(time_steps, max_error_h_list)
            ax5.grid()
            ax5.set_xlabel('Time Step [s]')
            ax5.set_ylabel('Max Angular Momentum Residual [m^2/s]')
            ax5.set_xscale('log')
            ax5.set_yscale('log')
            fig5.suptitle(f'[{type}][{method}]')
            fig5.tight_layout(pad=0.4)
            fig5.savefig(fig_folder+f'BENCHMARK_H[{type}][{method}].png', dpi = 300)
        
        if FIGS[6]:
            ax7.scatter(time_steps, max_error_e_list, marker='*')
            ax7.plot(time_steps, max_error_e_list)
            ax7.grid()
            ax7.set_xlabel('Time Step [s]')
            ax7.set_ylabel('Max Energy Residual [m^2/s^2]')
            ax7.set_xscale('log')
            ax7.set_yscale('log')
            fig7.suptitle(f'[{type}][{method}]')
            fig7.tight_layout(pad=0.4)
            fig7.savefig(fig_folder+f'BENCHMARK_E[{type}][{method}].png', dpi = 300)
        
        if FIGS[8]:
            figs_ts_compare = [fig9, fig10, fig11, fig12]
            ax_ts_compare = [ax9, ax10, ax11, ax12]
            y_axis = ['Position Residual [m]', 'Velocity Residual [m/s]', 'Angular Momentum Residual [m^2/s]', 'Energy Residual [m^2/s^2]']
            save = ['R', 'V', 'H', 'E']
            for jj, (fg, ax, y_ax, s) in enumerate(zip(figs_ts_compare, ax_ts_compare, y_axis, save)):
                for tsc, tim, param in zip(time_steps_compare, ts_compare_list[0], ts_compare_list[jj+1]):
                    ax.plot(tim, param, label=f'ts={print_int_float(tsc)}s')
                ax.set_xlabel('Time [days]')
                ax.set_ylabel(y_ax)
                ax.legend(loc=0)
                fg.suptitle(f'[{type}][{method}]')
                fg.tight_layout(pad=0.4)
                fg.savefig(fig_folder+f'COMPARE_TS/NORMAL_SCALE/BENCHMARK_{s}[{type}][{method}].png', dpi = 300)
                ax.set_yscale('log')
                fg.suptitle(f'[{type}][{method}]')
                fg.tight_layout(pad=0.4)
                fg.savefig(fig_folder+f'COMPARE_TS/LOG_SCALE/BENCHMARK_{s}[{type}][{method}].png', dpi = 300)


    # BENCHMARK WITH RESPECT TO REFRENCE
    # FIGS: [BENCHMARK_R_RESIDUALS, R_RESIDUALS, BENCHMARK_V_RESIDUALS, V_RESIDUALS, BENCHMARK_H_RESIDUALS, H_RESIDUALS, 
    #        BENCHMARK_E_RESIDUALS, E_RESIDUALS, TS_TO_COMPARE]
    def benchmark_wrt_reference(self, type, time_steps, fig_folder_name, method, reference,
                  name_sample='[-]', time_steps_compare=[], FIGS=[1,0,0,0,0,0,0,0,0]):

        if FIGS[0] or FIGS[2] or FIGS[4] or FIGS[6] or FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}'
            self.create_folder(fig_folder)
            # Create Figure: BENCHMARK_R_RESIDUALS
            if FIGS[0]:
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
            # Create Figure: BENCHMARK_V_RESIDUALS
            if FIGS[2]:
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
            # Create Figure: BENCHMARK_H_RESIDUALS
            if FIGS[4]:
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
            # Create Figure: BENCHMARK_E_RESIDUALS
            if FIGS[6]:
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()
            # Create Figure: BENCHMARK_TS_TO_COMPARE
            if FIGS[8]:
                # TS_TO_COMPARE: R
                fig9 = plt.figure(figsize=(10,5))
                ax9 = plt.axes()
                # TS_TO_COMPARE: V
                fig10 = plt.figure(figsize=(10,5))
                ax10 = plt.axes()
                # TS_TO_COMPARE: H
                fig11 = plt.figure(figsize=(10,5))
                ax11 = plt.axes()
                # TS_TO_COMPARE: E
                fig12 = plt.figure(figsize=(10,5))
                ax12 = plt.axes()
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/COMPARE_TS/NORMAL_SCALE'
            self.create_folder(fig_folder)
            fig_folder = f'./Plots/{fig_folder_name}/COMPARE_TS/LOG_SCALE'
            self.create_folder(fig_folder)

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Check if all elements in time_steps_compare are in time_steps
        if not all(element in time_steps for element in time_steps_compare):
            print('INVALID TERMS IN "time_steps_compare"!')
        
        # Parameters to compute
        compute_cart = 1
        compute_h_e = FIGS[4] or FIGS[5] or FIGS[6] or FIGS[7] or FIGS[8]
        compute_kep = 0
        to_compute = [compute_cart, compute_h_e, compute_kep]

        # Get Reference
        data_ref = self.read_states_file(type, reference)

        max_error_r_list = []
        max_error_v_list = []
        max_error_h_list = []
        max_error_e_list = []
        ts_compare_list = [[], [], [], [], []] # t,r,v,h,e
        # Loop over time steps
        for ts in time_steps:
            ts_sim = name_sample.replace('[-]',f'Benchmark-{print_int_float(ts)}')

            # SIM ts
            data_ts = self.read_states_file(type, ts_sim)
            time_ts, _, time_flight_day_ts = data_ts[:3]

            # Compute Residuals
            RESIDUALS = self.compute_residuals_softwares(data_ts, data_ref, 
                                                            interpolation=1, ToCalculate=to_compute, interp_type='lagrange')

            # Organize Residual Data
            time_plot = RESIDUALS['time_plot']
            r_diff_norm = RESIDUALS['cartesian']['r_residuals_norm']
            max_error_r = max(r_diff_norm)
            max_error_r_list.append(max_error_r)
            v_diff_norm = RESIDUALS['cartesian']['v_residuals_norm']
            max_error_v = max(v_diff_norm)
            max_error_v_list.append(max_error_v)
            if compute_h_e:
                h_diff_norm = RESIDUALS['h_e']['h_residuals_norm']
                max_error_h = max(h_diff_norm)
                max_error_h_list.append(max_error_h)
                e_diff_norm = RESIDUALS['h_e']['e_residuals_norm']
                max_error_e = max(e_diff_norm)
                max_error_e_list.append(max_error_e)

            # Save TS Compare data
            if ts in time_steps_compare:
                ts_compare_list[0].append(time_plot)
                ts_compare_list[1].append(r_diff_norm)
                ts_compare_list[2].append(v_diff_norm)
                ts_compare_list[3].append(h_diff_norm)
                ts_compare_list[4].append(e_diff_norm)

            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, r_diff_norm)
                ax2.set_xlabel('Time [days]')
                ax2.set_ylabel('Position Residual [m]')
                fig2.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'R_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: V_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = plt.axes()
                ax4.plot(time_plot, v_diff_norm)
                ax4.set_xlabel('Time [days]')
                ax4.set_ylabel('Velocity Residual [m/s]')
                fig4.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig4.tight_layout(pad=0.4)
                fig4.savefig(fig_folder+f'V_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, h_diff_norm)
                ax6.set_xlabel('Time [days]')
                ax6.set_ylabel('Angular Momentum Residual [m^2/s]')
                fig6.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'H_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: E_RESIDUALS
                fig8 = plt.figure(figsize=(10,5))
                ax8 = plt.axes()
                ax8.plot(time_plot, e_diff_norm)
                ax8.set_xlabel('Time [days]')
                ax8.set_ylabel('Energy Residual [m^2/s^2]')
                fig8.suptitle(f'[{type}][{method}] - ts = {print_int_float(ts)} s')
                fig8.tight_layout(pad=0.4)
                fig8.savefig(fig_folder+f'E_RESIDUALS/[{type}][{method}]-{print_int_float(ts)}.png', dpi = 300)
        
        if FIGS[0]:
            ax1.scatter(time_steps, max_error_r_list, marker='*')
            ax1.plot(time_steps, max_error_r_list)
            ax1.grid()
            ax1.set_xlabel('Time Step [s]')
            ax1.set_ylabel('Max Position Residual [m]')
            ax1.set_xscale('log')
            ax1.set_yscale('log')
            fig1.suptitle(f'[{type}][{method}]')
            fig1.tight_layout(pad=0.4)
            fig1.savefig(fig_folder+f'BENCHMARK_R[{type}][{method}].png', dpi = 300)
        
        if FIGS[2]:
            ax3.scatter(time_steps, max_error_v_list, marker='*')
            ax3.plot(time_steps, max_error_v_list)
            ax3.grid()
            ax3.set_xlabel('Time Step [s]')
            ax3.set_ylabel('Max Velocity Residual [m/s]')
            ax3.set_xscale('log')
            ax3.set_yscale('log')
            fig3.suptitle(f'[{type}][{method}]')
            fig3.tight_layout(pad=0.4)
            fig3.savefig(fig_folder+f'BENCHMARK_V[{type}][{method}].png', dpi = 300)
        
        if FIGS[4]:
            ax5.scatter(time_steps, max_error_h_list, marker='*')
            ax5.plot(time_steps, max_error_h_list)
            ax5.grid()
            ax5.set_xlabel('Time Step [s]')
            ax5.set_ylabel('Max Angular Momentum Residual [m^2/s]')
            ax5.set_xscale('log')
            ax5.set_yscale('log')
            fig5.suptitle(f'[{type}][{method}]')
            fig5.tight_layout(pad=0.4)
            fig5.savefig(fig_folder+f'BENCHMARK_H[{type}][{method}].png', dpi = 300)
        
        if FIGS[6]:
            ax7.scatter(time_steps, max_error_e_list, marker='*')
            ax7.plot(time_steps, max_error_e_list)
            ax7.grid()
            ax7.set_xlabel('Time Step [s]')
            ax7.set_ylabel('Max Energy Residual [m^2/s^2]')
            ax7.set_xscale('log')
            ax7.set_yscale('log')
            fig7.suptitle(f'[{type}][{method}]')
            fig7.tight_layout(pad=0.4)
            fig7.savefig(fig_folder+f'BENCHMARK_E[{type}][{method}].png', dpi = 300)
        
        if FIGS[8]:
            figs_ts_compare = [fig9, fig10, fig11, fig12]
            ax_ts_compare = [ax9, ax10, ax11, ax12]
            y_axis = ['Position Residual [m]', 'Velocity Residual [m/s]', 'Angular Momentum Residual [m^2/s]', 'Energy Residual [m^2/s^2]']
            save = ['R', 'V', 'H', 'E']
            for jj, (fg, ax, y_ax, s) in enumerate(zip(figs_ts_compare, ax_ts_compare, y_axis, save)):
                for tsc, tim, param in zip(time_steps_compare, ts_compare_list[0], ts_compare_list[jj+1]):
                    ax.plot(tim, param, label=f'ts={print_int_float(tsc)}s')
                ax.set_xlabel('Time [days]')
                ax.set_ylabel(y_ax)
                ax.legend(loc=0)
                fg.suptitle(f'[{type}][{method}]')
                fg.tight_layout(pad=0.4)
                fg.savefig(fig_folder+f'COMPARE_TS/NORMAL_SCALE/BENCHMARK_{s}[{type}][{method}].png', dpi = 300)
                ax.set_yscale('log')
                fg.suptitle(f'[{type}][{method}]')
                fg.tight_layout(pad=0.4)
                fg.savefig(fig_folder+f'COMPARE_TS/LOG_SCALE/BENCHMARK_{s}[{type}][{method}].png', dpi = 300)


    # CREATE COMPARISON BETWEEN SOFTWARES
    # FIGS: [STATES_RESIDUALS, R_RESIDUALS, V_RESIDUALS, A_RESIDUALS, H&E_RESIDUALS, H_RESIDUALS, 
    #        E_RESIDUALS, TIME_STEP, STATES_RESIDUALS_ELEMWISE, KEPLER_RESIDUALS_ELEMWISE]
    def compare_softwares(self, sim_d_list, sim_t_list, fig_folder_name, type_d, type_t, 
                          interpolation='no interpolation', FIGS=[1,0,0,0,1,0,0,0,0,1]):
        SIM_D = self.get_simulation(type_d)
        SIM_T = self.get_simulation(type_t)

        if FIGS[0]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[2]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/A_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[4]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H&E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[6]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/TIME_STEP'
            self.create_folder(fig_folder)

        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[9]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/KEPLER_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
    

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Parameters to compute
        compute_cart = 1
        compute_h_e = FIGS[4] or FIGS[5] or FIGS[6]
        compute_kep = FIGS[9]
        to_compute = [compute_cart, compute_h_e, compute_kep]

        # Loop through each Simulation
        for ii, (sim_d, sim_t) in enumerate(zip(sim_d_list, sim_t_list)):

            # SIM D
            data_d = self.read_states_file(type_d, sim_d)
            time_d, _, time_flight_day_d = data_d[:3]
            # SIM T
            data_t = self.read_states_file(type_t, sim_t)
            time_t, _, time_flight_day_t = data_t[:3]

            # Compute Residuals
            if interpolation != 'no interpolation':
                RESIDUALS = self.compute_residuals_softwares(data_d, data_t, 
                                                             interpolation=interpolation[ii], ToCalculate=to_compute)
            else:
                RESIDUALS = self.compute_residuals_softwares(data_d, data_t, 
                                                             interpolation=interpolation, ToCalculate=to_compute)
            
            # Organize Residual Data
            time_plot = RESIDUALS['time_plot']
            dcart_vec = RESIDUALS['cartesian']['states_residuals']
            dr = RESIDUALS['cartesian']['r_residuals_norm']
            dv = RESIDUALS['cartesian']['v_residuals_norm']
            da_vec = RESIDUALS['cartesian']['acceleration_residuals']
            da = RESIDUALS['cartesian']['a_residuals_norm']
            if compute_h_e:
                dh = RESIDUALS['h_e']['h_residuals_norm']
                de = RESIDUALS['h_e']['e_residuals_norm']
            if compute_kep:
                kep_diff = RESIDUALS['keplerian']['kep_residuals']

            if FIGS[0]:
                # Create Figure: STATES_RESIDUALS
                fig0 = plt.figure(figsize=(10,5))
                ax0 = []
                for jj in range(3):
                    ax0.append(plt.subplot(1,3,jj+1))
                ax0[0].plot(time_plot, dr)
                ax0[1].plot(time_plot, dv)
                ax0[2].plot(time_plot[1:], da)
                ax0[0].set_ylabel('Position Residuals [m]')
                ax0[0].set_xlabel('Time [days]')
                ax0[1].set_ylabel('Velocity Residuals [m/s]')
                ax0[1].set_xlabel('Time [days]')
                ax0[2].set_ylabel('Acceleration Residual [m/s^2]')
                ax0[2].set_xlabel('Time [days]')
                fig0.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig0.tight_layout(pad=0.4)
                #fig0.subplots_adjust(wspace=0.35)
                fig0.savefig(fig_folder+f'STATES_RESIDUALS/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
                ax1.plot(time_plot, dr)
                ax1.set_ylabel('Position Residual [m]')
                ax1.set_xlabel('Time [days]')
                fig1.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig1.tight_layout(pad=0.4)
                fig1.savefig(fig_folder+f'R_RESIDUALS/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[2]:
                # Create Figure: V_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, dv)
                ax2.set_ylabel('Velocity Residual [m/s]')
                ax2.set_xlabel('Time [days]')
                fig2.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'V_RESIDUALS/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: A_RESIDUALS
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
                ax3.plot(time_plot[1:], da)
                ax3.set_ylabel('Acceleration Residual [m/s^2]')
                ax3.set_xlabel('Time [days]')
                fig3.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig3.tight_layout(pad=0.4)
                fig3.savefig(fig_folder+f'A_RESIDUALS/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[4]:
                # Create Figure: H&E_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = []
                for jj in range(2):
                    ax4.append(plt.subplot(1,2,jj+1))
                ax4[0].plot(time_plot, dh)
                ax4[1].plot(time_plot, de)
                ax4[0].set_ylabel('Angular Momentum Residual [m^2/s]')
                ax4[0].set_xlabel('Time [days]')
                ax4[1].set_ylabel('Energy Residual [m^2/s^2]')
                ax4[1].set_xlabel('Time [days]')
                fig4.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig4.tight_layout(pad=0.4)
                #fig4.subplots_adjust(wspace=0.35)
                fig4.savefig(fig_folder+f'H&E_RESIDUALS/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
                ax5.plot(time_plot, dh)
                ax5.set_ylabel('Angular Momentum Residual [m^2/s]')
                ax5.set_xlabel('Time [days]')
                fig5.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig5.tight_layout(pad=0.4)
                fig5.savefig(fig_folder+f'H_RESIDUALS/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[6]:
                # Create Figure: E_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, de)
                ax6.set_ylabel('Energy Residual [m^2/s^2]')
                ax6.set_xlabel('Time [days]')
                fig6.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'E_RESIDUALS/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: TIME_STEP
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()
                ax7.plot(time_flight_day_d[:-1], time_d[1:]-time_d[:-1], '-b', label=f'{SIM_D[sim_d]["name"]}')
                ax7.plot(time_flight_day_t[:-1], time_t[1:]-time_t[:-1], '--r', label=f'{SIM_T[sim_t]["name"]}')
                ax7.set_ylabel('Time Step [s]')
                ax7.set_xlabel('Time [days]')
                ax7.legend(loc=0)
                fig7.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig7.tight_layout(pad=0.4)
                fig7.savefig(fig_folder+f'TIME_STEP/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[8]:
                # Create Figure: STATES_RESIDUALS_ELEMWISE
                fig8 = plt.figure(figsize=(10,5))
                ax8 = []
                for jj in range(9):
                    ax8.append(plt.subplot(3,3,jj+1))
                for jjj in range(6):
                    ax8[jjj].plot(time_plot, dcart_vec[:,jjj])
                for jjj in range(6,9):
                    ax8[jjj].plot(time_plot[1:], da_vec[:,jjj-6])
                ax8[0].set_ylabel('dx [m]')
                ax8[1].set_ylabel('dy [m]')
                ax8[2].set_ylabel('dz [m]')
                ax8[3].set_ylabel('dvx [m/s]')
                ax8[4].set_ylabel('dvy [m/s]')
                ax8[5].set_ylabel('dvz [m/s]')
                ax8[6].set_ylabel('dax [m/s^2]')
                ax8[7].set_ylabel('day [m/s^2]')
                ax8[8].set_ylabel('daz [m/s^2]')
                ax8[6].set_xlabel('Time [days]')
                ax8[7].set_xlabel('Time [days]')
                ax8[8].set_xlabel('Time [days]')
                fig8.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig8.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig8.savefig(fig_folder+f'STATES_RESIDUALS_ELEMWISE/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[9]:
                # Create Figure: KEPLER_RESIDUALS_ELEMWISE
                fig9 = plt.figure(figsize=(10,5))
                ax9 = []
                for jj in range(6):
                    ax9.append(plt.subplot(3,2,jj+1))
                for jjj in range(6):
                    ax9[jjj].plot(time_plot, kep_diff[:,jjj])
                ax9[0].set_ylabel('$\Delta$a [m]')
                ax9[1].set_ylabel('$\Delta$e [-]')
                ax9[2].set_ylabel('$\Delta$i [rad]')
                ax9[3].set_ylabel('$\Delta$$\omega$ [rad]')
                ax9[4].set_ylabel('$\Delta$$\Omega$ [rad]')
                # ax9[5].set_ylabel('$\Delta$M [rad]')
                ax9[5].set_ylabel(r'$\Delta$$\theta$ [rad]')
                ax9[3].set_xlabel('Time [days]')
                ax9[4].set_xlabel('Time [days]')
                ax9[5].set_xlabel('Time [days]')
                fig9.suptitle(f'{SIM_D[sim_d]["name"]} vs {SIM_T[sim_t]["name"]}')
                fig9.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig9.savefig(fig_folder+f'KEPLER_RESIDUALS_ELEMWISE/{SIM_D[sim_d]["name"]}-{SIM_T[sim_t]["name"]}.png', dpi = 300)


    # CREATE COMPARISON BETWEEN FRONTWARD AND BACKWARD PROPAGATIONS
    # FIGS: [STATES_RESIDUALS, R_RESIDUALS, V_RESIDUALS, A_RESIDUALS, H&E_RESIDUALS, H_RESIDUALS, 
    #        E_RESIDUALS, TIME_STEP, STATES_RESIDUALS_ELEMWISE, KEPLER_RESIDUALS_ELEMWISE, INTEGRATOR_PERFORMANCE]
    def compare_forward_backward(self, sim_ref, tolerances, fig_folder_name, type, integrator='', type_integrator='varstep',
                          interpolation='no interpolation', output_dir='./Outputs/PERFORMANCE/', FIGS=[1,0,0,0,1,0,0,0,0,1,1]):
        SIM = self.get_simulation(type)

        if FIGS[0]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[2]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/A_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[4]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H&E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[6]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/TIME_STEP'
            self.create_folder(fig_folder)

        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[9]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/KEPLER_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[10]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/INTEGRATOR_PERFORMANCE/RTC'
            self.create_folder(fig_folder)
            fig10 = plt.figure(figsize=(10,5))
            ax10 = plt.axes()
            fig_folder = f'./Plots/{fig_folder_name}/INTEGRATOR_PERFORMANCE/RESIDUAL'
            self.create_folder(fig_folder)
            fig11 = plt.figure(figsize=(10,5))
            ax11 = plt.axes()
    

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Create Output folder
        self.create_folder(output_dir)

        # Create Performance File
        with open(f'{output_dir}{integrator}_performance_{type_integrator}_front_back.txt', 'w') as fo:
                fo.write(f'{"INTEGRATOR":^15}{"RTC":^25}{"RESIDUAL |R| m":^25}{"RESIDUAL |V| m/s":^25}{"NCALLS":^15}{"RESIDUAL [X] m":^25}{"RESIDUAL [Y] m":^25}{"RESIDUAL [Z] m":^25}{"RESIDUAL [VX] m/s":^25}{"RESIDUAL [VY] m/s":^25}{"RESIDUAL [VZ] m/s":^25}{"NCALLS FRONT":^15}{"NCALLS BACK":^15}\n')

        # Parameters to compute
        compute_cart = 1
        compute_h_e = FIGS[4] or FIGS[5] or FIGS[6]
        compute_kep = FIGS[9]
        to_compute = [compute_cart, compute_h_e, compute_kep]

        # Loop through each Simulation
        ncalls_list = []
        RTC_list = []
        dr_norm_list = []
        for ii, (tol) in enumerate(tolerances):
            sim_d = sim_ref.replace('[tol]', f'{tol}')
            sim_d = sim_d.replace('[-]', 'front')
            sim_t = sim_ref.replace('[tol]', f'{tol}')
            sim_t = sim_t.replace('[-]', 'back')

            # SIM D
            data_d = self.read_states_file(type, sim_d)
            time_d, _, time_flight_day_d = data_d[:3]
            # SIM T
            data_t = self.read_states_file(type, sim_t)
            time_t, _, time_flight_day_t = data_t[:3]

            # Compute Residuals
            if interpolation != 'no interpolation':
                RESIDUALS = self.compute_residuals_softwares(data_d, data_t, 
                                                             interpolation=interpolation, ToCalculate=to_compute)
            else:
                data_t_invert = []
                for i in range(len(data_t)):
                    data_t_invert.append(data_t[i][::-1])
                RESIDUALS = self.compute_residuals_softwares(data_d, data_t_invert, 
                                                             interpolation=interpolation, ToCalculate=to_compute)
            
            # Organize Residual Data
            time_plot = RESIDUALS['time_plot']
            dcart_vec = RESIDUALS['cartesian']['states_residuals']
            dr = RESIDUALS['cartesian']['r_residuals_norm']
            dv = RESIDUALS['cartesian']['v_residuals_norm']
            da_vec = RESIDUALS['cartesian']['acceleration_residuals']
            da = RESIDUALS['cartesian']['a_residuals_norm']
            if compute_h_e:
                dh = RESIDUALS['h_e']['h_residuals_norm']
                de = RESIDUALS['h_e']['e_residuals_norm']
            if compute_kep:
                kep_diff = RESIDUALS['keplerian']['kep_residuals']
            
            # Compute State(t0) residual and RTC
            residual_t0 = self.compute_forw_back_residual(data_d[3], data_t[3])
            dr_norm_list.append(np.linalg.norm(residual_t0[:3]))
            RTC = self.compute_RTC(data_d[3], data_t[3])
            RTC_list.append(RTC)
            # print(f'Residual(t0) = {np.linalg.norm(residual_t0[:3]):.15e} m, {np.linalg.norm(residual_t0[3:]):.15e} m/s; RTC = {RTC:.15e}')

            # Get Number of rate_function calls
            ncalls_d = self.get_number_rate_function_calls(sim_d)
            ncalls_t = self.get_number_rate_function_calls(sim_t)
            ncalls = int((ncalls_d + ncalls_t) / 2)
            ncalls_list.append(ncalls)
            
            # Save Info in File
            with open(f'{output_dir}{integrator}_performance_{type_integrator}_front_back.txt', 'a') as fo:
                lab = f'{integrator}({tol})'
                fo.write(f'{lab:^15}{RTC:^25.15e}{np.linalg.norm(residual_t0[:3]):^25.15e}{np.linalg.norm(residual_t0[3:]):^25.15e}{ncalls:^15}{residual_t0[0]:^25.15e}{residual_t0[1]:^25.15e}{residual_t0[2]:^25.15e}{residual_t0[3]:^25.15e}{residual_t0[4]:^25.15e}{residual_t0[5]:^25.15e}{ncalls_d:^15}{ncalls_t:^15}\n')

            if FIGS[0]:
                # Create Figure: STATES_RESIDUALS
                fig0 = plt.figure(figsize=(10,5))
                ax0 = []
                for jj in range(3):
                    ax0.append(plt.subplot(1,3,jj+1))
                ax0[0].plot(time_plot, dr)
                ax0[1].plot(time_plot, dv)
                ax0[2].plot(time_plot[1:], da)
                ax0[0].set_ylabel('Position Residuals [m]')
                ax0[0].set_xlabel('Time [days]')
                ax0[1].set_ylabel('Velocity Residuals [m/s]')
                ax0[1].set_xlabel('Time [days]')
                ax0[2].set_ylabel('Acceleration Residual [m/s^2]')
                ax0[2].set_xlabel('Time [days]')
                fig0.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig0.tight_layout(pad=0.4)
                #fig0.subplots_adjust(wspace=0.35)
                fig0.savefig(fig_folder+f'STATES_RESIDUALS/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
                ax1.plot(time_plot, dr)
                ax1.set_ylabel('Position Residual [m]')
                ax1.set_xlabel('Time [days]')
                fig1.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig1.tight_layout(pad=0.4)
                fig1.savefig(fig_folder+f'R_RESIDUALS/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[2]:
                # Create Figure: V_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, dv)
                ax2.set_ylabel('Velocity Residual [m/s]')
                ax2.set_xlabel('Time [days]')
                fig2.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'V_RESIDUALS/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: A_RESIDUALS
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
                ax3.plot(time_plot[1:], da)
                ax3.set_ylabel('Acceleration Residual [m/s^2]')
                ax3.set_xlabel('Time [days]')
                fig3.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig3.tight_layout(pad=0.4)
                fig3.savefig(fig_folder+f'A_RESIDUALS/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[4]:
                # Create Figure: H&E_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = []
                for jj in range(2):
                    ax4.append(plt.subplot(1,2,jj+1))
                ax4[0].plot(time_plot, dh)
                ax4[1].plot(time_plot, de)
                ax4[0].set_ylabel('Angular Momentum Residual [m^2/s]')
                ax4[0].set_xlabel('Time [days]')
                ax4[1].set_ylabel('Energy Residual [m^2/s^2]')
                ax4[1].set_xlabel('Time [days]')
                fig4.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig4.tight_layout(pad=0.4)
                #fig4.subplots_adjust(wspace=0.35)
                fig4.savefig(fig_folder+f'H&E_RESIDUALS/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
                ax5.plot(time_plot, dh)
                ax5.set_ylabel('Angular Momentum Residual [m^2/s]')
                ax5.set_xlabel('Time [days]')
                fig5.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig5.tight_layout(pad=0.4)
                fig5.savefig(fig_folder+f'H_RESIDUALS/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[6]:
                # Create Figure: E_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, de)
                ax6.set_ylabel('Energy Residual [m^2/s^2]')
                ax6.set_xlabel('Time [days]')
                fig6.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'E_RESIDUALS/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: TIME_STEP
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()
                ax7.plot(time_flight_day_d[:-1], time_d[1:]-time_d[:-1], '-b', label=f'{SIM[sim_d]["name"]}')
                ax7.plot(time_flight_day_t[:-1], time_t[1:]-time_t[:-1], '--r', label=f'{SIM[sim_t]["name"]}')
                ax7.set_ylabel('Time Step [s]')
                ax7.set_xlabel('Time [days]')
                ax7.legend(loc=0)
                fig7.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig7.tight_layout(pad=0.4)
                fig7.savefig(fig_folder+f'TIME_STEP/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[8]:
                # Create Figure: STATES_RESIDUALS_ELEMWISE
                fig8 = plt.figure(figsize=(10,5))
                ax8 = []
                for jj in range(9):
                    ax8.append(plt.subplot(3,3,jj+1))
                for jjj in range(6):
                    ax8[jjj].plot(time_plot, dcart_vec[:,jjj])
                for jjj in range(6,9):
                    ax8[jjj].plot(time_plot[1:], da_vec[:,jjj-6])
                ax8[0].set_ylabel('dx [m]')
                ax8[1].set_ylabel('dy [m]')
                ax8[2].set_ylabel('dz [m]')
                ax8[3].set_ylabel('dvx [m/s]')
                ax8[4].set_ylabel('dvy [m/s]')
                ax8[5].set_ylabel('dvz [m/s]')
                ax8[6].set_ylabel('dax [m/s^2]')
                ax8[7].set_ylabel('day [m/s^2]')
                ax8[8].set_ylabel('daz [m/s^2]')
                ax8[6].set_xlabel('Time [days]')
                ax8[7].set_xlabel('Time [days]')
                ax8[8].set_xlabel('Time [days]')
                fig8.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig8.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig8.savefig(fig_folder+f'STATES_RESIDUALS_ELEMWISE/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)
            
            if FIGS[9]:
                # Create Figure: KEPLER_RESIDUALS_ELEMWISE
                fig9 = plt.figure(figsize=(10,5))
                ax9 = []
                for jj in range(6):
                    ax9.append(plt.subplot(3,2,jj+1))
                for jjj in range(6):
                    ax9[jjj].plot(time_plot, kep_diff[:,jjj])
                ax9[0].set_ylabel('$\Delta$a [m]')
                ax9[1].set_ylabel('$\Delta$e [-]')
                ax9[2].set_ylabel('$\Delta$i [rad]')
                ax9[3].set_ylabel('$\Delta$$\omega$ [rad]')
                ax9[4].set_ylabel('$\Delta$$\Omega$ [rad]')
                # ax9[5].set_ylabel('$\Delta$M [rad]')
                ax9[5].set_ylabel(r'$\Delta$$\theta$ [rad]')
                ax9[3].set_xlabel('Time [days]')
                ax9[4].set_xlabel('Time [days]')
                ax9[5].set_xlabel('Time [days]')
                fig9.suptitle(f'{SIM[sim_d]["name"]} vs {SIM[sim_t]["name"]}')
                fig9.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig9.savefig(fig_folder+f'KEPLER_RESIDUALS_ELEMWISE/{SIM[sim_d]["name"]}-{SIM[sim_t]["name"]}.png', dpi = 300)

        if FIGS[10]:
            # Plot Figures: Performance RTC
            ax10.plot(ncalls_list, RTC_list, 'b-o')
            ax10.grid()
            ax10.set_xlabel('Number of function evaluations [-]')
            ax10.set_ylabel('RTC [-]')
            ax10.set_xscale('log')
            ax10.set_yscale('log')
            fig10.suptitle(f'{integrator} Performance - RTC')
            fig10.tight_layout(pad=0.4)
            fig10.savefig(fig_folder+f'INTEGRATOR_PERFORMANCE/RTC/{integrator}.png', dpi = 300)
            # Plot Figures: Performance Residual
            ax11.plot(ncalls_list, dr_norm_list, 'b-o')
            ax11.grid()
            ax11.set_xlabel('Number of rate_function calls [-]')
            ax11.set_ylabel('dr($t_0$) [m]')
            ax11.set_xscale('log')
            ax11.set_yscale('log')
            fig11.suptitle(f'{integrator} Performance - Position Residual at $t_0$')
            fig11.tight_layout(pad=0.4)
            fig11.savefig(fig_folder+f'INTEGRATOR_PERFORMANCE/RESIDUAL/{integrator}.png', dpi = 300)
        
        # Print Slopes
        print(f'{integrator.upper()} FORWARD BACKWARD SLOPE: {(np.log(dr_norm_list[-1])-np.log(dr_norm_list[0]))/(np.log(ncalls_list[-1])-np.log(ncalls_list[0]))}')


    # CREATE COMPARISON BETWEEN PROPAGATION AND BENCHMARK
    # FIGS: [STATES_RESIDUALS, R_RESIDUALS, V_RESIDUALS, A_RESIDUALS, H&E_RESIDUALS, H_RESIDUALS, 
    #        E_RESIDUALS, TIME_STEP, STATES_RESIDUALS_ELEMWISE, KEPLER_RESIDUALS_ELEMWISE, INTEGRATOR_PERFORMANCE]
    def compare_benchmark(self, benchmark_sim, sims, tolerances, fig_folder_name, type, integrator='', type_integrator='varstep',
                          interpolation='no interpolation', output_dir='./Outputs/PERFORMANCE/', FIGS=[1,0,0,0,1,0,0,0,0,1,1]):
        SIM = self.get_simulation(type)

        if FIGS[0]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[2]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/A_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[4]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H&E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[6]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/TIME_STEP'
            self.create_folder(fig_folder)

        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[9]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/KEPLER_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[10]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/INTEGRATOR_PERFORMANCE/BENCHMARK'
            self.create_folder(fig_folder)
            fig10 = plt.figure(figsize=(10,5))
            ax10 = plt.axes()
    

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Create Output folder
        self.create_folder(output_dir)

        # Create Performance File
        with open(f'{output_dir}{integrator}_performance_{type_integrator}_benchmark.txt', 'w') as fo:
                fo.write(f'{"INTEGRATOR":^15}{"MAX RESIDUAL |R| m":^25}{"NCALLS":^15}\n')

        # Parameters to compute
        compute_cart = 1
        compute_h_e = FIGS[4] or FIGS[5] or FIGS[6]
        compute_kep = FIGS[9]
        to_compute = [compute_cart, compute_h_e, compute_kep]

        # Read Benchmark
        if benchmark_sim.lower() != 'analytical':
            data_sim = self.read_states_file(type, benchmark_sim)
            time_t, _, time_flight_day_t = data_sim[:3]

        # Loop through each Simulation
        ncalls_list = []
        max_dr_norm_list = []
        for sim, tol in zip(sims, tolerances):
            # SIM D
            data_d = self.read_states_file(type, sim)
            time_d, time_flight_d, time_flight_day_d = data_d[:3]

            # Compute Residuals
            if benchmark_sim.lower() != 'analytical':
                if interpolation != 'no interpolation':
                    RESIDUALS = self.compute_residuals_softwares(data_d, data_sim, 
                                                                interpolation=interpolation, ToCalculate=to_compute)
                else:
                    RESIDUALS = self.compute_residuals_softwares(data_d, data_sim, 
                                                                interpolation=1, ToCalculate=to_compute)
                
                # Organize Residual Data
                time_plot = RESIDUALS['time_plot']
                dcart_vec = RESIDUALS['cartesian']['states_residuals']
                dr = RESIDUALS['cartesian']['r_residuals_norm']
                dv = RESIDUALS['cartesian']['v_residuals_norm']
                da_vec = RESIDUALS['cartesian']['acceleration_residuals']
                da = RESIDUALS['cartesian']['a_residuals_norm']
                if compute_h_e:
                    dh = RESIDUALS['h_e']['h_residuals_norm']
                    de = RESIDUALS['h_e']['e_residuals_norm']
                if compute_kep:
                    kep_diff = RESIDUALS['keplerian']['kep_residuals']
            else:
                # Get Semi-Analytical Solution
                state_analytical, kep_analytical = self.compute_analytical(time_flight_d, states=data_d[3], type='time')
                acc_analytical = self.compute_analytical_acceleration(state_analytical[1:,:3])
                time_plot = time_flight_day_d

                # Compute Cartesian residual
                dcart_vec = data_d[3] - state_analytical
                dr = np.linalg.norm(dcart_vec[:,:3], axis=1)
                dv = np.linalg.norm(dcart_vec[:,3:6], axis=1)
                da_vec = data_d[4] - acc_analytical
                da = np.linalg.norm(da_vec, axis=1)
                if compute_h_e:
                    # Compute Energy and Angular Momentum Residuals
                    _, h_sim_d = cart2angular_momentum(data_d[3])
                    dh_vec = h_sim_d - self.analytical_h
                    dh = np.linalg.norm(dh_vec, axis=1)
                    e_sim_d = cart2energy(data_d[3], self.analytical_mu)
                    de_no_abs = e_sim_d - self.analytical_e
                    de = abs(de_no_abs) 
                if compute_kep:
                    # Compute Kepler Elements Residuals
                    kep_sim_d = cart2kep(data_d[3], self.analytical_mu)
                    kep_sim_d[:,5] = true2mean_anomaly(kep_sim_d[:,1], kep_sim_d[:,5])
                    kep_analytical[:,5] = self.compute_analytical_mean_anomaly(time_flight_d)
                    kep_diff = kep_sim_d - kep_analytical

            
            # Get Maximum Position Residual Norm
            max_dr_norm = max(dr)
            max_dr_norm_list.append(max_dr_norm)

            # Get Number of rate_function calls
            ncalls = self.get_number_rate_function_calls(sim)
            ncalls_list.append(ncalls)
            
            # Save Info in File
            with open(f'{output_dir}{integrator}_performance_{type_integrator}_benchmark.txt', 'a') as fo:
                lab = f'{integrator}({tol})'
                fo.write(f'{lab:^15}{max_dr_norm:^25.15e}{ncalls:^15}\n')

            if FIGS[0]:
                # Create Figure: STATES_RESIDUALS
                fig0 = plt.figure(figsize=(10,5))
                ax0 = []
                for jj in range(3):
                    ax0.append(plt.subplot(1,3,jj+1))
                ax0[0].plot(time_plot, dr)
                ax0[1].plot(time_plot, dv)
                ax0[2].plot(time_plot[1:], da)
                ax0[0].set_ylabel('Position Residuals [m]')
                ax0[0].set_xlabel('Time [days]')
                ax0[1].set_ylabel('Velocity Residuals [m/s]')
                ax0[1].set_xlabel('Time [days]')
                ax0[2].set_ylabel('Acceleration Residual [m/s^2]')
                ax0[2].set_xlabel('Time [days]')
                fig0.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig0.tight_layout(pad=0.4)
                #fig0.subplots_adjust(wspace=0.35)
                fig0.savefig(fig_folder+f'STATES_RESIDUALS/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
                ax1.plot(time_plot, dr)
                ax1.set_ylabel('Position Residual [m]')
                ax1.set_xlabel('Time [days]')
                fig1.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig1.tight_layout(pad=0.4)
                fig1.savefig(fig_folder+f'R_RESIDUALS/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[2]:
                # Create Figure: V_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, dv)
                ax2.set_ylabel('Velocity Residual [m/s]')
                ax2.set_xlabel('Time [days]')
                fig2.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'V_RESIDUALS/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: A_RESIDUALS
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
                ax3.plot(time_plot[1:], da)
                ax3.set_ylabel('Acceleration Residual [m/s^2]')
                ax3.set_xlabel('Time [days]')
                fig3.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig3.tight_layout(pad=0.4)
                fig3.savefig(fig_folder+f'A_RESIDUALS/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[4]:
                # Create Figure: H&E_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = []
                for jj in range(2):
                    ax4.append(plt.subplot(1,2,jj+1))
                ax4[0].plot(time_plot, dh)
                ax4[1].plot(time_plot, de)
                ax4[0].set_ylabel('Angular Momentum Residual [m^2/s]')
                ax4[0].set_xlabel('Time [days]')
                ax4[1].set_ylabel('Energy Residual [m^2/s^2]')
                ax4[1].set_xlabel('Time [days]')
                fig4.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig4.tight_layout(pad=0.4)
                #fig4.subplots_adjust(wspace=0.35)
                fig4.savefig(fig_folder+f'H&E_RESIDUALS/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
                ax5.plot(time_plot, dh)
                ax5.set_ylabel('Angular Momentum Residual [m^2/s]')
                ax5.set_xlabel('Time [days]')
                fig5.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig5.tight_layout(pad=0.4)
                fig5.savefig(fig_folder+f'H_RESIDUALS/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[6]:
                # Create Figure: E_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, de)
                ax6.set_ylabel('Energy Residual [m^2/s^2]')
                ax6.set_xlabel('Time [days]')
                fig6.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'E_RESIDUALS/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: TIME_STEP
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()
                ax7.plot(time_flight_day_d[:-1], time_d[1:]-time_d[:-1], '-b', label=f'{SIM[sim]["name"]}')
                ax7.plot(time_flight_day_t[:-1], time_t[1:]-time_t[:-1], '--r', label=f'{SIM[benchmark_sim]["name"]}')
                ax7.set_ylabel('Time Step [s]')
                ax7.set_xlabel('Time [days]')
                ax7.legend(loc=0)
                fig7.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig7.tight_layout(pad=0.4)
                fig7.savefig(fig_folder+f'TIME_STEP/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[8]:
                # Create Figure: STATES_RESIDUALS_ELEMWISE
                fig8 = plt.figure(figsize=(10,5))
                ax8 = []
                for jj in range(9):
                    ax8.append(plt.subplot(3,3,jj+1))
                for jjj in range(6):
                    ax8[jjj].plot(time_plot, dcart_vec[:,jjj])
                for jjj in range(6,9):
                    ax8[jjj].plot(time_plot[1:], da_vec[:,jjj-6])
                ax8[0].set_ylabel('dx [m]')
                ax8[1].set_ylabel('dy [m]')
                ax8[2].set_ylabel('dz [m]')
                ax8[3].set_ylabel('dvx [m/s]')
                ax8[4].set_ylabel('dvy [m/s]')
                ax8[5].set_ylabel('dvz [m/s]')
                ax8[6].set_ylabel('dax [m/s^2]')
                ax8[7].set_ylabel('day [m/s^2]')
                ax8[8].set_ylabel('daz [m/s^2]')
                ax8[6].set_xlabel('Time [days]')
                ax8[7].set_xlabel('Time [days]')
                ax8[8].set_xlabel('Time [days]')
                fig8.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig8.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig8.savefig(fig_folder+f'STATES_RESIDUALS_ELEMWISE/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)
            
            if FIGS[9]:
                # Create Figure: KEPLER_RESIDUALS_ELEMWISE
                fig9 = plt.figure(figsize=(10,5))
                ax9 = []
                for jj in range(6):
                    ax9.append(plt.subplot(3,2,jj+1))
                for jjj in range(6):
                    ax9[jjj].plot(time_plot, kep_diff[:,jjj])
                ax9[0].set_ylabel('$\Delta$a [m]')
                ax9[1].set_ylabel('$\Delta$e [-]')
                ax9[2].set_ylabel('$\Delta$i [rad]')
                ax9[3].set_ylabel('$\Delta$$\omega$ [rad]')
                ax9[4].set_ylabel('$\Delta$$\Omega$ [rad]')
                # ax9[5].set_ylabel('$\Delta$M [rad]')
                ax9[5].set_ylabel(r'$\Delta$$\theta$ [rad]')
                ax9[3].set_xlabel('Time [days]')
                ax9[4].set_xlabel('Time [days]')
                ax9[5].set_xlabel('Time [days]')
                fig9.suptitle(f'{SIM[sim]["name"]} vs {SIM[benchmark_sim]["name"]}')
                fig9.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig9.savefig(fig_folder+f'KEPLER_RESIDUALS_ELEMWISE/{SIM[sim]["name"]}-{SIM[benchmark_sim]["name"]}.png', dpi = 300)

        if FIGS[10]:
            # Plot Figures: Benchmark
            ax10.plot(ncalls_list, max_dr_norm_list, 'b-o')
            ax10.grid()
            ax10.set_xlabel('Number of rate_function calls [-]')
            ax10.set_ylabel('Max |dr| [m]')
            ax10.set_xscale('log')
            ax10.set_yscale('log')
            fig10.suptitle(f'{integrator} Performance - Benchmark')
            fig10.tight_layout(pad=0.4)
            fig10.savefig(fig_folder+f'INTEGRATOR_PERFORMANCE/BENCHMARK/{integrator}.png', dpi = 300)
        
        # Print Slopes
        print(f'{integrator.upper()} BENCHMARK SLOPE: {(np.log(max_dr_norm_list[-1])-np.log(max_dr_norm_list[0]))/(np.log(ncalls_list[-1])-np.log(ncalls_list[0]))}')



    # CREATE COMPARISON BETWEEN SOFTWARE and ANALYTICAL
    # FIGS: [STATES_RESIDUALS, R_RESIDUALS, V_RESIDUALS, A_RESIDUALS, H&E_RESIDUALS, H_RESIDUALS, 
    #        E_RESIDUALS, STATES_RESIDUALS_ELEMWISE, KEPLER_RESIDUALS_ELEMWISE]
    def compare_software_with_analytical(self, sim_d_list, fig_folder_name, type_d, type_analytical='time',
                          FIGS=[1,0,0,0,1,0,0,0,1]):
        SIM_D = self.get_simulation(type_d)

        if FIGS[0]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[2]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/A_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[4]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H&E_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[6]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)

        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/KEPLER_RESIDUALS_ELEMWISE'
            self.create_folder(fig_folder)
    

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Loop through each Simulation
        for ii, sim_d in enumerate(sim_d_list):

            # SIM D
            time_d, time_flight_d, time_flight_day_d, states_sim_d, acc_rep_d = self.read_states_file(type_d, sim_d)
            time_plot = time_flight_day_d

            # Get Semi-Analytical Solution
            state_analytical, kep_analytical = self.compute_analytical(time_flight_d, states=states_sim_d, type=type_analytical)
            acc_analytical = self.compute_analytical_acceleration(state_analytical[1:,:3])

            # Compute Cartesian residual
            dcart_vec = states_sim_d - state_analytical
            dr = np.linalg.norm(dcart_vec[:,:3], axis=1)
            dv = np.linalg.norm(dcart_vec[:,3:6], axis=1)
            da_vec = acc_rep_d - acc_analytical
            da = np.linalg.norm(da_vec, axis=1)

            if FIGS[4] or FIGS[5] or FIGS[6]:
                # Compute Energy and Angular Momentum Residuals
                _, h_sim_d = cart2angular_momentum(states_sim_d)
                dh_vec = h_sim_d - self.analytical_h
                dh = np.linalg.norm(dh_vec, axis=1)
                e_sim_d = cart2energy(states_sim_d, self.analytical_mu)
                de_no_abs = e_sim_d - self.analytical_e
                de = abs(de_no_abs)
            
            if FIGS[8]:
                # Compute Kepler Elements Residuals
                kep_sim_d = cart2kep(states_sim_d, self.analytical_mu)
                kep_sim_d[:,5] = true2mean_anomaly(kep_sim_d[:,1], kep_sim_d[:,5])
                kep_analytical[:,5] = self.compute_analytical_mean_anomaly(time_flight_d)
                kep_diff = kep_sim_d - kep_analytical

            if FIGS[0]:
                # Create Figure: STATES_RESIDUALS
                fig0 = plt.figure(figsize=(10,5))
                ax0 = []
                for jj in range(3):
                    ax0.append(plt.subplot(1,3,jj+1))
                ax0[0].plot(time_plot, dr)
                ax0[1].plot(time_plot, dv)
                ax0[2].plot(time_plot[1:], da)
                ax0[0].set_ylabel('Position Residuals [m]')
                ax0[0].set_xlabel('Time [days]')
                ax0[1].set_ylabel('Velocity Residuals [m/s]')
                ax0[1].set_xlabel('Time [days]')
                ax0[2].set_ylabel('Acceleration Residual [m/s^2]')
                ax0[2].set_xlabel('Time [days]')
                fig0.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical[{type_analytical}]')
                fig0.tight_layout(pad=0.4)
                #fig0.subplots_adjust(wspace=0.35)
                fig0.savefig(fig_folder+f'STATES_RESIDUALS/{SIM_D[sim_d]["name"]}-Analytical[{type_analytical}].png', dpi = 300)
            
            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
                ax1.plot(time_plot, dr)
                ax1.set_ylabel('Position Residual [m]')
                ax1.set_xlabel('Time [days]')
                fig1.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical[{type_analytical}]')
                fig1.tight_layout(pad=0.4)
                fig1.savefig(fig_folder+f'R_RESIDUALS/{SIM_D[sim_d]["name"]}-Analytical[{type_analytical}].png', dpi = 300)
            
            if FIGS[2]:
                # Create Figure: V_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, dv)
                ax2.set_ylabel('Velocity Residual [m/s]')
                ax2.set_xlabel('Time [days]')
                fig2.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical[{type_analytical}]')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'V_RESIDUALS/{SIM_D[sim_d]["name"]}-Analytical[{type_analytical}].png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: A_RESIDUALS
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
                ax3.plot(time_plot[1:], da)
                ax3.set_ylabel('Acceleration Residual [m/s^2]')
                ax3.set_xlabel('Time [days]')
                fig3.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical[{type_analytical}]')
                fig3.tight_layout(pad=0.4)
                fig3.savefig(fig_folder+f'A_RESIDUALS/{SIM_D[sim_d]["name"]}-Analytical[{type_analytical}].png', dpi = 300)
            
            if FIGS[4]:
                # Create Figure: H&E_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = []
                for jj in range(2):
                    ax4.append(plt.subplot(1,2,jj+1))
                ax4[0].plot(time_plot, dh)
                ax4[1].plot(time_plot, de)
                ax4[0].set_ylabel('Angular Momentum Residual [m^2/s]')
                ax4[0].set_xlabel('Time [days]')
                ax4[1].set_ylabel('Energy Residual [m^2/s^2]')
                ax4[1].set_xlabel('Time [days]')
                fig4.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical')
                fig4.tight_layout(pad=0.4)
                #fig4.subplots_adjust(wspace=0.35)
                fig4.savefig(fig_folder+f'H&E_RESIDUALS/{SIM_D[sim_d]["name"]}-Analytical.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
                ax5.plot(time_plot, dh)
                ax5.set_ylabel('Angular Momentum Residual [m^2/s]')
                ax5.set_xlabel('Time [days]')
                fig5.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical')
                fig5.tight_layout(pad=0.4)
                fig5.savefig(fig_folder+f'H_RESIDUALS/{SIM_D[sim_d]["name"]}-Analytical.png', dpi = 300)
            
            if FIGS[6]:
                # Create Figure: E_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, de)
                ax6.set_ylabel('Energy Residual [m^2/s^2]')
                ax6.set_xlabel('Time [days]')
                fig6.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'E_RESIDUALS/{SIM_D[sim_d]["name"]}-Analytical.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: STATES_RESIDUALS_ELEMWISE
                fig7 = plt.figure(figsize=(10,5))
                ax7 = []
                for jj in range(9):
                    ax7.append(plt.subplot(3,3,jj+1))
                for jjj in range(6):
                    ax7[jjj].plot(time_plot, dcart_vec[:,jjj])
                for jjj in range(6,9):
                    ax7[jjj].plot(time_plot[1:], da_vec[:,jjj-6])
                ax7[0].set_ylabel('dx [m]')
                ax7[1].set_ylabel('dy [m]')
                ax7[2].set_ylabel('dz [m]')
                ax7[3].set_ylabel('dvx [m/s]')
                ax7[4].set_ylabel('dvy [m/s]')
                ax7[5].set_ylabel('dvz [m/s]')
                ax7[6].set_ylabel('dax [m/s^2]')
                ax7[7].set_ylabel('day [m/s^2]')
                ax7[8].set_ylabel('daz [m/s^2]')
                ax7[6].set_xlabel('Time [days]')
                ax7[7].set_xlabel('Time [days]')
                ax7[8].set_xlabel('Time [days]')
                fig7.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical[{type_analytical}]')
                fig7.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig7.savefig(fig_folder+f'STATES_RESIDUALS_ELEMWISE/{SIM_D[sim_d]["name"]}-Analytical[{type_analytical}].png', dpi = 300)
            
            if FIGS[8]:
                # Create Figure: KEPLER_RESIDUALS_ELEMWISE
                fig8 = plt.figure(figsize=(10,5))
                ax8 = []
                for jj in range(6):
                    ax8.append(plt.subplot(3,2,jj+1))
                for jjj in range(6):
                    ax8[jjj].plot(time_plot, kep_diff[:,jjj])
                ax8[0].set_ylabel('$\Delta$a [m]')
                ax8[1].set_ylabel('$\Delta$e [-]')
                ax8[2].set_ylabel('$\Delta$i [rad]')
                ax8[3].set_ylabel('$\Delta$$\omega$ [rad]')
                ax8[4].set_ylabel('$\Delta$$\Omega$ [rad]')
                ax8[5].set_ylabel('$\Delta$M [rad]')
                ax8[3].set_xlabel('Time [days]')
                ax8[4].set_xlabel('Time [days]')
                ax8[5].set_xlabel('Time [days]')
                fig8.suptitle(f'{SIM_D[sim_d]["name"]} vs Analytical')
                fig8.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig8.savefig(fig_folder+f'KEPLER_RESIDUALS_ELEMWISE/{SIM_D[sim_d]["name"]}-Analytical.png', dpi = 300)


    # Plot SIMULATIONS
    # FIGS: [STATES, R, V, A, H&E, H, 
    #        E, TIME_STEP, STATES_ELEMWISE, KEPLER_ELEMWISE]
    def plot_simulation(self, sim_list, fig_folder_name, type,
                        FIGS=[1,0,0,0,1,0,0,0,0,1]):
        SIM = self.get_simulation(type)

        if FIGS[0]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES'
            self.create_folder(fig_folder)
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R'
            self.create_folder(fig_folder)
        
        if FIGS[2]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/A'
            self.create_folder(fig_folder)
        
        if FIGS[4]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H&E'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H'
            self.create_folder(fig_folder)
        
        if FIGS[6]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/TIME_STEP'
            self.create_folder(fig_folder)

        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[9]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/KEPLER_ELEMWISE'
            self.create_folder(fig_folder)
    
        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Loop through each Simulation
        for ii, sim in enumerate(sim_list):

            # SIM D
            time, time_flight, time_flight_day, states_sim, acc_rep = self.read_states_file(type, sim)
            time_plot = time_flight_day

            # Compute Cartesian
            r = np.linalg.norm(states_sim[:,:3],axis=1)
            v = np.linalg.norm(states_sim[:,3:6],axis=1)
            a = np.linalg.norm(acc_rep[:,:3],axis=1)

            if FIGS[4] or FIGS[5] or FIGS[6]:
                # Compute Energy and Angular Momentum
                h_sim, h_sim_arr = cart2angular_momentum(states_sim)
                e_sim = cart2energy(states_sim, self.analytical_mu)
            
            if FIGS[9]:
                # Compute Kepler Elements
                kep_sim = cart2kep(states_sim, self.analytical_mu)
                # kep_sim[:,5] = true2mean_anomaly(kep_sim[:,1], kep_sim[:,5])

            if FIGS[0]:
                # Create Figure: STATES
                fig0 = plt.figure(figsize=(10,5))
                ax0 = []
                for jj in range(3):
                    ax0.append(plt.subplot(1,3,jj+1))
                ax0[0].plot(time_plot, r)
                ax0[1].plot(time_plot, v)
                ax0[2].plot(time_plot[1:], a)
                ax0[0].set_ylabel('Position [m]')
                ax0[0].set_xlabel('Time [days]')
                ax0[1].set_ylabel('Velocity [m/s]')
                ax0[1].set_xlabel('Time [days]')
                ax0[2].set_ylabel('Acceleration [m/s^2]')
                ax0[2].set_xlabel('Time [days]')
                fig0.suptitle(f'{SIM[sim]["name"]}')
                fig0.tight_layout(pad=0.4)
                #fig0.subplots_adjust(wspace=0.35)
                fig0.savefig(fig_folder+f'STATES/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[1]:
                # Create Figure: R
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
                ax1.plot(time_plot, r)
                ax1.set_ylabel('Position [m]')
                ax1.set_xlabel('Time [days]')
                fig1.suptitle(f'{SIM[sim]["name"]}')
                fig1.tight_layout(pad=0.4)
                fig1.savefig(fig_folder+f'R/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[2]:
                # Create Figure: V
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, v)
                ax2.set_ylabel('Velocity [m/s]')
                ax2.set_xlabel('Time [days]')
                fig2.suptitle(f'{SIM[sim]["name"]}')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'V/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: A
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
                ax3.plot(time_plot[1:], a)
                ax3.set_ylabel('Acceleration [m/s^2]')
                ax3.set_xlabel('Time [days]')
                fig3.suptitle(f'{SIM[sim]["name"]}')
                fig3.tight_layout(pad=0.4)
                fig3.savefig(fig_folder+f'A/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[4]:
                # Create Figure: H&E
                fig4 = plt.figure(figsize=(10,5))
                ax4 = []
                for jj in range(2):
                    ax4.append(plt.subplot(1,2,jj+1))
                ax4[0].plot(time_plot, h_sim)
                ax4[1].plot(time_plot, e_sim)
                ax4[0].set_ylabel('Angular Momentum [m^2/s]')
                ax4[0].set_xlabel('Time [days]')
                ax4[1].set_ylabel('Energy [m^2/s^2]')
                ax4[1].set_xlabel('Time [days]')
                fig4.suptitle(f'{SIM[sim]["name"]}')
                fig4.tight_layout(pad=0.4)
                #fig4.subplots_adjust(wspace=0.35)
                fig4.savefig(fig_folder+f'H&E/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
                ax5.plot(time_plot, h_sim)
                ax5.set_ylabel('Angular Momentum [m^2/s]')
                ax5.set_xlabel('Time [days]')
                fig5.suptitle(f'{SIM[sim]["name"]}')
                fig5.tight_layout(pad=0.4)
                fig5.savefig(fig_folder+f'H/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[6]:
                # Create Figure: E
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, e_sim)
                ax6.set_ylabel('Energy [m^2/s^2]')
                ax6.set_xlabel('Time [days]')
                fig6.suptitle(f'{SIM[sim]["name"]}')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'E/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: TIME_STEP
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()
                ax7.plot(time_plot[:-1], time[1:]-time[:-1], '-b')
                ax7.set_ylabel('Time Step [s]')
                ax7.set_xlabel('Time [days]')
                fig7.suptitle(f'{SIM[sim]["name"]}')
                fig7.tight_layout(pad=0.4)
                fig7.savefig(fig_folder+f'TIME_STEP/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[8]:
                # Create Figure: STATES_ELEMWISE
                fig8 = plt.figure(figsize=(10,5))
                ax8 = []
                for jj in range(9):
                    ax8.append(plt.subplot(3,3,jj+1))
                for jjj in range(6):
                    ax8[jjj].plot(time_plot, states_sim[:,jjj])
                for jjj in range(6,9):
                    ax8[jjj].plot(time_plot[1:], acc_rep[:,jjj-6])
                ax8[0].set_ylabel('x [m]')
                ax8[1].set_ylabel('y [m]')
                ax8[2].set_ylabel('z [m]')
                ax8[3].set_ylabel('vx [m/s]')
                ax8[4].set_ylabel('vy [m/s]')
                ax8[5].set_ylabel('vz [m/s]')
                ax8[6].set_ylabel('ax [m/s^2]')
                ax8[7].set_ylabel('ay [m/s^2]')
                ax8[8].set_ylabel('az [m/s^2]')
                ax8[6].set_xlabel('Time [days]')
                ax8[7].set_xlabel('Time [days]')
                ax8[8].set_xlabel('Time [days]')
                fig8.suptitle(f'{SIM[sim]["name"]}')
                fig8.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig8.savefig(fig_folder+f'STATES_ELEMWISE/{SIM[sim]["name"]}.png', dpi = 300)
            
            if FIGS[9]:
                # Create Figure: KEPLER_ELEMWISE
                fig9 = plt.figure(figsize=(10,5))
                ax9 = []
                for jj in range(6):
                    ax9.append(plt.subplot(3,2,jj+1))
                for jjj in range(6):
                    ax9[jjj].plot(time_plot, kep_sim[:,jjj])
                ax9[0].set_ylabel('a [m]')
                ax9[1].set_ylabel('e [-]')
                ax9[2].set_ylabel('i [rad]')
                ax9[3].set_ylabel('$\omega$ [rad]')
                ax9[4].set_ylabel('$\Omega$ [rad]')
                # ax9[5].set_ylabel('M [rad]')
                ax9[5].set_ylabel(r'$\theta$ [rad]')
                ax9[3].set_xlabel('Time [days]')
                ax9[4].set_xlabel('Time [days]')
                ax9[5].set_xlabel('Time [days]')
                fig9.suptitle(f'{SIM[sim]["name"]}')
                fig9.tight_layout(pad=0.4)
                #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
                fig9.savefig(fig_folder+f'KEPLER_ELEMWISE/{SIM[sim]["name"]}.png', dpi = 300)
    
    
    # Plot ANALYTICAL
    # FIGS: [STATES, R, V, A, H&E, H, 
    #        E, STATES_ELEMWISE, KEPLER_ELEMWISE]
    def plot_analytical(self, time_flight, title, fig_folder_name, FIGS=[1,0,0,0,1,0,0,0,1]):

        if FIGS[0]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES'
            self.create_folder(fig_folder)
        
        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R'
            self.create_folder(fig_folder)
        
        if FIGS[2]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/A'
            self.create_folder(fig_folder)
        
        if FIGS[4]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H&E'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H'
            self.create_folder(fig_folder)
        
        if FIGS[6]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E'
            self.create_folder(fig_folder)

        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/STATES_ELEMWISE'
            self.create_folder(fig_folder)
        
        if FIGS[8]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/KEPLER_ELEMWISE'
            self.create_folder(fig_folder)
    
        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Get Semi-Analytical Solution
        state_analytical, kep_analytical = self.compute_analytical(time_flight, type='time')
        acc_analytical = self.compute_analytical_acceleration(state_analytical[1:,:3])
        time_plot = time_flight/3600/24 # days

        # Compute Cartesian
        r = np.linalg.norm(state_analytical[:,:3],axis=1)
        v = np.linalg.norm(state_analytical[:,3:6],axis=1)
        a = np.linalg.norm(acc_analytical[:,:3],axis=1)

        # Compute Energy and Angular Momentum
        h = np.zeros(len(time_flight)) + self.analytical_h_norm
        e = np.zeros(len(time_flight)) + self.analytical_e

        if FIGS[0]:
            # Create Figure: STATES
            fig0 = plt.figure(figsize=(10,5))
            ax0 = []
            for jj in range(3):
                ax0.append(plt.subplot(1,3,jj+1))
            ax0[0].plot(time_plot, r)
            ax0[1].plot(time_plot, v)
            ax0[2].plot(time_plot[1:], a)
            ax0[0].set_ylabel('Position [m]')
            ax0[0].set_xlabel('Time [days]')
            ax0[1].set_ylabel('Velocity [m/s]')
            ax0[1].set_xlabel('Time [days]')
            ax0[2].set_ylabel('Acceleration [m/s^2]')
            ax0[2].set_xlabel('Time [days]')
            fig0.suptitle(f'{title}-Analytical')
            fig0.tight_layout(pad=0.4)
            #fig0.subplots_adjust(wspace=0.35)
            fig0.savefig(fig_folder+f'STATES/{title}-Analytical.png', dpi = 300)
        
        if FIGS[1]:
            # Create Figure: R
            fig1 = plt.figure(figsize=(10,5))
            ax1 = plt.axes()
            ax1.plot(time_plot, r)
            ax1.set_ylabel('Position [m]')
            ax1.set_xlabel('Time [days]')
            fig1.suptitle(f'{title}-Analytical')
            fig1.tight_layout(pad=0.4)
            fig1.savefig(fig_folder+f'R/{title}-Analytical.png', dpi = 300)
        
        if FIGS[2]:
            # Create Figure: V
            fig2 = plt.figure(figsize=(10,5))
            ax2 = plt.axes()
            ax2.plot(time_plot, v)
            ax2.set_ylabel('Velocity [m/s]')
            ax2.set_xlabel('Time [days]')
            fig2.suptitle(f'{title}-Analytical')
            fig2.tight_layout(pad=0.4)
            fig2.savefig(fig_folder+f'V/{title}-Analytical.png', dpi = 300)
        
        if FIGS[3]:
            # Create Figure: A
            fig3 = plt.figure(figsize=(10,5))
            ax3 = plt.axes()
            ax3.plot(time_plot[1:], a)
            ax3.set_ylabel('Acceleration [m/s^2]')
            ax3.set_xlabel('Time [days]')
            fig3.suptitle(f'{title}-Analytical')
            fig3.tight_layout(pad=0.4)
            fig3.savefig(fig_folder+f'A/{title}-Analytical.png', dpi = 300)
        
        if FIGS[4]:
            # Create Figure: H&E
            fig4 = plt.figure(figsize=(10,5))
            ax4 = []
            for jj in range(2):
                ax4.append(plt.subplot(1,2,jj+1))
            ax4[0].plot(time_plot, h)
            ax4[1].plot(time_plot, e)
            ax4[0].set_ylabel('Angular Momentum [m^2/s]')
            ax4[0].set_xlabel('Time [days]')
            ax4[1].set_ylabel('Energy [m^2/s^2]')
            ax4[1].set_xlabel('Time [days]')
            fig4.suptitle(f'{title}-Analytical')
            fig4.tight_layout(pad=0.4)
            #fig4.subplots_adjust(wspace=0.35)
            fig4.savefig(fig_folder+f'H&E/{title}-Analytical.png', dpi = 300)
        
        if FIGS[5]:
            # Create Figure: H
            fig5 = plt.figure(figsize=(10,5))
            ax5 = plt.axes()
            ax5.plot(time_plot, h)
            ax5.set_ylabel('Angular Momentum [m^2/s]')
            ax5.set_xlabel('Time [days]')
            fig5.suptitle(f'{title}-Analytical')
            fig5.tight_layout(pad=0.4)
            fig5.savefig(fig_folder+f'H/{title}-Analytical.png', dpi = 300)
        
        if FIGS[6]:
            # Create Figure: E
            fig6 = plt.figure(figsize=(10,5))
            ax6 = plt.axes()
            ax6.plot(time_plot, e)
            ax6.set_ylabel('Energy [m^2/s^2]')
            ax6.set_xlabel('Time [days]')
            fig6.suptitle(f'{title}-Analytical')
            fig6.tight_layout(pad=0.4)
            fig6.savefig(fig_folder+f'E/{title}-Analytical.png', dpi = 300)
        
        if FIGS[7]:
            # Create Figure: STATES_ELEMWISE
            fig8 = plt.figure(figsize=(10,5))
            ax8 = []
            for jj in range(9):
                ax8.append(plt.subplot(3,3,jj+1))
            for jjj in range(6):
                ax8[jjj].plot(time_plot, state_analytical[:,jjj])
            for jjj in range(6,9):
                ax8[jjj].plot(time_plot[1:], acc_analytical[:,jjj-6])
            ax8[0].set_ylabel('x [m]')
            ax8[1].set_ylabel('y [m]')
            ax8[2].set_ylabel('z [m]')
            ax8[3].set_ylabel('vx [m/s]')
            ax8[4].set_ylabel('vy [m/s]')
            ax8[5].set_ylabel('vz [m/s]')
            ax8[6].set_ylabel('ax [m/s^2]')
            ax8[7].set_ylabel('ay [m/s^2]')
            ax8[8].set_ylabel('az [m/s^2]')
            ax8[6].set_xlabel('Time [days]')
            ax8[7].set_xlabel('Time [days]')
            ax8[8].set_xlabel('Time [days]')
            fig8.suptitle(f'{title}-Analytical')
            fig8.tight_layout(pad=0.4)
            #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
            fig8.savefig(fig_folder+f'STATES_ELEMWISE/{title}-Analytical.png', dpi = 300)
        
        if FIGS[8]:
            # Create Figure: KEPLER_ELEMWISE
            fig9 = plt.figure(figsize=(10,5))
            ax9 = []
            for jj in range(6):
                ax9.append(plt.subplot(3,2,jj+1))
            for jjj in range(6):
                ax9[jjj].plot(time_plot, kep_analytical[:,jjj])
            ax9[0].set_ylabel('a [m]')
            ax9[1].set_ylabel('e [-]')
            ax9[2].set_ylabel('i [rad]')
            ax9[3].set_ylabel('$\omega$ [rad]')
            ax9[4].set_ylabel('$\Omega$ [rad]')
            # ax9[5].set_ylabel('M [rad]')
            ax9[5].set_ylabel(r'$\theta$ [rad]')
            ax9[3].set_xlabel('Time [days]')
            ax9[4].set_xlabel('Time [days]')
            ax9[5].set_xlabel('Time [days]')
            fig9.suptitle(f'{title}-Analytical')
            fig9.tight_layout(pad=0.4)
            #fig2.subplots_adjust(hspace=0.289,wspace=0.560, top=0.927)
            fig9.savefig(fig_folder+f'KEPLER_ELEMWISE/{title}-Analytical.png', dpi = 300)


    # BENCHMARK SHERICAL HARMONICS
    # FIGS: [BENCHMARK_SH_R_RESIDUALS, R_RESIDUALS, BENCHMARK_SH_V_RESIDUALS, V_RESIDUALS, BENCHMARK_SH_H_RESIDUALS, H_RESIDUALS, 
    #        BENCHMARK_SH_E_RESIDUALS, E_RESIDUALS]
    def benchmark_sh(self, type, degrees, fig_folder_name, method, compare_to='first_degree',
                  interpolation='no interpolation', name_sample='[-]', LOG=0, FIGS=[1,0,0,0,0,0,0,0]):

        if FIGS[0] or FIGS[2] or FIGS[4] or FIGS[6]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}'
            self.create_folder(fig_folder)
            # Create Figure: BENCHMARK_R_RESIDUALS
            if FIGS[0]:
                fig1 = plt.figure(figsize=(10,5))
                ax1 = plt.axes()
            # Create Figure: BENCHMARK_V_RESIDUALS
            if FIGS[2]:
                fig3 = plt.figure(figsize=(10,5))
                ax3 = plt.axes()
            # Create Figure: BENCHMARK_H_RESIDUALS
            if FIGS[4]:
                fig5 = plt.figure(figsize=(10,5))
                ax5 = plt.axes()
            # Create Figure: BENCHMARK_E_RESIDUALS
            if FIGS[6]:
                fig7 = plt.figure(figsize=(10,5))
                ax7 = plt.axes()

        if FIGS[1]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/R_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[3]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/V_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[5]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/H_RESIDUALS'
            self.create_folder(fig_folder)
        
        if FIGS[7]:
            # Figures Folder
            fig_folder = f'./Plots/{fig_folder_name}/E_RESIDUALS'
            self.create_folder(fig_folder)

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Parameters to compute
        compute_cart = 1
        compute_h_e = FIGS[4] or FIGS[5] or FIGS[6] or FIGS[7]
        compute_kep = 0
        to_compute = [compute_cart, compute_h_e, compute_kep]

        # Read First Degree
        if compare_to == 'first_degree':
            fd_sim = name_sample.replace('[-]',f'Benchmark_SH-{print_int_float(degrees[0])}')
            data_fd = self.read_states_file(type, fd_sim)
            degree_x = degrees[1:]
        # Read Last Degree
        elif compare_to == 'last_degree':
            ld_sim = name_sample.replace('[-]',f'Benchmark_SH-{print_int_float(degrees[-1])}')
            data_ld = self.read_states_file(type, ld_sim)
            degree_x = degrees[:-1]
        else:
            degree_x = degrees[1:]

        max_error_r_list = []
        max_error_v_list = []
        max_error_h_list = []
        max_error_e_list = []
        # Loop over time steps
        for deg1, deg2 in zip(degrees[:-1], degrees[1:]):
            sim1 = name_sample.replace('[-]',f'Benchmark_SH-{print_int_float(deg1)}')
            sim2 = name_sample.replace('[-]',f'Benchmark_SH-{print_int_float(deg2)}')
            # SIM
            data1 = self.read_states_file(type, sim1)
            data2 = self.read_states_file(type, sim2)

            # Compute Residuals
            if compare_to == 'first_degree':
                RESIDUALS = self.compute_residuals_softwares(data_fd, data2, 
                                                                interpolation=interpolation, ToCalculate=to_compute)
            elif compare_to == 'previous_degree':
                RESIDUALS = self.compute_residuals_softwares(data1, data2, 
                                                                interpolation=interpolation, ToCalculate=to_compute)
            elif compare_to == 'last_degree':
                RESIDUALS = self.compute_residuals_softwares(data_ld, data1, 
                                                                interpolation=interpolation, ToCalculate=to_compute)

            # Organize Residual Data
            time_plot = RESIDUALS['time_plot']
            r_diff_norm = RESIDUALS['cartesian']['r_residuals_norm']
            max_error_r = max(r_diff_norm)
            max_error_r_list.append(max_error_r)
            v_diff_norm = RESIDUALS['cartesian']['v_residuals_norm']
            max_error_v = max(v_diff_norm)
            max_error_v_list.append(max_error_v)
            if compute_h_e:
                h_diff_norm = RESIDUALS['h_e']['h_residuals_norm']
                max_error_h = max(h_diff_norm)
                max_error_h_list.append(max_error_h)
                e_diff_norm = RESIDUALS['h_e']['e_residuals_norm']
                max_error_e = max(e_diff_norm)
                max_error_e_list.append(max_error_e)

            if FIGS[1]:
                # Create Figure: R_RESIDUALS
                fig2 = plt.figure(figsize=(10,5))
                ax2 = plt.axes()
                ax2.plot(time_plot, r_diff_norm)
                ax2.set_xlabel('Time [days]')
                ax2.set_ylabel('Position Residual [m]')
                fig2.suptitle(f'[{type}][{method}] - deg = {print_int_float(deg2)} s')
                fig2.tight_layout(pad=0.4)
                fig2.savefig(fig_folder+f'R_RESIDUALS/[{type}][{method}]-{print_int_float(deg2)}.png', dpi = 300)
            
            if FIGS[3]:
                # Create Figure: V_RESIDUALS
                fig4 = plt.figure(figsize=(10,5))
                ax4 = plt.axes()
                ax4.plot(time_plot, v_diff_norm)
                ax4.set_xlabel('Time [days]')
                ax4.set_ylabel('Velocity Residual [m/s]')
                fig4.suptitle(f'[{type}][{method}] - deg = {print_int_float(deg2)} s')
                fig4.tight_layout(pad=0.4)
                fig4.savefig(fig_folder+f'V_RESIDUALS/[{type}][{method}]-{print_int_float(deg2)}.png', dpi = 300)
            
            if FIGS[5]:
                # Create Figure: H_RESIDUALS
                fig6 = plt.figure(figsize=(10,5))
                ax6 = plt.axes()
                ax6.plot(time_plot, h_diff_norm)
                ax6.set_xlabel('Time [days]')
                ax6.set_ylabel('Angular Momentum Residual [m^2/s]')
                fig6.suptitle(f'[{type}][{method}] - deg = {print_int_float(deg2)} s')
                fig6.tight_layout(pad=0.4)
                fig6.savefig(fig_folder+f'H_RESIDUALS/[{type}][{method}]-{print_int_float(deg2)}.png', dpi = 300)
            
            if FIGS[7]:
                # Create Figure: E_RESIDUALS
                fig8 = plt.figure(figsize=(10,5))
                ax8 = plt.axes()
                ax8.plot(time_plot, e_diff_norm)
                ax8.set_xlabel('Time [days]')
                ax8.set_ylabel('Energy Residual [m^2/s^2]')
                fig8.suptitle(f'[{type}][{method}] - deg = {print_int_float(deg2)} s')
                fig8.tight_layout(pad=0.4)
                fig8.savefig(fig_folder+f'E_RESIDUALS/[{type}][{method}]-{print_int_float(deg2)}.png', dpi = 300)
        
        if FIGS[0]:
            ax1.scatter(degree_x, max_error_r_list, marker='*')
            ax1.plot(degree_x, max_error_r_list)
            ax1.grid()
            ax1.set_xlabel('Max Degree/Order [-]')
            ax1.set_ylabel('Max Position Residual [m]')
            fig1.suptitle(f'[{type}][{method}]')
            fig1.tight_layout(pad=0.4)
            fig1.savefig(fig_folder+f'BENCHMARK_SH_R[{type}][{method}].png', dpi = 300)
            if LOG:
                ax1.set_yscale('log')
                fig1.suptitle(f'[{type}][{method}]')
                fig1.tight_layout(pad=0.4)
                fig1.savefig(fig_folder+f'BENCHMARK_SH_R[{type}][{method}]-LOG.png', dpi = 300)
        
        if FIGS[2]:
            ax3.scatter(degree_x, max_error_v_list, marker='*')
            ax3.plot(degree_x, max_error_v_list)
            ax3.grid()
            ax3.set_xlabel('Max Degree/Order [-]')
            ax3.set_ylabel('Max Velocity Residual [m/s]')
            fig3.suptitle(f'[{type}][{method}]')
            fig3.tight_layout(pad=0.4)
            fig3.savefig(fig_folder+f'BENCHMARK_SH_V[{type}][{method}].png', dpi = 300)
            if LOG:
                ax3.set_yscale('log')
                fig3.suptitle(f'[{type}][{method}]')
                fig3.tight_layout(pad=0.4)
                fig3.savefig(fig_folder+f'BENCHMARK_SH_V[{type}][{method}]-LOG.png', dpi = 300)
        
        if FIGS[4]:
            ax5.scatter(degree_x, max_error_h_list, marker='*')
            ax5.plot(degree_x, max_error_h_list)
            ax5.grid()
            ax5.set_xlabel('Max Degree/Order [-]')
            ax5.set_ylabel('Max Angular Momentum Residual [m^2/s]')
            fig5.suptitle(f'[{type}][{method}]')
            fig5.tight_layout(pad=0.4)
            fig5.savefig(fig_folder+f'BENCHMARK_SH_H[{type}][{method}].png', dpi = 300)
            if LOG:
                ax5.set_yscale('log')
                fig5.suptitle(f'[{type}][{method}]')
                fig5.tight_layout(pad=0.4)
                fig5.savefig(fig_folder+f'BENCHMARK_SH_H[{type}][{method}]-LOG.png', dpi = 300)
        
        if FIGS[6]:
            ax7.scatter(degree_x, max_error_e_list, marker='*')
            ax7.plot(degree_x, max_error_e_list)
            ax7.grid()
            ax7.set_xlabel('Max Degree/Order [-]')
            ax7.set_ylabel('Max Energy Residual [m^2/s^2]')
            fig7.suptitle(f'[{type}][{method}]')
            fig7.tight_layout(pad=0.4)
            fig7.savefig(fig_folder+f'BENCHMARK_SH_E[{type}][{method}].png', dpi = 300)
            if LOG:
                ax7.set_yscale('log')
                fig7.suptitle(f'[{type}][{method}]')
                fig7.tight_layout(pad=0.4)
                fig7.savefig(fig_folder+f'BENCHMARK_SH_E[{type}][{method}]-LOG.png', dpi = 300)


    # COMPARISON BETWEEN INTEGRATORS PERFORMANCE BASED ON FORWARD AND BACKWARD PROPAGATION RESIDUAL AT t0
    def compare_integrators_performance_forward_backward(self, var_step_integ, fix_step_integ, fig_folder_name,
                                                          reference_file_dir='./OUTPUTS/PERFORMANCE', reference_file_name='[integ]-[type]'):
        
        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/FRONTBACK_PERFORMANCE'
        self.create_folder(fig_folder)

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'

        # Create Figure: Residual
        fig1 = plt.figure(figsize=(10,5))
        ax1 = plt.axes()
        
        # Create Figure: RTC
        fig2 = plt.figure(figsize=(10,5))
        ax2 = plt.axes()

        # List of Colors
        colors_list = ['tab:blue', 'tab:red', 'tab:green', 'tab:orange', 'tab:purple', 
                       'tab:brown', 'tab:olive', 'tab:pink', 'tab:cyan', 'tab:gray']
        
        # Variable Step Marker
        var_step_marker = 'o'

        # Fixed Step Marker
        fix_step_marker = '*'

        # Variable Step SIze Integrators Info
        i = 0
        for var_integ in var_step_integ:
            # Read Info
            file = reference_file_dir + '/' + reference_file_name.replace('[integ]', var_integ).replace('[type]', 'varstep')
            names, performance_info, tolerances = self.read_performance_front_back_info(file)

            # Organize info
            label = f'{var_integ.lower()}'
            RTC_list = performance_info[:,0]
            dr_norm_list = performance_info[:,1]
            ncalls_list = performance_info[:,3]

            # Plots Figure: Residual
            ax1.plot(ncalls_list, dr_norm_list, color=colors_list[i], marker=var_step_marker, label=label)

            # Plots Figure: RTC
            ax2.plot(ncalls_list, RTC_list, color=colors_list[i], marker=var_step_marker, label=label)

            i+=1
        
        # Fixed Step SIze Integrators Info
        for fix_integ in fix_step_integ:
            # Read Info
            file = reference_file_dir + '/' + reference_file_name.replace('[integ]', fix_integ).replace('[type]', 'fixstep')
            names, performance_info, tolerances = self.read_performance_front_back_info(file)

            # Organize info
            label = f'{fix_integ.lower()}[fix]'
            RTC_list = performance_info[:,0]
            dr_norm_list = performance_info[:,1]
            ncalls_list = performance_info[:,3]

            # Plots Figure: Residual
            ax1.plot(ncalls_list, dr_norm_list, color=colors_list[i], marker=fix_step_marker, label=label)

            # Plots Figure: RTC
            ax2.plot(ncalls_list, RTC_list, color=colors_list[i], marker=fix_step_marker, label=label)

            i+=1
        
        # Figure Style: Residual
        ax1.legend(loc=0)
        ax1.grid()
        ax1.set_xlabel('Number of function evaluations [-]')
        ax1.set_ylabel('dr($t_0$) [m]')
        ax1.set_xscale('log')
        ax1.set_yscale('log')
        fig1.suptitle(f'Performance - Position Residual at $t_0$')
        fig1.tight_layout(pad=0.4)
        fig1.savefig(fig_folder+f'FRONTBACK_PERFORMANCE/integrators_performance_frontback_residual.png', dpi = 300)

        # Figure Style: Residual
        ax2.legend(loc=0)
        ax2.grid()
        ax2.set_xlabel('Number of function evaluations [-]')
        ax2.set_ylabel('RTC [-]')
        ax2.set_xscale('log')
        ax2.set_yscale('log')
        fig2.suptitle(f'Performance - RTC')
        fig2.tight_layout(pad=0.4)
        fig2.savefig(fig_folder+f'FRONTBACK_PERFORMANCE/integrators_performance_frontback_rtc.png', dpi = 300)


    # BUILD REFERENCE WITH NUMBER OF RATE FUNCTION CALLS
    def build_ncalls_reference(self, sim, name='T.txt', output_dir='./Outputs/Reference/'):
        # Figures Folder
        fig_folder_out = f'{output_dir}'
        self.create_folder(fig_folder_out)
        ncalls = self.get_number_rate_function_calls(sim)
        # Write Info File
        with open(f'{output_dir}{name}_ncalls.txt', 'w') as fo:
            fo.write(f'{name} ncalls: {ncalls}')


    # CHECK IF NCALLS MATCH WITH REFERENCE NCALLS
    def check_reference_ncalls(self, sim):
        SIM_D = self.get_simulation('DOCKS')

        default_output_DOCKS = self.output_DOCKS
        # Read Simulation Ncalls
        self.update_output_path('DOCKS', self.output_DOCKS + SIM_D[sim]['output_folder'])
        ncalls = self.get_number_rate_function_calls(f'{SIM_D[sim]["name"]}_logs', encoding=False)
        self.update_output_path('DOCKS', default_output_DOCKS)

        # Read Reference Ncalls
        ncalls_reference = self.read_ncalls_reference(SIM_D[sim]['reference_folder']+'Ncalls_Reference/'+SIM_D[sim]['name'])

        return ncalls==ncalls_reference, ncalls, ncalls_reference


    # BUILD REFERENCE ELLIPSOID FOR TEST CASE
    def build_reference_ellipsoid(self, types, sims, fig_folder_name, axes_factor, name='T.txt',
                        numerical_noise_sphere_R=None, reference=None, interp_type='cubic', interpolation='no interpolation', output_dir='./Outputs/Reference/'):

        # Create Variables
        DATA = {}
        RESIDUALS_DICT = {}

        # Read Data
        for sim,type in zip(sims, types):
            DATA[type] = self.read_states_file(type, sim)
        
        # Figures Folder
        fig_folder_out = f'{output_dir}'
        self.create_folder(fig_folder_out)

        # Write Info File
        with open(f'{output_dir}{name}_info.txt', 'w') as fo:
            fo.write(f'{name:^90}\n\n')
            fo.write(f'{"MAXIMUM RESIDUALS":^90}\n')
            fo.write(f'{"Name":^15}{"|R| [m]":^25}{"t [days]":^25}{"INDEX":^25}\n')

        # Compute Residuals
        types_aux = copy.deepcopy(types)
        for t1 in types:
            types_aux.remove(t1)
            for t2 in types_aux:
                # Compute Residuals
                RESIDUALS = self.compute_residuals_softwares(DATA[t1], DATA[t2], 
                                                                interpolation=interpolation, ToCalculate=[1,0,0])

                time_plot = RESIDUALS['time_plot']
                dr = RESIDUALS['cartesian']['r_residuals_norm']
                maximum_r_norm_index = np.argmax(dr)

                if 'DOCKS' in [t1, t2]:
                    if 'TUDAT' in [t1, t2]:
                        dict_key = 'DT'
                    elif 'GMAT' in [t1, t2]:
                        dict_key = 'DG'
                    elif 'FREEFLYER' in [t1, t2]:
                        dict_key = 'DF'
                elif 'TUDAT' in [t1, t2]:
                    if 'GMAT' in [t1, t2]:
                        dict_key = 'TG'
                    elif 'FREEFLYER' in [t1, t2]:
                        dict_key = 'TF'
                elif 'GMAT' in [t1, t2]:
                    if 'FREEFLYER' in [t1, t2]:
                        dict_key = 'GF'

                RESIDUALS_DICT[dict_key] = [maximum_r_norm_index, RESIDUALS]

                # Write Info File
                with open(f'{output_dir}{name}_info.txt', 'a') as fo:
                    lab = f'R_{dict_key}(t_{dict_key})'
                    fo.write(f'{lab:^15}{dr[maximum_r_norm_index]:^25.15e}{time_plot[maximum_r_norm_index]:^25.15e}{maximum_r_norm_index:^25}\n')
        
        # Read Half Time Step if available, And Compute Residuals
        if reference:
            if not isinstance(reference, list):
                reference = [reference]
            for jjj,ref in enumerate(reference):
                lab_ind = f'Dr{jjj+1}'
                DATA[f'ref_{jjj+1}'] = self.read_states_file('DOCKS', ref)
                RESIDUALS = self.compute_residuals_softwares(DATA['DOCKS'], DATA[f'ref_{jjj+1}'], 
                                                                    interpolation=1, ToCalculate=[1,0,0], interp_type=interp_type)
                time_plot = RESIDUALS['time_plot']
                dr = RESIDUALS['cartesian']['r_residuals_norm']
                maximum_r_norm_index = np.argmax(dr)
                # Write Info File
                with open(f'{output_dir}{name}_info.txt', 'a') as fo:
                    lab = f'R_{lab_ind}(t_{lab_ind})'
                    fo.write(f'{lab:^15}{dr[maximum_r_norm_index]:^25.15e}{time_plot[maximum_r_norm_index]:^25.15e}{maximum_r_norm_index:^25}\n')
                RESIDUALS_DICT[lab_ind] = [maximum_r_norm_index, RESIDUALS]

        # Write Info File
        with open(f'{output_dir}{name}_info.txt', 'a') as fo:
            fo.write(f'\n{"EXTRA RESIDUALS":^90}\n')
            fo.write(f'{"Name":^15}{"|R| [m]":^25}{"t [days]":^25}{"INDEX":^25}\n')
        
        # Initialize Variables
        residuals_coordinates = []
        labels = []

        # Get Residuals Coordinates
        for k in list(RESIDUALS_DICT.keys()):
            if 'D' in k:
                residuals_coordinates.append(RESIDUALS_DICT[k][1]['cartesian']['states_residuals'][RESIDUALS_DICT[k][0],:3])
                labels.append(f'R_{k}(t_{k})')

            else:
                if interpolation == 'no interpolation':
                    if k == 'TG':
                        k_aux1 = 'DT'
                        k_aux2 = 'DG'
                    elif k == 'TF':
                        k_aux1 = 'DT'
                        k_aux2 = 'DF'
                    elif k == 'GF':
                        k_aux1 = 'DG'
                        k_aux2 = 'DF'

                    residuals_coordinates.append(RESIDUALS_DICT[k_aux1][1]['cartesian']['states_residuals'][RESIDUALS_DICT[k][0],:3])
                    labels.append(f'R_{k_aux1}(t_{k})')
                    residuals_coordinates.append(RESIDUALS_DICT[k_aux2][1]['cartesian']['states_residuals'][RESIDUALS_DICT[k][0],:3])
                    labels.append(f'R_{k_aux2}(t_{k})')

                    # Write Info File
                    with open(f'{output_dir}{name}_info.txt', 'a') as fo:
                        lab = f'R_{k_aux1}(t_{k})'
                        fo.write(f'{lab:^15}{RESIDUALS_DICT[k_aux1][1]["cartesian"]["r_residuals_norm"][RESIDUALS_DICT[k][0]]:^25.15e}{RESIDUALS_DICT[k_aux1][1]["time_plot"][RESIDUALS_DICT[k][0]]:^25.15e}{RESIDUALS_DICT[k][0]:^25}\n')
                        lab = f'R_{k_aux2}(t_{k})'
                        fo.write(f'{lab:^15}{RESIDUALS_DICT[k_aux2][1]["cartesian"]["r_residuals_norm"][RESIDUALS_DICT[k][0]]:^25.15e}{RESIDUALS_DICT[k_aux2][1]["time_plot"][RESIDUALS_DICT[k][0]]:^25.15e}{RESIDUALS_DICT[k][0]:^25}\n')
        residuals_coordinates = np.array(residuals_coordinates)
        plot_index = len(residuals_coordinates)
        residuals_coordinates = np.vstack((residuals_coordinates, np.array([0.,0.,0.])))

        # Write Reference Validation File
        with open(f'{output_dir}{name}_coordinates.txt', 'w') as fo:
            fo.write(f'{name:^90}\n')
            fo.write(f'{"Name":^15}{"X [m]":^25}{"Y [m]":^25}{"Z [m]":^25}\n')
            for lab,rc in zip(labels,residuals_coordinates):
                fo.write(f'{lab:^15}{rc[0]:^25.15e}{rc[1]:^25.15e}{rc[2]:^25.15e}\n')
        
        # Add Numerical Noise Sphere (NNS) To Residuals, If Available 
        if numerical_noise_sphere_R:
            # Spherical Coordinates
            nns_colat = np.linspace(0, np.pi, 25)
            nns_long = np.linspace(0, 2*np.pi, 50)
            nns_COLAT, nns_LONG = np.meshgrid(nns_colat, nns_long)
            # Cartesian Coordiantes Meshgrid
            nns_x_mesh = np.cos(nns_LONG) * np.sin(nns_COLAT) * numerical_noise_sphere_R
            nns_y_mesh = np.sin(nns_LONG) * np.sin(nns_COLAT) * numerical_noise_sphere_R
            nns_z_mesh = np.cos(nns_COLAT) * numerical_noise_sphere_R
            # Cartesian Coordiantes Flat
            nns_coord = np.array([nns_x_mesh.flatten(), nns_y_mesh.flatten(), nns_z_mesh.flatten()]).T
            # Add Sphere Coordinates To Residuals Array
            residuals_coordinates = np.vstack((residuals_coordinates, nns_coord))

        # Get Validation Ellipsoid Paramters
        center_ell, axes_ell, R_to_ell = self.outer_ellipsoid_fit(residuals_coordinates)

        # Apply Ellipsoid Margin Factor
        axes_ell[0] *= axes_factor[0]
        axes_ell[1] *= axes_factor[1]
        axes_ell[2] *= axes_factor[2]
        
        # Write Ellipsoid Validation File
        with open(f'{output_dir}{name}_ellipsoid.txt', 'w') as fo:
            fo.write(f'{name}\n')
            fo.write(f'{"Center X [m]":^25}{"Center Y [m]":^25}{"Center Z [m]":^25}{"a":^25}{"b":^25}{"c":^25}\
{"R_11":^25}{"R_12":^25}{"R_13":^25}{"R_21":^25}{"R_22":^25}{"R_23":^25}{"R_31":^25}{"R_32":^25}{"R_33":^25}\n')
            fo.write(f'{center_ell[0]:^25.15e}{center_ell[1]:^25.15e}{center_ell[2]:^25.15e}{axes_ell[0]:^25.15e}\
{axes_ell[1]:^25.15e}{axes_ell[2]:^25.15e}{R_to_ell[0,0]:^25.15e}{R_to_ell[0,1]:^25.15e}{R_to_ell[0,2]:^25.15e}\
{R_to_ell[1,0]:^25.15e}{R_to_ell[1,1]:^25.15e}{R_to_ell[1,2]:^25.15e}{R_to_ell[2,0]:^25.15e}{R_to_ell[2,1]:^25.15e}\
{R_to_ell[2,2]:^25.15e}\n')

        # Get Ellipsoid X, Y, Z (MeshGrid)
        X_ell, Y_ell, Z_ell = self.get_xyz_ellipsoid(center_ell, axes_ell, R_to_ell)

        # Figures Folder
        fig_folder = f'./Plots/{fig_folder_name}/'
        self.create_folder(fig_folder)
        
        # Create Figure: Residuals Coordinates
        fig1 = plt.figure(figsize=(10,5))
        ax1 = plt.axes(projection='3d')
        if numerical_noise_sphere_R:
            ax1.plot_surface(nns_x_mesh, nns_y_mesh, nns_z_mesh, color='orange', alpha=0.3, label='Numerical Noise Sphere')
        ax1.scatter(residuals_coordinates[:plot_index,0], residuals_coordinates[:plot_index,1], residuals_coordinates[:plot_index,2], c='b', marker='o', label='Residuals')
        ax1.legend()
        ax1.grid(False)
        ax1.set_xlabel('X [m]')
        ax1.set_ylabel('Y [m]')
        ax1.set_zlabel('Z [m]')

        # Make a 3D quiver plot
        max_x = max(abs(residuals_coordinates[:,0])) * 1.2
        max_y = max(abs(residuals_coordinates[:,1])) * 1.2
        max_z = max(abs(residuals_coordinates[:,2])) * 1.2
        max_coord = max([max_x, max_y, max_z])
        # x, y, z = np.array([[-max_x,0,0],[0,-max_y,0],[0,0,-max_z]])
        # u, v, w = np.array([[max_x,0,0],[0,max_y,0],[0,0,max_z]])*2
        # ax1.quiver(x,y,z,u,v,w,arrow_length_ratio=0.1, color="black")
        ax1.plot([-max_x, max_x], [0,0], [0,0], color='k')
        ax1.scatter(max_x, 0, 0, color='k', marker='>', s = 60)
        ax1.plot([0, 0], [-max_y,max_y], [0,0], color='k')
        ax1.scatter(0, max_y, 0, color='k', marker='>', s = 60)
        ax1.plot([0, 0], [0,0], [-max_z,max_z], color='k')
        ax1.scatter(0, 0, max_z, color='k', marker='^', s = 60)
        ax1.set_xlim(-max_x,max_x)
        ax1.set_ylim(-max_y,max_y)
        ax1.set_zlim(-max_z,max_z)
        fig1.suptitle(f'{name} - Reference Points')
        fig1.tight_layout(pad=0.4)
        fig1.savefig(fig_folder+f'{name}-ReferencePoints.png', dpi = 300)
        
        # Create Figure: Residuals Coordinates with Validation Area
        fig2 = plt.figure(figsize=(10,5))
        ax2 = plt.axes(projection='3d')
        ax2.scatter(residuals_coordinates[:plot_index,0], residuals_coordinates[:plot_index,1], residuals_coordinates[:plot_index,2], c='b', marker='o')
        ax2.grid(False)
        ax2.set_xlabel('X [m]')
        ax2.set_ylabel('Y [m]')
        ax2.set_zlabel('Z [m]')
        ax2.plot_wireframe(X_ell, Y_ell, Z_ell, color='g', alpha=0.15)
        ax2.plot_surface(X_ell, Y_ell, Z_ell, color='g', alpha=0.08)

        # Make a 3D quiver plot
        max_x = np.max(np.abs(X_ell)) * 1.2
        max_y = np.max(np.abs(Y_ell)) * 1.2
        max_z = np.max(np.abs(Z_ell)) * 1.2
        # x, y, z = np.array([[-max_x,0,0],[0,-max_y,0],[0,0,-max_z]])
        # u, v, w = np.array([[max_x,0,0],[0,max_y,0],[0,0,max_z]])*2
        ax2.plot([-max_x, max_x], [0,0], [0,0], color='k')
        ax2.scatter(max_x, 0, 0, color='k', marker='>', s = 60)
        ax2.plot([0, 0], [-max_y,max_y], [0,0], color='k')
        ax2.scatter(0, max_y, 0, color='k', marker='>', s = 60)
        ax2.plot([0, 0], [0,0], [-max_z,max_z], color='k')
        ax2.scatter(0, 0, max_z, color='k', marker='^', s = 60)
        #ax2.quiver(x,y,z,u,v,w,arrow_length_ratio=0.1, color="black")
        ax2.set_xlim(-max_x,max_x)
        ax2.set_ylim(-max_y,max_y)
        ax2.set_zlim(-max_z,max_z)
        ax2.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax2.ticklabel_format(style='sci', axis='z', scilimits=(0,0))
        fig2.suptitle(f'{name} - Validation Volume')
        fig2.tight_layout(pad=0.4)
        fig2.savefig(fig_folder+f'{name}-ValidationArea.png', dpi = 300)


    # CHECK IF INSIDE VALIDATION ELLIPSOID VOLUME
    def check_reference_ellipsoid(self, sim,
                        interpolation='no interpolation'):
        SIM_D = self.get_simulation('DOCKS')

        default_output_DOCKS = self.output_DOCKS
        # Read DOCKS Simulation States
        self.update_output_path('DOCKS', self.output_DOCKS + SIM_D[sim]['output_folder'])
        data = self.read_states_file('DOCKS', sim)

        # Read DOCKS Simulation States Reference
        self.update_output_path('DOCKS', SIM_D[sim]['reference_folder']+'/DOCKS_Reference/')
        data_ref = self.read_states_file('DOCKS', sim)
        self.update_output_path('DOCKS', default_output_DOCKS)

        # Compute Residuals
        RESIDUALS = self.compute_residuals_softwares(data_ref, data, 
                                                             interpolation=interpolation, ToCalculate=[1,0,0])
        
        # Get Maximum Residual Coordinate
        maximum_r_norm_index = np.argmax(RESIDUALS['cartesian']['r_residuals_norm'])
        maximum_residual_coordinate = RESIDUALS['cartesian']['states_residuals'][maximum_r_norm_index, :3]

        # Get Reference Ellipsoid and Residual Coordinates
        residuals_coordinates, ellipsoid_parameters = self.read_residuals_ellipsoid(SIM_D[sim]['name'], SIM_D[sim]['reference_folder']+'Residuals_Ellipsoid')
        center_ell = ellipsoid_parameters[:3]
        axes_ell = ellipsoid_parameters[3:6]
        R_to_ell = ellipsoid_parameters[6:].reshape(3,3)

        # Check If Inside Ellipsoid
        inside_check = self.check_inside_ellipsoid(center_ell, axes_ell, R_to_ell, maximum_residual_coordinate)
        if inside_check:
            inside_check_pass_fail = 'PASS'
        else:
            inside_check_pass_fail = 'FAIL'

        # Get Ellipsoid X, Y, Z (MeshGrid)
        X_ell, Y_ell, Z_ell = self.get_xyz_ellipsoid(center_ell, axes_ell, R_to_ell)

        # Plot Ellipsoid And Residuals Points
        fig_folder = './Outputs/' + SIM_D[sim]['output_folder']
        self.create_folder(fig_folder)
        fig = plt.figure(figsize=(8,6))
        ax = plt.axes(projection='3d')
        l1 = ax.scatter(residuals_coordinates[:,0], residuals_coordinates[:,1], residuals_coordinates[:,2], c='b', marker='o', s = 5)
        l2 = ax.scatter(maximum_residual_coordinate[0], maximum_residual_coordinate[1], maximum_residual_coordinate[2], c='r', marker='*', s = 40)
        ax.grid(False)
        ax.set_xlabel('X [m]')
        ax.set_ylabel('Y [m]')
        ax.set_zlabel('Z [m]')
        l3 = ax.plot_wireframe(X_ell, Y_ell, Z_ell, color='g', alpha=0.15)
        l4 = ax.plot_surface(X_ell, Y_ell, Z_ell, color='g', alpha=0.08)
        max_x = np.max(np.abs(X_ell)) * 1.2
        max_x = max(max_x, abs(maximum_residual_coordinate[0]))
        max_y = np.max(np.abs(Y_ell)) * 1.2
        max_y = max(max_y, abs(maximum_residual_coordinate[1]))
        max_z = np.max(np.abs(Z_ell)) * 1.2
        max_z = max(max_z, abs(maximum_residual_coordinate[2]))
        l5 = ax.plot([-max_x, max_x], [0,0], [0,0], color='k')
        l6 = ax.scatter(max_x, 0, 0, color='k', marker='>', s = 60)
        l7 = ax.plot([0, 0], [-max_y,max_y], [0,0], color='k')
        l8 = ax.scatter(0, max_y, 0, color='k', marker='>', s = 60)
        l9 = ax.plot([0, 0], [0,0], [-max_z,max_z], color='k')
        l10 = ax.scatter(0, 0, max_z, color='k', marker='^', s = 60)
        ax.set_xlim(-max_x,max_x)
        ax.set_ylim(-max_y,max_y)
        ax.set_zlim(-max_z,max_z)
        fig.suptitle(f'{SIM_D[sim]["name"]} - {inside_check_pass_fail}\nResidual = {maximum_residual_coordinate} m')
        fig.tight_layout(pad=0.4)
        fig.savefig(fig_folder+f'{SIM_D[sim]["name"]}.png', dpi = 300)

        # Save Fig in Pickle
        pickle.dump(fig, open(fig_folder+f'{SIM_D[sim]["name"].split("-")[0]}_fig.pkl', 'wb'))

        # Create GIF Of Plot
        # Set the viewing angle
        ax.view_init(elev=10, azim=0)
        # Compute Number of Frames and Interval
        fps = 12
        duration = 7  # Duration in seconds
        num_frames = fps * duration  # Total number of frames
        interval = duration * 1000 / num_frames  # Interval in milliseconds
        # Define the update function for animation
        def update(frame):
            angle = frame * 2 * np.pi / num_frames  # Rotate 360 degrees over 100 frames
            ax.view_init(elev=10, azim=np.degrees(angle))  # Change the view angle
            return l1, l2, l3, l4, l5, l6, l7, l8, l9, l10
        # Create animation
        ani = FuncAnimation(fig, update, frames=num_frames, interval=interval)
        # Save the animation as a GIF
        ani.save(fig_folder+f'{SIM_D[sim]["name"]}.gif', fps=fps, dpi=300)

        return inside_check, maximum_residual_coordinate
         

    # BUILD REFERENCE RTC FOR TEST CASE USING FORWARD AND BACKWARD PROPAGATION
    def build_reference_rtc(self, sim, type, factor,
                         name='T.txt', output_dir='./Outputs/Reference/'):
        # Read Forward Propagation
        sim_fw = f'{sim}-front'
        _, _, _, states_fw, _ = self.read_states_file(type, sim_fw)

        # Read Backward Propagation
        sim_bw = f'{sim}-back'
        _, _, _, states_bw, _ = self.read_states_file(type, sim_bw)

        # Compute RTC
        RTC = self.compute_RTC(states_fw, states_bw)

        # Apply Factor to RTC
        RTC_ref = RTC * factor

        # Create Reference File Output Folder
        fig_folder_out = output_dir
        self.create_folder(fig_folder_out)

        # Write Reference RTC in Reference File
        with open(f'{output_dir}{name}_rtc.txt', 'w') as fo:
            fo.write(f'{name} RTC: {RTC_ref:.15e}')
    

   # CHECK IF RTC IS SMALLER OR EQUAL TO REFERENCE RTC
    def check_reference_rtc(self, sim):
        SIM_D = self.get_simulation('DOCKS')

        default_output_DOCKS = self.output_DOCKS
        # Read Forward Propagation
        self.update_output_path('DOCKS', self.output_DOCKS + SIM_D[sim]['output_folder'])
        sim_fw = f'{sim}-front'
        _, _, _, states_fw, _ = self.read_states_file('DOCKS', sim_fw)
        # Read Backward Propagation
        sim_bw = f'{sim}-back'
        _, _, _, states_bw, _ = self.read_states_file('DOCKS', sim_bw)
        self.update_output_path('DOCKS', default_output_DOCKS)

        # Compute RTC
        RTC = self.compute_RTC(states_fw, states_bw)

        # Read Reference RTC
        RTC_ref = self.read_rtc_reference(SIM_D[sim]['reference_folder']+'RTC_Reference/'+SIM_D[sim]['name'])

        return RTC<=RTC_ref, RTC, RTC_ref

 