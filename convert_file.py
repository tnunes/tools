from . import ccsds
from . import time_function as tt

from astropy.time import Time
import numpy as np

### FILE CONVERSIONS ###

# CONVERT JPL EPEHEMRIDES TO CIC
def jpl_eph2cic(input_file, object_name, center_name, time_format='MJD2COL', case='Non-Keplerian'):
    # open the .txt ephemerides file downloaded from JPL web interface
    fM = open(input_file,'r')
    file = fM.readlines()
    file = np.array(file)
    for i in range(file.size):
        file[i] = file[i].strip()

    file_filter = []
    for i in range(file.size):
        if file[i] != '':
            file_filter.append(file[i])
    file_filter = np.array(file_filter)

    for i in range(file_filter.size):
        if (file_filter[i] == '$$SOE'):
            start = i
        if (file_filter[i] == '$$EOE'):
            stop = i
            break
    values = file_filter[start + 1:stop]
    header = file_filter[0:start]
    datafloat = []

    for i in values:
        datafloat.append(i.split(","))
    datafloat = np.array(datafloat)
    datafloat = datafloat[:,0:-1]
    datafloat = np.delete(datafloat,np.s_[1],1)
    datafloat = datafloat.astype(float)

    data_science = datafloat[:,1:]
    data_time = Time(datafloat[:,0],format='jd',scale='tdb')

    if time_format == 'MJD2COL':
        data_time = tt.julian_to_vector_julian(data_time.mjd)
        data_time = np.rint(data_time)
        # mention precision of data i.e no. of digits after decimal
        sequence = ["%d", "%.6f", "%.15e", "%.15e", "%.15e","%.15e", "%.15e", "%.15e"]
    elif time_format == 'ISOT':
        data_time = [[d.isot] for d in data_time]
        # mention precision of data i.e no. of digits after decimal
        sequence = ["%s", "%.15e", "%.15e", "%.15e","%.15e", "%.15e", "%.15e"]


    if case == 'Keplerian':
        data_science = np.array([data_science[0, 0], data_science[0, 1], data_science[0, 2], 0.0, 0.0, 0.0])
        data_science = np.array([data_science, ] * len(data_time))

    # Writing CIC File
    cic = "CIC_OEM_VERS = 2.0"
    origin = "ORIGINATOR = DOCKS"
    comment = ["COMMENT        Ephemeris, JPL",
            "COMMENT        Colomn 1:2 Unit : Time (MJD)",
            "COMMENT        Colomn 3:5 Unit : Position (KM)",
            "COMMENT        Colomn 6:8 Unit : Velocity (KM/S)",
            "COMMENT        Colomn 9:11 Unit : Acceleration (KM/S**2)"]
    # comment = ["COMMENT = Ephemeris of " + object_name + " from jpl in ICRS frame centered at " + center_name + "."]
    header = ["OBJECT_NAME = " + object_name,
            "OBJECT_ID = " + center_name + "_" + object_name +"\n",
            "CENTER_NAME = " + center_name,
            "REF_FRAME = ICRS",
            "TIME_SYSTEM = TDB"]

    if case == 'Keplerian':
        ccsds.write_ccsds(object_name + '_Keplerian_CIC.txt', cic,
                        origin, comment, header, sequence, data_time, data_science)
    else :
        ccsds.write_ccsds(input_file.split('.')[0]+'_CIC'+'.txt',
                        cic, origin, comment, header, sequence, data_time, data_science)


# CONVERT GMAT Ephemerides ISOT GREGORIAN UTC to MJD2COL TDB
def gmat_eph_utc_isot_2_tdb_mjd2col(gmat_eph_filepath, output_filepath):
    cic, origin, comment, header, data_time, data_science = ccsds.read_ccsds(gmat_eph_filepath, time_format='ISOT-UTC', separator='\t')
    cic = 'CIC_OEM_VERS = 2.0'
    header = [h.replace('UTC', 'TDB') for h in header]
    astro_time_utc = Time(data_time, format="isot", scale='utc')
    astro_time_tdb = astro_time_utc.tdb.copy().value
    time = Time(astro_time_tdb, format="isot", scale='tdb')
    time_mjd_vec = tt.julian_to_vector_julian(time.mjd)
    sat_quat_path_out = output_filepath
    sequence = ["%d","%10.4f","%.15e","%.15e","%.15e","%.15e","%.15e","%.15e"]
    ccsds.write_ccsds(sat_quat_path_out, cic, origin, comment, header, sequence, time_mjd_vec, data_science*1000, separator='\t')


# CONVERT SPHERICAL HARMONIC FILE FROM DOCKS TO FREEFLYER
def sh_docks2sh_ff(docks_sh_filepath, output_filepath, model_name, body):
    with open(docks_sh_filepath) as f:
        lines = f.readlines()
    # convert data in list for each line
    data = [line.split(',') for line in lines]
    # get parameters
    R = data[0][0].replace(" ", "").replace("\t", "")
    mu = data[0][1].replace(" ", "").replace("\t", "")
    degree = data[0][3].replace(" ", "").replace("\t", "")
    order = data[0][4].replace(" ", "").replace("\t", "")
    normalized = data[0][5].replace(" ", "").replace("\t", "")
    # get coefficients
    coeffs = data[3:]
    if ['\n'] in coeffs:
        coeffs.remove(['\n'])
    for i in range(len(coeffs)):
        for j in range(len(coeffs[i])):
            coeffs[i][j] = coeffs[i][j].replace(" ", "").replace("\t", "")

    # Create Header
    header = []
    header.append(['Model Name', model_name])
    header.append(['Body', body])
    header.append(['Degree', degree])
    header.append(['Order', order])
    header.append(['Gravity Const.', mu])
    header.append(['Body Radius', R])
    header.append(['Rec. Flattening', '0.0'])
    if int(normalized):
        header.append(['Normalized', 'Yes'])
    else:
        header.append(['Normalized', 'No'])
    header.append(['Tide Model', '-'])
    # Write Header
    with open(output_filepath, 'w') as fo:
        for h in header:
            fo.write(f'{h[0]:<18}{h[1]}\n')
        fo.write('\n')
    # Write Coefficients header
        fo.write(f'{"Deg":>10}{"Ord":>5}{"C":^30}{"S":^30}\n')
    # Write Coefficents
        for c in coeffs:
            fo.write(f'GCOEF{c[0]:>5}{c[1]:>5}{c[2]:^30}{c[3]:^30}\n')


# CONVERT SPHERICAL HARMONIC FILE FROM DOCKS TO GMAT
def sh_docks2sh_gmat(docks_sh_filepath, output_filepath, model_name, body):
    with open(docks_sh_filepath) as f:
        lines = f.readlines()
    # convert data in list for each line
    data = [line.split(',') for line in lines]
    # get parameters
    R = float(data[0][0].replace(" ", "").replace("\t", ""))*1e3
    mu = float(data[0][1].replace(" ", "").replace("\t", ""))*1e9
    degree = data[0][3].replace(" ", "").replace("\t", "")
    order = data[0][4].replace(" ", "").replace("\t", "")
    normalized = data[0][5].replace(" ", "").replace("\t", "")
    # get coefficients
    coeffs = data[3:]
    if ['\n'] in coeffs:
        coeffs.remove(['\n'])
    for i in range(len(coeffs)):
        for j in range(len(coeffs[i])):
            coeffs[i][j] = float(coeffs[i][j].replace(" ", "").replace("\t", ""))

    # Write Header
    with open(output_filepath, 'w') as fo:
        fo.write(f'COMMENT   5\n')
        fo.write(f'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC\n')
        fo.write(f'CCCCC  ------------------------------------------------------------------  CCCCC\n')
        header = f'CCCCC  {body} - {model_name} : [{degree}x{order}]'
        fo.write(header)
        space = 80 - len(header)
        fo.write(f'{"CCCCC":>{space}}\n')
        fo.write(f'CCCCC  ------------------------------------------------------------------  CCCCC\n')
        fo.write(f'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC\n')
    # Write Coefficients header
        fo.write(f'POTFIELD{degree:>3}{order:>3}{"1":>3} {mu:.14e} {R:.14e} {1.0:.14e}\n')
    # Write Coefficents
        for c in coeffs:
            fo.write(f'RECOEF{int(c[0]):>5}{int(c[1]):>3}{c[2]:>24.14e}{c[3]:>21.14e}\n')


# CONVERT WebGeocalc ROTATION MATRIX FILE TO DOCKS
def rot_mat_webgeocalc2docks(input_filepath, output_filepath, body, time_format='calendar'):
    with open(input_filepath) as fi:
        lines = fi.readlines()
    # Find limit Index
    for i, line in enumerate(lines):
        if 'Calculation Inputs' in line:
            header_start = i + 2 
        elif 'Frame Transformation Results' in line:
            header_end = i - 2
            data_start = i + 3
        elif 'Kernels Used' in line:
            data_end = i - 2
    # Seperate Data
    header_inp_data = [line.split('=') for line in lines[header_start:header_end]]
    data_inp = [line.split() for line in lines[data_start:data_end]]
    # Initialize Lists
    data_science = []
    data_time = []
    comment = []
    sequence = []
    # DATE FROMAT = CALENDAR
    if time_format == 'calendar':
        for l in data_inp:
            data_science.append(l[3:])
            data_time.append(['T'.join(l[:2])])
        data_science = np.array(data_science, dtype=float)
        # Comments
        comment = [
                "COMMENT        Rotation Matrix",
                "COMMENT        Colomn 1    Unit : Calendar Date and Time (TDB)",
                "COMMENT        Colomn 2:10 from : M11, M12, M13, M21, M22, M23, M31, M32, M33",
                ]
        # Sequence
        sequence = ["%s", "%.15e", "%.15e", "%.15e","%.15e", "%.15e", "%.15e", "%.15e", "%.15e", "%.15e"]
    # Writing CIC File
    cic = "CIC_AEM_VERS = 1.0"
    origin = "ORIGINATOR = JPL WebGeocalc"
    # Header
    header = [
            "OBJECT_NAME = " + body,
            "",
            "REF_FRAME_A = " + header_inp_data[1][1].replace("\n",""),
            "REF_FRAME_B = " + header_inp_data[2][1].replace("\n",""),
            "ATTITUDE_DIR = A2B",
            "",
            "ATTITUDE_TYPE = ROTATION_MATRIX",
            "LIGHT PROPAGATION = " + header_inp_data[7][1].replace("\n",""),
            "TIME_SYSTEM = TDB",
            "TIME RANGE = " + header_inp_data[5][1].replace("\n",""),
            "TIME STEP = " + header_inp_data[6][1].replace("\n",""),
            ]
    # Write
    ccsds.write_ccsds(output_filepath, cic, origin, comment, header, sequence, data_time, data_science)