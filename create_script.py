import os
import numpy as np
import ruamel.yaml
from ruamel.yaml.comments import CommentedMap as OrderedDict
import subprocess
import copy



class DOCKS_script():

    def __init__(self):
        self.data = {}
        self.data_reseter = {}
        self.yaml = ruamel.yaml.YAML()

        self.initialize_empty_data()
        self.save_data_reset()
  
    
    def initialize_empty_data(self):
        self.data = OrderedDict()
        template = {
                'initialConditions': ['file_path', 'frame', 'format', 'center'],
                'ephemInput': ['spice_kernels', 'text_files'],
                'perturbations': ['central_body', 'predefined_bodies', 'new_bodies_added', 'complex_grav_model_activated', 'non_grav_perturbations', 'propulsion_burns_file'],
                'new_grav_bodies': ['body1'],
                'complex_grav_bodies': ['body1'],
                'probeFeatures': ['mass', 'srp_area', 'srp_coefficient'],
                'numericalMethod': ['method', 'tolerance', 'safety_factor'],
                'timeSettings': ['method', 'propagation_time', 'time_step_unit', 'time_step'],
                'output': ['directory', 'file_name', 'frame', 'format', 'step_divider', 'center'],
            }
        for t in template:
            self.data [t] = OrderedDict()
            for tt in template[t]:
                self.data [t][tt] = ''
        self.data['perturbations']['predefined_bodies'] = []
        self.data['perturbations']['non_grav_perturbations'] = []
    

    def save_data_reset(self):
        self.data_reseter = copy.deepcopy(self.data)


    def reset_data(self):
        self.data = copy.deepcopy(self.data_reseter)


    def deafult_data(self):
        self.data['initialConditions']['center'] = ['custom', ['Earth', 3.98600441500000e+14, 'none', 399]]
        self.data['initialConditions']['frame'] = 'ICRF'
        self.data['ephemInput']['spice_kernels'] = '../../../../Kernel/'
        self.data['ephemInput']['text_files'] = ['ISOT', 'KM', 'KM/S']
        self.data['perturbations']['predefined_bodies'] = []
        self.data['timeSettings']['method'] = 'Duration'
        self.data['timeSettings']['time_step_unit'] = 'seconds'
        self.data['output']['directory'] = '../../Outputs/DOCKS'
        self.data['output']['frame'] = 'ICRF'
        self.data['output']['format'] = ['MJD_2COL', 'M', 'M/S', 'M/S^2']
        self.data['output']['step_divider'] = 0
        self.data['output']['center'] = ['custom', ['Earth', 3.98600441500000e+14, 'none', 399]]
        self.data['probeFeatures']['mass'] = 10
        self.data['perturbations']['new_bodies_added'] = False
        self.data['perturbations']['complex_grav_model_activated'] = False
        self.data['perturbations']['central_body'] = 'Earth'


    def add_propagation_time(self, time_days, time='0:0:0.0'):
        self.data['timeSettings']['propagation_time'] = [time_days, time]


    def add_initial_conditions(self, file_path, format=['ISOT', 'KM', 'KM/S'], center=[]):
        self.data['initialConditions']['file_path'] = file_path
        self.data['initialConditions']['format'] = format
        if center:
            self.data['initialConditions']['center'] = center


    def change_central_body(self, body):
        self.data['perturbations']['central_body'] = body
    

    def change_probe_mass(self, mass):
        self.data['probeFeatures']['mass'] = mass


    def add_pred_bodies(self, bodies):
        for b in bodies:
            self.data['perturbations']['predefined_bodies'].append(b)
    

    def remove_pred_bodies(self, bodies):
        for b in bodies:
            if b in self.data['perturbations']['predefined_bodies']:
                self.data['perturbations']['predefined_bodies'].remove(b)


    def add_new_body(self, name, mu, type_eph, eph, radius=0, name_ref='body1'):
        self.data['perturbations']['new_bodies_added'] = True
        self.data['new_grav_bodies'][name_ref] = OrderedDict()
        self.data['new_grav_bodies'][name_ref]['name'] = name
        self.data['new_grav_bodies'][name_ref]['mu'] = mu
        self.data['new_grav_bodies'][name_ref]['radius'] = radius
        if type_eph == 'file':
            self.data['new_grav_bodies'][name_ref]['ephFile'] = eph
            self.data['new_grav_bodies'][name_ref]['naifId'] = ''
        elif type_eph == 'naifid':
            self.data['new_grav_bodies'][name_ref]['ephFile'] = ''
            self.data['new_grav_bodies'][name_ref]['naifId'] = eph
    

    def remove_new_body(self, bodies, name_ref_type=0):
        if name_ref_type:
            for b in bodies:
                if b in list(self.data['new_grav_bodies'].keys()):
                    del self.data['new_grav_bodies'][b]
        else:
            for name_ref in list(self.data['new_grav_bodies'].keys()):
                for b in bodies:
                    if b == self.data['new_grav_bodies'][name_ref]['name']:
                        del self.data['new_grav_bodies'][name_ref]
        if not list(self.data['new_grav_bodies'].keys()):
            self.data['perturbations']['new_bodies_added'] = False
            self.data['new_grav_bodies']['body1'] = ''
    

    def add_new_complex_grav(self, name, type_eph, eph, type_rot, rot, sh_file, sh_deg, name_ref='body1'):
        self.data['perturbations']['complex_grav_model_activated'] = True
        self.data['complex_grav_bodies'][name_ref] = OrderedDict()
        self.data['complex_grav_bodies'][name_ref]['name'] = name
        self.data['complex_grav_bodies'][name_ref]['quatFile'] = None
        if type_eph == 'file':
            self.data['complex_grav_bodies'][name_ref]['ephFile'] = eph
            self.data['complex_grav_bodies'][name_ref]['naifId'] = ''
        elif type_eph == 'naifid':
            self.data['complex_grav_bodies'][name_ref]['ephFile'] = ''
            self.data['complex_grav_bodies'][name_ref]['naifId'] = eph
        if type_rot == 'file':
            self.data['complex_grav_bodies'][name_ref]['rotMatrixFile'] = rot
            self.data['complex_grav_bodies'][name_ref]['rotMatrixSpice'] = ''
        elif type_rot == 'spice':
            self.data['complex_grav_bodies'][name_ref]['rotMatrixFile'] = ''
            self.data['complex_grav_bodies'][name_ref]['rotMatrixSpice'] = rot
        self.data['complex_grav_bodies'][name_ref]['sphCoeffFile'] = sh_file
        self.data['complex_grav_bodies'][name_ref]['sphHarmDegree'] = sh_deg
    

    def remove_new_complex_grav(self, bodies, name_ref_type=0):
        if name_ref_type:
            for b in bodies:
                if b in list(self.data['complex_grav_bodies'].keys()):
                    del self.data['complex_grav_bodies'][b]
        else:
            for name_ref in list(self.data['complex_grav_bodies'].keys()):
                for b in bodies:
                    if b == self.data['complex_grav_bodies'][name_ref]['name']:
                        del self.data['complex_grav_bodies'][name_ref]
        if not list(self.data['complex_grav_bodies'].keys()):
            self.data['perturbations']['complex_grav_model_activated'] = False
            self.data['complex_grav_bodies']['body1'] = ''


    def add_srp(self, S, Cr):
        if 'srp' not in self.data['perturbations']['non_grav_perturbations']:
            self.data['perturbations']['non_grav_perturbations'].append('srp')
        self.data['probeFeatures']['srp_area'] = S
        self.data['probeFeatures']['srp_coefficient'] = Cr
    

    def remove_srp(self):
        if 'srp' in self.data['perturbations']['non_grav_perturbations']:
            self.data['perturbations']['non_grav_perturbations'].remove('srp')
        self.data['probeFeatures']['srp_area'] = 0
        self.data['probeFeatures']['srp_coefficient'] = 0
    

    def add_integrator(self, method, tol, sf, time_step):
        self.data['timeSettings']['time_step'] = time_step
        if method == 'rk4':
            self.data['numericalMethod']['method'] = 'rk4'
            self.data['numericalMethod']['tolerance'] = 0.
            self.data['numericalMethod']['safety_factor'] = 0.
            self.data['timeSettings']['time_step'][1] = 0.
            self.data['timeSettings']['time_step'][2] = 0.
        elif method == 'ias15':
            self.data['numericalMethod']['method'] = 'ias15'
            self.data['numericalMethod']['tolerance'] = 10**int(tol)
            self.data['numericalMethod']['safety_factor'] = 0.
            self.data['timeSettings']['time_step'][2] = 0.
        elif method == 'rkf45':
            self.data['numericalMethod']['method'] = 'rkf45'
            self.data['numericalMethod']['tolerance'] = 10**int(tol)
            self.data['numericalMethod']['safety_factor'] = sf
        elif method == 'rkf56':
            self.data['numericalMethod']['method'] = 'rkf56'
            self.data['numericalMethod']['tolerance'] = 10**int(tol)
            self.data['numericalMethod']['safety_factor'] = sf
        elif method == 'rkf78':
            self.data['numericalMethod']['method'] = 'rkf78'
            self.data['numericalMethod']['tolerance'] = 10**int(tol)
            self.data['numericalMethod']['safety_factor'] = sf


    def save_script(self, file_path):
        with open(f'{file_path}.yaml', 'w') as file:
            self.yaml.dump(self.data, file)
    

    def variable_step_size_several_tol(self, method, time_step, tol_list, sf, file_path_list):
        for tol, fp in zip(tol_list, file_path_list):
            self.add_integrator(method, tol, sf, time_step)
            back_bar = '\\'
            self.data['output']['file_name'] = f'{fp.replace(back_bar,"/").split("/")[-1]}.txt'
            self.save_script(f'{fp}')
    

    def fix_step_several_time_step(self, method, time_step_list, file_path_list):
        for ts, fp in zip(time_step_list, file_path_list):
            time_step = [ts, ts, ts]
            self.add_integrator(method, 0, 0, time_step)
            back_bar = '\\'
            self.data['output']['file_name'] = f'{fp.replace(back_bar,"/").split("/")[-1]}.txt'
            self.save_script(f'{fp}')


    def run_scripts(self, scripts, propagator_path='../../../../../DOCKS/Propagator/propagator.py', log=None):
        for script in scripts:
            if log:
                # Create LOGS Output Folder
                if not os.path.isdir(log):
                    os.makedirs(log)
                powershell_command = "powershell -Command \"python " + propagator_path + f' --performance \'{script}.yaml\'' + " | Tee-Object -FilePath" + " '" + f'{log}/{script}.txt' + "'" + '"'
                print(f'Running: {script}')
                subprocess.run(powershell_command, shell=True)
            else:
                command = ['python', propagator_path, f'{script}.yaml']
                print(f'Running: {script}')
                subprocess.run(command)

    
    def save_run_script(self, file_path, propagator_path='../../../../../DOCKS/Propagator/propagator.py'):
        self.save_script(file_path)
        self.run_scripts([file_path], propagator_path)



class GMAT_script():

    def __init__(self):
        self.data = []
        self.data_reseter = []
        self.output_directory = os.path.abspath('../../Outputs/GMAT')

        self.INTEGRATOR = {'RungeKutta4': 'rk4',
                           'PrinceDormand45': 'pd45',
                           'RungeKutta56': 'rkf56',
                           'RungeKutta68': 'rkf68',
                           'RungeKutta89': 'rkf89',
                           }

        self.MONTHS = {
                    '01': 'Jan',
                    '02': 'Fev',
                    '03': 'Mar',
                    '04': 'Apr',
                    '05': 'May',
                    '06': 'Jun',
                    '07': 'Jul',
                    '08': 'Aug',
                    '09': 'Sep',
                    '10': 'Oct',
                    '11': 'Nov',
                    '12': 'Dec',
                            }   
    

    def load_base_script(self, file):
        with open(file) as f:
            lines = f.readlines()
        self.data = [line.split() for line in lines]
        self.change_output('')
        self.save_data_reset()
    

    def save_data_reset(self):
        self.data_reseter = copy.deepcopy(self.data)


    def reset_data(self):
        self.data = copy.deepcopy(self.data_reseter)

    
    def change_parameter(self, keys, values):
        for i in range(len(self.data)):
            if self.data[i]:
                if self.data[i][0] == 'GMAT':
                    for key,value in zip(keys, values):
                        if self.data[i][1] == key:
                            self.data[i][3] = value
                            if key == 'REPORT.Add' or key == 'DefaultSC.Epoch':
                                del self.data[i][4:]


    def add_parameter(self, keys, values, previous_key):
        for i in range(len(self.data)):
            if self.data[i]:
                if self.data[i][0] == 'GMAT' and  self.data[i][1] == previous_key:
                    index = i+1
        for key,value in zip(keys, values):
            line_list = ['GMAT', key, '=', value]
            self.data.insert(index, line_list)
            index += 1


    def change_spk_file(self, file_path):
        self.change_parameter(['SolarSystem.SPKFilename'], ["'"+os.path.abspath(file_path)+"';"])


    def change_initial_date(self, initial_date, initial_date_format='TDBGregorian'):
        keys = ['DefaultSC.DateFormat', 'DefaultSC.Epoch']
        values = [initial_date_format+';', initial_date+';']
        self.change_parameter(keys, values)
    

    def change_initial_state(self, state, initial_CoordinateSystem='EarthMJ2000Eq'):
        initial_state = [f'{s:.15e};' for s in state]
        keys = ['DefaultSC.CoordinateSystem', 'DefaultSC.X', 'DefaultSC.Y', 'DefaultSC.Z', 'DefaultSC.VX', 'DefaultSC.VY', 'DefaultSC.VZ']
        values = [initial_CoordinateSystem+';', initial_state[0], initial_state[1], initial_state[2], initial_state[3], initial_state[4], initial_state[5]]
        self.change_parameter(keys, values)
    

    def change_initial_conditions_from_file(self, initCond_file, file_data_type='isot', initial_date_format='TDBGregorian', 
                                            initial_CoordinateSystem='EarthMJ2000Eq'):
        with open(initCond_file) as f:
            lines = f.readlines()
        input_data = [line.split() for line in lines]
        input_data = [d for d in input_data if d][-1]
        
        if file_data_type=='isot':
            initial_date = input_data[0]
            time = initial_date.split('T')[1]
            year, month, day = initial_date.split('T')[0].split('-')
            initial_date = f"\'{day} {self.MONTHS[month]} {year} {time}\'"
            initial_state = np.array(input_data[1:7], dtype=float)

        self.change_initial_date(initial_date, initial_date_format)
        self.change_initial_state(initial_state, initial_CoordinateSystem)


    def change_central_body(self, body):
        body += ';'
        self.change_parameter(['DefaultProp_ForceModel.CentralBody'], [body])
    

    def change_point_masses(self, bodies):
        bodies_value = '{'
        for i,b in enumerate(bodies):
            if i == len(bodies)-1:
                bodies_value += (b + '};')              
            else:
                bodies_value += (b + ', ')
        self.change_parameter(['DefaultProp_ForceModel.PointMasses'], [bodies_value])
    

    def change_bodies_mu(self, bodies, mus):
        keys= []
        values = []
        for b,mu in zip(bodies,mus):
            keys.append(b+'.Mu')
            values.append(f'{mu};')
        self.change_parameter(keys, values)
    

    def change_probe_mass(self, mass):
        self.change_parameter(['DryMass'], [mass])


    def add_integrator_fix_step(self, method, time_step):
        keys = ['DefaultProp_ForceModel.ErrorControl', 'DefaultProp.MaxStepAttempts', 
                'DefaultProp.Type', 'DefaultProp.InitialStepSize',
                'DefaultProp.MinStep', 'DefaultProp.MaxStep']
        values = ['None;', '1;',
                  method+';', f'{time_step};',
                  f'{time_step};', f'{time_step};']
        self.change_parameter(keys, values)
    

    def add_integrator_variable_step(self, method, time_step, min_step, max_step, tol):
        keys = ['DefaultProp_ForceModel.ErrorControl', 'DefaultProp.MaxStepAttempts', 
                'DefaultProp.Type', 'DefaultProp.InitialStepSize',
                'DefaultProp.MinStep', 'DefaultProp.MaxStep',
                'DefaultProp.Accuracy']
        values = ['RSSStep;', '50;',
                  method+';', f'{time_step};',
                  f'{min_step};', f'{max_step};',
                  f'1e{tol};']
        self.change_parameter(keys, values)
    

    def change_propagation_duration(self, time_days):
        for i in range(len(self.data)):
            if self.data[i]:
                if self.data[i][0] == 'BeginMissionSequence;':
                    self.data[i+1] = ['Propagate', f"'Propagate-{time_days}d'", 'DefaultProp(DefaultSC)',
                                      '{DefaultSC.ElapsedSecs', '=', f'{time_days*86400}'+'};']
    

    def back_propagation(self, type='ON'):
        for i in range(len(self.data)):
            if self.data[i]:
                if self.data[i][0] == 'BeginMissionSequence;':
                    if type.lower() == 'on':
                        self.data[i+1].insert(2, 'BackProp')
                    elif type.lower() == 'off':
                        if 'BackProp' in self.data[i+1]:
                            self.data[i+1].remove('BackProp')
            

    def change_output_directory(self, path):
        self.output_directory = os.path.abspath('../../Outputs/GMAT')
    

    def change_output(self, output_name):
        keys = ['EPH.Filename', 'REPORT.Filename']
        values = []
        values.append( "'" + self.output_directory + '/' + output_name + "-eph.txt';" )
        values.append( "'" + self.output_directory + '/' + output_name + "-rep.txt';" )
        self.change_parameter(keys, values)
    

    def change_output_coordinate_system(self, coordinate_system):
        keys = ['EPH.CoordinateSystem', 'REPORT.Add']
        values = []
        values.append(f'{coordinate_system};')
        values.append("{"+f'DefaultSC.TDBGregorian, DefaultSC.{coordinate_system}.X, DefaultSC.{coordinate_system}.Y,\
 DefaultSC.{coordinate_system}.Z, DefaultSC.{coordinate_system}.VX, DefaultSC.{coordinate_system}.VY,\
 DefaultSC.{coordinate_system}.VZ, DefaultSC.DefaultProp_ForceModel.AccelerationX,\
 DefaultSC.DefaultProp_ForceModel.AccelerationY, DefaultSC.DefaultProp_ForceModel.AccelerationZ'+"};")
        self.change_parameter(keys, values)
    

    def SRP(self, onoff, S, Cr, SF=1367, Dist_SF=149597870.691):
        if onoff.lower() == 'off':
            keys = ['DefaultProp_ForceModel.SRP']
            values = ['Off;']
            self.change_parameter(keys, values)
        elif onoff.lower() == 'on':
            keys = ['DefaultProp_ForceModel.SRP',
                    'DefaultSC.Cr',
                    'DefaultSC.SRPArea' ]
            values = ['On;',
                      f'{Cr};',
                      f'{S};']
            self.change_parameter(keys, values)
            new_keys = ['DefaultProp_ForceModel.SRP.SRPModel',
                        'DefaultProp_ForceModel.SRP.Flux',
                        'DefaultProp_ForceModel.SRP.Nominal_Sun']
            new_values = ['Spherical;',
                          f'{SF};',
                          f'{Dist_SF};']
            self.add_parameter(new_keys, new_values, 'DefaultProp_ForceModel.SRP')


    def SH(self, body, Degree, SH_file):
        new_keys = ['DefaultProp_ForceModel.PrimaryBodies',
                    f'DefaultProp_ForceModel.GravityField.{body}.Degree',
                    f'DefaultProp_ForceModel.GravityField.{body}.Order',
                    f'DefaultProp_ForceModel.GravityField.{body}.StmLimit',
                    f'DefaultProp_ForceModel.GravityField.{body}.PotentialFile',
                    f'DefaultProp_ForceModel.GravityField.{body}.TideModel',]
        new_values = ['{'+body+'};',
                      f'{Degree};',
                      f'{Degree};',
                      f'100;',
                      "'"+os.path.abspath(SH_file)+"'"+';',
                      "'None';"]
        self.add_parameter(new_keys, new_values, 'DefaultProp_ForceModel.CentralBody')


    def save_script(self, file_path):
        aux = [' '.join(d) for d in self.data]
        with open(f'{file_path}.script', 'w') as f1:
            f1.write('\n'.join(aux))
            f1.close()
    

    def run_scripts(self, scripts, GMAT_path='../../../../../GMAT/bin/GMAT'):
        # Create Folder if it doesnt exists
        logs_dir = self.output_directory + '/LOGS/' 
        if not os.path.isdir(logs_dir):
            os.makedirs(logs_dir)
        for script in scripts:
            log_file = f'{script}.log'
            log_file = os.path.normpath(logs_dir + log_file)
            command = [GMAT_path, '-m', '-x', '-r', '-ns', '-l', log_file, f'{script}.script']
            print(f'Running: {script}')
            subprocess.run(command)